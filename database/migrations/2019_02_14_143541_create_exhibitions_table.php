<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExhibitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exhibitions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('about',config('variables.size_exh_about'))->nullable();
            $table->longText('description')->nullable();
            $table->string('photo_url')->nullable();
            $table->unsignedInteger('author_id');
            $table->foreign('author_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedInteger('type_id');
            $table->foreign('type_id')->references('id')->on('types_exhibitions');
            $table->unsignedInteger('style_id');
            $table->foreign('style_id')->references('id')->on('styles');
            $table->date('started_at');
            $table->date('finished_at');
            $table->unsignedBigInteger('ratings')->default(0);
            $table->unsignedBigInteger('ratings_count')->default(0);
            $table->unsignedInteger('views')->default(0);
            $table->boolean('private')->default(0);
            $table->json('code')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exhibitions');
    }
}
