<?php

// use Faker\Generator as Faker;
use App\Models\Exhibition;
use App\Models\Picture;


$factory->define(Picture::class, function (Faker\Generator $faker) {
	$faker = Faker\Factory::create('ru_RU');
	$pictures=collect([
		"examples/picture1.png",
		"examples/picture2.jpg",
		"examples/picture3.jpg",
		"examples/picture4.jpg",
		"examples/picture6.jpg",
		"examples/picture7.jpg",
		"examples/picture14.jpg",
		"examples/picture9.jpg",
		"examples/picture10.jpg",
		"examples/picture11.jpg",
		"examples/picture12.jpg",
		"examples/picture13.jpg",
		"examples/picture15.jpg"
	]);

    return [
        //
        'name' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'description' => $faker->paragraphs($nb = 5, $asText = true),
        'url'=>$pictures->random(),
        'exhibition_id'=>$faker->biasedNumberBetween(1,Exhibition::count())



    ];
});
