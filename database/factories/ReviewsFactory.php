<?php

use Faker\Generator as Faker;
use App\Models\User;
use App\Models\Exhibition;
use App\Models\Review;

$factory->define(Review::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'about'=>$faker->text(config('variables.size_user_about')),
        'description' => $faker->articleContent,
        'exhibition_id'=> $faker->biasedNumberBetween(1,Exhibition::count()),
        'author_id'=> $faker->biasedNumberBetween(1,User::count()),
        'created_at'=>$faker->dateTimeBetween('-7 months')
    ];
});