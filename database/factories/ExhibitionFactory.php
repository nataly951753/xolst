<?php

// use Faker\Generator as Faker;
use App\Models\Exhibition;
use App\Models\User;
use App\Models\Style;
use App\Models\TypeExhibition;




$factory->define(Exhibition::class, function (Faker\Generator $faker) {
	$faker = Faker\Factory::create('ru_RU');
	$a=$faker->dateTimeBetween('-1 years', '+1 years');
	$b=$faker->dateTimeInInterval($a, '+1 months');
	$c=$faker->biasedNumberBetween(1,1000);
	$d=$faker->biasedNumberBetween($c,$c*5);
    $pictures=collect([
        "examples/photo1.jpg",
        "examples/photo2.jpg",
        "examples/photo3.jpg",
        "examples/photo4.jpg",
        "examples/photo6.jpg",
        "examples/photo7.jpg",
        "examples/photo5.jpg",
        "examples/photo8.jpg",
        "examples/photo9.jpg",
        "examples/photo10.jpg",
        "examples/photo11.jpg",
        "examples/photo12.jpg",
    ]);
    $faker->addProvider(new BlogArticleFaker\FakerProvider($faker));

    return [
        'name' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'about'=>$faker->text(config('variables.size_user_about')),
        'description' => $faker->articleContent,
        'photo_url'=>$pictures->random(),
        'author_id'=> $faker->biasedNumberBetween(1,config('variables.authors_count')),
        'type_id'=>$faker->biasedNumberBetween(1,TypeExhibition::count()),
        'style_id'=>$faker->biasedNumberBetween(1,Style::count()),
        'started_at'=>$a,
        'finished_at'=>$b,
        'ratings'=>$d,
        'ratings_count'=>$c,
        'private'=>0,
        'views'=>$faker->biasedNumberBetween(1,100000),
        'created_at'=>$faker->dateTimeBetween('-7 months')
    ];
});
