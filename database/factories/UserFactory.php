<?php

// use Faker\Generator as Faker;
use App\Models\User;
use App\Models\Role;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker\Generator $faker) {
	$faker2 = Faker\Factory::create('ru_RU');
    $faker->addProvider(new BlogArticleFaker\FakerProvider($faker));
    $pictures=collect([
        "examples/photo1.jpg",
        "examples/photo2.jpg",
        "examples/photo3.jpg",
        "examples/photo4.jpg",
        "examples/photo6.jpg",
        "examples/photo7.jpg",
        "examples/photo5.jpg",
        "examples/photo8.jpg"
    ]);
    return [
        'name' => $faker2->name,
        'role_id'=>$faker->biasedNumberBetween(1,Role::count()),
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'about'=>$faker2->text(config('variables.size_user_about')),
        'information'=>$faker->articleContent,
        'photo_url'=>$pictures->random(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'created_at'=>$faker->dateTimeBetween('-7 months')
    ];
});
