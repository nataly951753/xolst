<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new Role([
                	'name'=>'admin',
                	'description' => 'Адміністратори мають можливість створювати нових адміністраторів, переглядати звіти, редагувати стилі, типи, теги.'
                ]))->save();
        (new Role([
                	'name'=>'author',
                	'description' => 'Автори мають можливість створювати виставки.'
                ]))->save();
        (new Role([
                	'name'=>'reviewer',
                	'description' => 'Критики мають можливість створювати рецензії.'
                ]))->save();
        (new Role([
                	'name'=>'user',
                	'description' => 'Зареєстровані користувачі мають можливість оцінювати виставки, додавати виставки та авторів до обраного, відвідувати закриті виставки.'
                ]))->save();
    }
}
