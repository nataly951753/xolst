<?php

use Illuminate\Database\Seeder;
use App\Models\Style;

class StylesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new Style([
        	'name'=>'Ню (Nu-art)',
        	'description'=>'Ню (Nu-art) переводится с французского как раздетый или нагой. Отсюда и название жанра в искусстве, посвященного изображению обнаженного тела, преимущественно женского.На фотографиях в жанре арт-ню обнаженное тело показано артистично, художественно, а фото считается произведением искусства, воспевающим красоту человеческого тела. На раскрытие идеи работают все изобразительные средства – свет, цвет, форма, фактура и композиция. Художественное фото в стиле ню вызывает восхищение, удивление, затрагивает высокие чувства.

			Многие фотографии в стиле ню на заре зарождения жанра казались революционными, сегодня они воспринимаются абсолютно спокойно. Примером таких фотографий могут служить работы Эрвина Блюмендфельда (1897-1969), на которых запечатлены экстравагантные и одновременно целомудренные позы и ракурсы его моделей. Женское тело казалось ему неведомым, загадочным континентом, поэтому так бесплотны и эфемерны героини его «ню».'
        ]))->save();
        (new Style([
                	'name'=>'Документальна фотографія',
                	'description'=>'До недавнего времени в профессиональном языке фотографов отсутствовало само понятие «документальная фотография». На сегодняшний день оно обозначает жанр фотографии, изображающий события и явления социального характера через глубокое погружение в тему.Если проанализировать различные источники, то можно выделить основные черты, которые отличают данный жанр от других:
        
        • запечатление социальной проблемы, которая вызывает резонанс;
        
        • эстетичность кадров в сочетании с их естественностью и реалистичностью;
        
        • обычно это несколько фотографий или даже целая развернутая серия снимков, связанных между собой общей темой или идеей;
        
        • как правило, этот жанр тесно связан с политическими веяниями и сам претендует на политическое влияние;
        
        • документальная фотография носит публичный характер.
        
        Нередко снимок дополняется текстом, который описывает запечатленное событие, часто его составляет сам фотограф. Современная документальная фотография вышла за рамки повествования о злободневных проблемах, в ней все чаще проявляется субъективная позиция фотографа, его авторский стиль и взгляд. Но информационная функция, которая отличает ее от других жанров, все равно остается ведущей.
        
        Документальные фотографии – уникальные, правдивые и прямые визуальные историко-художественные свидетельства тех или иных событий, в основе которых лежит серьезная сюжетная завязка.'
                ]))->save();
        (new Style([
                	'name'=>'Абстракціонізм',
                	'description'=>'Абстракционизм (лат. abstractio «удаление, отвлечение») или нефигуративное искусство — направление искусства, отказавшееся от приближённого к действительности изображения форм в живописи и скульптуре. Одна из целей абстракционизма — изображение определённых цветовых сочетаний и геометрических форм, вызывающих у созерцателя чувство полноты и завершённости композиции.'
                ]))->save();
        (new Style([
                	'name'=>'Авангардизм',
                	'description'=>'Авангардизм (от фр. avant-garde - передовой отряд) - совокупность экспериментальных, модернистских, подчеркнуто необычных, поисковых начинаний в искусстве 20 века. Авангардными направлениями являются: фовизм, кубизм, футуризм, экспрессионизм, абстракционизм, сюрреализм, акционизм, поп-арт, концептуальное искусство.'
                ]))->save();
        (new Style([
                	'name'=>'Ампір',
                	'description'=>'Ампир (фр. Empire – империя) – стиль в  изобразительном искусстве, завершивший развитие классицизма. Стиль опирался на художественное наследие архаической Греции и императорского Рима, черпая из него мотивы для воплощения величественной мощи: монументальные массивные портики, военная эмблематика в архитектурных деталях и декоре (ликторские связки, воинские доспехи, лавровые венки, орлы).'
                ]))->save();
        (new Style([
                	'name'=>'Андергрaунд',
                	'description'=>'Андергрaунд (от англ. underground — подполье, подпольный; under — под, ниже, ground — земля, площадка, пол) — совокупность творческих направлений в современном искусстве (музыке, литературе, кино, изобразительном искусстве и др.), противопоставляющихся массовой культуре, мейнстриму и официальному искусству. Андеграунд включает в себя неформальные, независимые или запрещённые цензурой виды и произведения искусства. Для андерграунда характерны разрыв с господствующей идеологией, игнорирование стилистических и языковых ограничений, отказ от общепринятых ценностей, норм, от социальных и художественных традиций, нередко эпатаж публики, бунтарство. Андерграунд отвергает и часто нарушает принятые в обществе политические, моральные и этические ориентации и стереотипы поведения, внедряя в повседневность новые схемы поведения. Типичная тематика американского и европейского андерграунда — «сексуальная революция», наркотики, антирелигиозность, проблемы маргинальных групп.'
                ]))->save();
        (new Style([
                	'name'=>'Гіперреалізм',
                	'description'=>'Гиперреализм, фотореализм, суперреализм - стиль в живописи и скульптуре, основанный на фотореализации объекта. Гиперреализм возник в США в середине 20 века. Главная цель гиперреализма показать действительность.'
                ]))->save();
        (new Style([
                	'name'=>'Готика',
                	'description'=>'Готика (от итал. gotico - непривычный, варварский) - период в развитии средневекового искусства, охватывавший почти все области культуры и развивавшийся на территории Западной, Центральной и отчасти Восточной Европы с XII по XV век. Готика завершила развитие европейского средневекового искусства, возникнув на основе достижений романской культуры, а в эпоху Возрождения искусство средневековья считалось «варварским». Готическое искусство было культовым по назначению и религиозным по тематике. Оно обращалось к высшим божественным силам, вечности, христианскому мировоззрению.'
                ]))->save();
       ( new Style([
               	'name'=>'Дівізіонізм',
               	'description'=>'Дивизионизм (от франц. division - разделение), пуантилизм - направление неоимпрессионизма, письмо раздельными четкими мазками в виде точек или мелких квадратов. Смешение цветов с образованием оттенков происходит на этапе восприятия картины зрителем. '
               ]))->save();
        (new Style([
                	'name'=>'Імпресіонизм',
                	'description'=>'Импрессионизм (фр. impressionnisme, от impression — впечатление) — одно из крупнейших течений в искусстве последней трети XIX — начала XX веков, зародившееся во Франции и затем распространившееся по всему миру. Представители импрессионизма стремились разрабатывать методы и приёмы, которые позволяли наиболее естественно и живо запечатлеть реальный мир в его подвижности и изменчивости, передать свои мимолётные впечатления. '
                ]))->save();
        (new Style([
                	'name'=>'Класицизм',
                	'description'=>'Классицизм (произошло от лат. classicus - образцовый) - художественный стиль и эстетическое направление в европейской литературе и искусстве 17 - начала 19 вв., одной из важных черт которых являлось обращение к образам и формам античной литературы и искусства, как идеальному эстетическому эталону. Художественное произведение, с точки зрения классицизма, должно строиться на основании строгих канонов, тем самым, обнаруживая стройность и логичность самого мироздания. Интерес для классицизма представляет только вечное и неизменное. В каждом явлении он стремится распознать только существенные, типологические черты, отбрасывая случайные индивидуальные признаки. Эстетика классицизма придаёт огромное значение общественно-воспитательной функции искусства. Многие правила и каноны классицизм берет из античного искусства.
        
        '
                ]))->save();
        (new Style([
                	'name'=>'Концептуалізм',
                	'description'=>'Концептуальное искусство (концептуализм) (от лат. conceptus - мысль) - направление в модернизме 1970-х, 1980-х годов, целью которого было создание произведений более или менее свободных от материального воплощения "художественных идей". Последователи концептуализма в своих картинах заменяли изображение надписями, диаграммами, схемами и т.п. При этом материальные средства выполняют роль только возбудителя представлений, а предметом созерцания является мысленная форма.'
                ]))->save();
        (new Style([
                	'name'=>'Космізм',
                	'description'=>'Космизм (от греч. kosmos - организованный мир, kosma - украшение) - художественно-философское мировоззрение, в основе которого располагается знание о Космосе и представление о человеке как о гражданине Мира, а также о микрокосмосе, подобном Макрокосмосу. Космизм связан с астрономическими знаниями о Вселенной.
        
        '
                ]))->save();
        (new Style([
                	'name'=>'Кубізм',
                	'description'=>'Кубизм (от фр. cubisme, произошло от cube - куб) - модернистское направление в живописи начала ХХ века, которое выдвинуло на первый план формальную задачу конструирования объёмной формы на плоскости, сведя к минимуму изобразительно-познавательные функции искусства. Слово "кубисты" было употреблено в 1908 и 1909 французским критиком Л. Воселем как насмешливое прозвище группы художников, изображавших предметный мир в виде комбинации геометрических тел или фигур. '
                ]))->save();
        (new Style([
                	'name'=>'Метареалізм',
                	'description'=>'Метареализм (произошло от греч. meta - между, после, через и геalis - вещественный, действительный) - реализм многих реальностей, связанных непрерывностью метаболических превращений и перемен состояний. Есть реальность, открытая зрению муравья, и реальность, открытая блужданию электрона, и реальность, свёрнутая в математическую формулу. Метареальный образ, метаморфоза, метабола - способ взаимосвязи всех этих реальностей, утверждение их растущего единства.
        
        '
                ]))->save();
        (new Style([
                	'name'=>'Мінімалізм',
                	'description'=>'Минимализм (произошло от англ. minimal art - минимальное искусство) - художественное течение, исходящее из минимальной трансформации используемых в процессе творчества материалов, простоты и единообразия форм, монохромности, творческого самоограничения художника. Для минимализма характерен отказ от субъективности, репрезентации, иллюзионизма. Отвергая классические приемы и традиционные художественные материалы, минималисты используют промышленные и природные материалы простых геометрических форм и нейтральных цветов (черный, серый), малых объемов, применяют серийные, конвейерные методы индустриального производства.'
                ]))->save();
        (new Style([
                	'name'=>'Модерн',
                	'description'=>'Модерн (произошло от фр. moderne - новейший, современный) - cтиль в европейском и американском иcкyccтвe нa pyбeжe XIX-XX вeкoв. Модерн переосмысливал и стилизовал черты искусства разных эпox, и выpaбoтaл coбcтвeнныe xyдoжecтвeнныe пpиeмы, ocнoвaнныe нa пpинципax асимметрии, орнаментальности и декоративности. Его отличительными особенностями являются: отказ от прямых линий и углов в пользу более естественных, природных линий. Этим oбъяcняeтcя нe тoлькo интepec к pacтитeльным opнaмeнтaм в пpoизвeдeнияx мoдepнa, нo и caмa иx кoмпoзициoннaя и плacтичecкaя cтpyктypa - oбилиe кpивoлинeйныx oчepтaний, oплывaющиx, нepoвныx кoнтypoв, напоминающих pacтитeльныe фopмы. '
                ]))->save();
        (new Style([
                	'name'=>'Натуралізм',
                	'description'=>'Натурализм (произошло от лат. naturalis - природный) - направление в литературе и искусстве последней трети 19 века, стремившееся к объективно точному и бесстрастному воспроизведению наблюдаемой реальности. Считается, что натурализм воспроизводит реальность без ее идейного осмысления, художественного обобщения, критической оценки и отбора.'
                ]))->save();
        (new Style([
                	'name'=>'Орфізм',
                	'description'=>'Орфизм (от фр. orphisme, от Orpһée - Орфей) - направление во французской живописи 1910-х гг. Название дано в 1912 французским поэтом Аполлинером живописи художника Робер Делоне. Орфизм связан с кубизмом, футуризмом и экспрессионизмом. Основные особенности живописи этого стиля эстетизм, пластичность, ритмичность, изящество силуэтов и линий. '
                ]))->save();
        (new Style([
                	'name'=>'Примітивізм',
                	'description'=>'Примитивизм - стиль живописи, зародившийся в XIX-XX веках. Примитивисты намеренно упрощали картину, делая её формы примитивными, как народное искусство, творчество ребенка или первобытного человека. Основное отличие от наивного искусства: наив - живопись непрофессионалов, а примитивизм - стилизованная живопись профессионалов. '
                ]))->save();
        (new Style([
                	'name'=>'Поп-арт',
                	'description'=>'Поп-арт (произошло от англ. popular art - популярное, общедоступное искусство или от pop - отрывистый звук, лёгкий хлопок) - буквально: искусство, производящее взрывной, шокирующий эффект - неоавангардистское направление в изобразительном искусстве. Поп арт получил распространение со 2-й половины 1950-х гг. в США. Художники этого направления воспроизводят буквально типичные предметы современного быта (вещи домашнего обихода, упаковку товаров, фрагменты интерьеров, детали машин...), популярные печатные изображения известных личностей, вырезки из газет или включают эти предметы и изображения в композиции. Отличительная черта поп-арта - сочетание вызова с безразличием. Все одинаково ценно или бесценно, одинаково красиво или безобразно, одинаково достойно или недостойно. '
                ]))->save();
        (new Style([
                	'name'=>'Реалізм',
                	'description'=>'Реализм (произошло от лат. геalis - вещественный, действительный) - направление в искусстве, характеризующееся изображением социальных, психологических и прочих явлений, максимально соответствующим действительности.'
                ]))->save();
        (new Style([
                	'name'=>'Рококо',
                	'description'=>'Рококо (происходит от фр. rococo, rocaille) - стиль в искусстве и архитектуре, зародившийся во Франции в начале 18 века. Отличался грациозностью, легкостью, интимно-кокетливым характером. Придя на смену тяжеловесному барокко, рококо явился одновременно и логическим результатом его развития, и его художественным антиподом. '
                ]))->save();
        (new Style([
                	'name'=>'Романтизм',
                	'description'=>'Романтизм (произошло от фр. romantisme) - идейное и художественное направление в европейской и американской живописи конца 18 - начала 19 веков, выдвигавшее на первый план индивидуальность, наделяя ее идеальными устремлениями. Романтизм выделял главенство воображения и чувств. Его основная социально-идеологическая предпосылка - разочарование в буржуазной цивилизации, в социальном, промышленном, политическом и научном прогрессе. '
                ]))->save();
        (new Style([
                	'name'=>'Символізм',
                	'description'=>'Символизм (происходит от фр. symbolisme) - одно из крупнейших направлений в искусстве (в литературе, музыке и живописи), возникшее во Франции в 1870-80-х гг. и достигшее наибольшего развития на рубеже XIX и XX веков, прежде всего в самой Франции, Бельгии и России. Символисты радикально изменили не только различные виды искусства, но и само отношение к нему. Их экспериментаторский характер, стремление новаторству, космополитизм и обширный диапазон влияний стали образцом для большинства современных направлений искусства. '
                ]))->save();
        (new Style([
                	'name'=>'Стімпанк',
                	'description'=>'Стимпанк, паропанк (от англ. steam - пар и punk - протест, бунт) - направление в искусстве берущее начало в научной фантастике. В произведениях этого стиля показывается альтернативный вариант развития человечества на основе паровых машин с выраженной стилизацией под викторианскую эпоху. В стиле стимпанк нет природы, он наполнен элементами урбанистического пейзажа: кирпич, трубы, булыжные мостовые, воздух, фабричные трубы, роботы и механизмы, паровозы, автомобили.'
                ]))->save();
        (new Style([
                	'name'=>'Сюрреалізм',
                	'description'=>'Сюрреализм (от фр. surrealisme - сверх + реализм) - одно из направлений модернизма. Сюрреализм возник во Франции в середине 20 века. Художники с фотографической точностью создавали нелогичные, пугающие картины обычных предметов или использовали нестандартную технику живописи, дающую возможность выразить подсознание.'
                ]))->save();
        (new Style([
                	'name'=>'Ташизм',
                	'description'=>'Ташизм (от фр. tache - пятно) - одно из названий направления в абстракционизме 50-х годов. Ташизму - живопись пятнами, которые: не воссоздают образов реальности; наносятся на холст быстрыми движениями руки без заранее обдуманного плана; выражают бессознательную активность художника.'
                ]))->save();
        (new Style([
                	'name'=>'Фентезі',
                	'description'=>'Фэнтези (от англ. fantasy - фантазия) - стиль живописи, появившийся в начале XX века и основанный на использовании мифологических и сказочных мотивов. Развитие живописи в жанре фэнтези шло параллельно и схоже с развитием литературы, так как основная масса художников рисовала обложки и иллюстрации к книгам и играм, а также фантастические комиксы и коллекционные игровые карты.'
                ]))->save();
        (new Style([
                	'name'=>'Футуризм',
                	'description'=>'Футуризм (произошло от итал. futurismo - будущее) - течение в искусстве начала 20 века. Основные темы футуризма: достижения техники и технический прогресс.'
                ]))->save();
        (new Style([
                	'name'=>'Експресіонізм',
                	'description'=>'Экспрессионизм (произошло от фр. expression - выразительность) - модернистское течение в западноевропейском искусстве начале 20 века, возникло как отклик на острейший социальный кризис - 1-ю мировую войну. Идейной основой экспрессионизма стал протест против уродливого мира, все большее отчуждение человека от мира, чувства крушения надежд. Экспрессионистам свойственны тяготение к мистике и пессимизм.'
                ]))->save();
        (new Style([
                	'name'=>'Вінтаж',
                	'description'=>'Винтаж это направление которое было популярно в 50-х годах 20-го века. Само слова «Винтаж» подразумевает под собой определённый предмет, типичную вещь своего времени, несущую особые стилевые тенденции. Винтаж добавляет атмосферу ностальгии и заставляет нас обратиться к прошлому. Типичный приём данного стиля это «состаривание»  фотографий и образов.
        
        Что касается колористики то это либо серые монохромные композиции, либо мягкие разбелённые цвета словно выцветшие со временем. Часто используются специфические ретро-шрифты. Винтажный графический стиль несёт в себе ценность прошлого через настоящее.'
                ]))->save();
        (new Style([
                	'name'=>'Типографіка',
                	'description'=>'Использование текста как основного элемента в композиции — вот отличительные черты типографики как стиля в графическом дизайне. Использование шрифтовых гарнитур для создания форм и образов усиливает смысловую нагрузку и глубину визуального сообщения.На данный момент это один из самых действенных инструментов воздействия в рекламе. Верстая буклет, плакат или вывеску мы работаем с различными гарнитурами и шрифтами, и то как мы это делаем имеет первостепенное значение. Размещая текстовую информацию как основной элемент нам приходится пристальнее посмотреть на целесообразность использования тех или иных гарнитур и особенности восприятия шрифтов.'
                ]))->save();
        (new Style([
                	'name'=>'Ар-деко',
                	'description'=>'Ар-деко (арт-деко) (фр. art déco  — «декоративное искусство»). Течение в изобразительном искусстве первой половины XX века, зародившееся во Франции в 1920-х годах. Ар-деко это след в архитектуре, живописи, моде. Эклектичный стиль сочетающий в себе модерн и неоклассицизм. Произошёл от авангардизма.Отличительные черты стиля это смелые геометрические формы, строгая закономерность, щедрые орнаменты и этнические геометрические узоры, богатство цветов. Стилю свойственна экзотичность выражаемая при помощи элементов культуры разных стран и народов.'
                ]))->save();
        (new Style([
                	'name'=>'Гранж',
                	'description'=>'Гранж (англ. Grunge – грязь, мерзость) – стиль возникший в музыке в середине 1980-х годов как поджанр альтернативного рока. Этот стиль быстро перекинулся из музыки  во все остальные сферы творческой деятельности. Гранж перерос в новую молодёжную субкультуру, став противоположностью гламуру и «глянцевости».Сейчас мы можем наблюдать гранж в дизайне одежды, в веб дизайне, графическом дизайне и т.д. Гранж характеризуется нарочитой небрежностью и отказом от театральности. Основные отличительные черты стиля гранж  это небрежные мазки, грязь, кляксы, потёртости, помятости, грубые обшарпанные фактуры и прочая неряшливость. Цветовая гамма приглушённая, спокойная: чёрный, коричневый, бежевый и серый цвета.'
                ]))->save();
        (new Style([
                	'name'=>'Цифровой стиль',
                	'description'=>'Цифровой стиль (1985 — настоящее время). Компьютер позволяет современным дизайнерам гораздо больше, чем когда-либо ещё. В современном дизайне мы можем увидеть графику из вебдизайна  (web 2.0) и компьютерных игр, несуществующие 3D формы и перспективы.Цифровой стиль сложно классифицировать по каким бы то ни было критериям, основная особенность это использование компьютера как основного инструмента, без которого создание композиции стало бы невозможным, либо труднодоступным.'
                ]))->save();
        (new Style([
                	'name'=>'Інфографіка',
                	'description'=>'Инфографика представляет информацию с помощью графики. Этот способ донесения информации зародился во второй половине XX века, но стал особеннно популярен в последние годы из-за чего я решил выделить его в этой статье.Инфографику возможно пока нельзя выделить в отдельный стиль, но не узнать её трудно. Это всегда графики, цифры и сухие факты. Инфографика способна доносить максимальное количество информации в минимальные сроки, что делает её крайне эффективной.'
                ]))->save();
        (new Style([
                	'name'=>'Чорний і білий',
                	'description'=>'Черно-белые снимки — это монохромное искусство. Есть в них что-то такое, что просто невозможно передать цветом. Авторы сознательно возвращаются во времена черно-белой фотографии, чтобы ограничить себя в цвете и сконцентрироваться на других выразительных способах. Наверное, именно поэтому монохромная фотография так хорошо раскрывает эмоции и находит у нас отклик на глубинном уровне.'
                ]))->save();
        // new Style([
        // 	'name'=>'',
        // 	'description'=>''
        // ])->save();
    }
}
