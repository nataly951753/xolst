<?php

use Illuminate\Database\Seeder;
use App\Models\Exhibition;
use App\Models\User;
use App\Models\Tag;

class ExhibitionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $exhibition = factory(Exhibition::class, 100)->create()->each(function($exh) {
            $r = rand(5,20);
            for ($i=0; $i < $r; $i++) { 
                $user = factory(User::class)->make();
                $exh->favourites()->save($user);
            }
            $r = rand(0,5);
            for ($i=0; $i < $r; $i++) { 
                $tag = factory(tag::class)->make();
                $exh->tags()->save($tag);
            }
        });;
    }
}
