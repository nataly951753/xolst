<?php

use Illuminate\Database\Seeder;
use App\Models\TypeExhibition;

class TypesExhibitionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        (new TypeExhibition([
                	'name'=>'Графіка'
                ]))->save();
        (new TypeExhibition([
                	'name'=>'Живопис'
                ]))->save();
        (new TypeExhibition([
                	'name'=>'Фотографія'
                ]))->save();
    }
}
