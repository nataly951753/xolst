<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(StylesTableSeeder::class);
        $this->call(TypesExhibitionsTableSeeder::class);
        $this->call(ExhibitionsTableSeeder::class);
        $this->call(PicturesTableSeeder::class);
        $this->call(ReviewsTableSeeder::class);

    }
}
