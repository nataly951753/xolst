<?php

use Illuminate\Database\Seeder;
use App\Models\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(User::class, config('variables.authors_count'))->create()->each(function($author) {
            $r = rand(5,20);
            for ($i=0; $i < $r; $i++) { 
                $user = factory(User::class)->make();
                $author->subscribers()->save($user);
            }
        });
    }
}
