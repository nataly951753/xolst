<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exhibition;


class SearchController extends Controller
{
    //страница search
     public function index()
	 {	
	 	return view('pages.search');
	 }

	 public function search_exhibition(Request $request)
	 {	 	
	 	if ($request->text) {
		 	$f = Exhibition::search($request->text)->paginate(3);
		 	return response()->json($f, 200);	    
		} else {
		 	return response()->json([], 200);	    
		}
	 }
	
}
