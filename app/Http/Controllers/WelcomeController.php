<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;


class WelcomeController extends Controller
{
    //отображение главной страницы
    public function __invoke()
	 {
	 return view('pages.welcome', [
	 	'authors' => User::has('exhibitions')->leftJoin('favourites_authors', 'favourites_authors.author_id', '=', 'users.id')->
               selectRaw('users.*, count(favourites_authors.author_id) AS `count`')->
               groupBy('users.id')->
               orderBy('count','DESC')
               ->take(3)
               ->get()
	 ]);
	 }
}
