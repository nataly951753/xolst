<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exhibition;
use App\Models\View;
use App\Models\User;
use App\Models\TypeExhibition;
use App\Models\Style;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;




class ExhibitionsController extends Controller
{
	 public function index($type = 'all', $style = 'all', $tag = 'all', $orderBy = 'views')
	 {	 	
	 	$exh = Exhibition::has('pictures', '>=', config('variables.pictures_count_min'))->where('finished_at','>',Carbon::now());
	 	if($type!='all')
	 		$exh=$exh->whereHas('type', function (Builder $query) use($type) {
    				$query->where('name', $type);
			});
	 	if($style!='all')
	 		$exh=$exh->whereHas('style', function (Builder $query) use($style) {
    				$query->where('name', $style);
			});
	 	if($tag!='all')
	 		$exh=$exh->whereHas('tags', function (Builder $query) use($tag) {
    				$query->where('name', $tag);
			});
	 	if($orderBy=='views')
	 		$exh=$exh->orderBy('views', 'desc');
	 	if($orderBy=='started_date')
	 		$exh=$exh->orderBy('started_at', 'asc');
	 	if($orderBy=='public')
	 		$exh=$exh->where('private', 0)->orderBy('views', 'desc');
	 	if($orderBy=='ratings')
	 		$exh=$exh->selectRaw('*, ratings/ratings_count AS `count`')->
               orderBy('count','DESC');
	 	if($orderBy=='favorites')
	 		$exh=$exh->leftJoin('favourites_exhibitions', 'favourites_exhibitions.exhibition_id', '=', 'exhibitions.id')->
               selectRaw('exhibitions.*, count(favourites_exhibitions.exhibition_id) AS `count`')->
               groupBy('exhibitions.id')->
               orderBy('count','DESC');


	 	return view('pages.all_exhibitions',[
	 		//выставки, количество картин которых соответствует требуемому, которые не закрылись и сортированны по количеству просмотров, 15 записей на страницу
	 		'exhibitions' => $exh->paginate(15),
	 		'orderBy'=>$orderBy,
			'tag'=>$tag,
			'exh_type'=>$type,
			'exh_style'=>$style,
			'types'=>TypeExhibition::all(),
			'styles'=>Style::all(),
	 		//дата для определения статуса выставки
	 		'date'=>Carbon::now()
	 	]);
	 } 
	 //динамическая страница выставки
	 public function show_one($id)
	 {	 	
	 	//проверка есть ли ip пользователя в таблице views_exhibitions
	 	if(!(Exhibition::where('id',$id)->firstOrFail()->views_exhibition()->where('visitor',\Request::getClientIp(true))->count()))
	 	{	 		
	 		//создание записи в таблице
	 		Exhibition::where('id',$id)->first()->views_exhibition()->save(new View(['visitor'=>\Request::getClientIp(true)]));
	 		//добавление просмотра к выставке
	 		Exhibition::where('id',$id)->increment('views');
	 	}

	 	return view('pages.one_exhibition',[
	 		//модель выставки
	 		'exh' => Exhibition::where('id',$id)->first(),
	 		//дата для определения статуса выставки
	 		'date'=>Carbon::now(),
	 		//количество добавивших в избранное
	 		'favourite'=>(Auth::check()) ? User::find(Auth::id())->favourites_exhibition()->where('exhibition_id', $id)->count() : 0,
	 		//поставленная пользователем оценка
	 		'rating'=>(Auth::check()) ? (($r=User::find(Auth::id())->rating_for_exhibitions()->where('exhibition_id',$id)->first()) ? $r->pivot->rating : 0) : 0,
	 		//заявка на посещение частной выставки
	 		'request'=>(Auth::check()) ? (($req=User::find(Auth::id())->exhibitions_of_requests()->where('exhibition_id',$id)->first()) ? $req->pivot->response : -1) : -1
	 	]);
	 }
	 //выставление выставке оценки
	 public function rating(Request $request){
	 	//проверка ставил ли пользователь выставке оценку
	 	//обновление оценки, если ставил
	 	//и создание новой записи, если оценки не было
	 	if(User::find(Auth::id())->rating_for_exhibitions()->where('exhibition_id',$request->id)->count())
	 		User::find(Auth::id())->rating_for_exhibitions()->updateExistingPivot($request->id, ['rating'=>$request->rating+1]);
		else
	 	User::find(Auth::id())->rating_for_exhibitions()->attach($request->id, ['rating'=>$request->rating+1]);
	 	//получение модели выставки для обновления полей рейтинга
		$up_r=Exhibition::where('id',$request->id)->first();
		$up_r->ratings=Exhibition::find($request->id)->ratings_exhibition()->sum('rating');
		$up_r->ratings_count=Exhibition::find($request->id)->ratings_exhibition()->count();
		$up_r->save();	 	
		//возвращение нового рейтинга
	 	return response()->json(round($up_r->ratings/$up_r->ratings_count,2), 200);	

	 }
	 
}
