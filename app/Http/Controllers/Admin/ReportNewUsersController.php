<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ReportNewUsersController extends Controller
{
    
    private function getDataArray($items)
    {
        $dateFirstDayOfYear = Carbon::create(Carbon::now()->year, 1, 1);
            $months = $items->get()->groupBy(function($d) {
     return Carbon::parse($d->created_at)->format('m');
 })->toArray();
            $data=[count(array_get($months, $dateFirstDayOfYear->format("m"), []))];
            for ($i=1; $i < Carbon::now()->month; $i++) { 
                $val=array_get($months, $dateFirstDayOfYear->addMonths(1)->format("m"), []);
                $data[]= count($val);
            }
            return $data;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke()
    {
         $allUsers = User::where('created_at','>',Carbon::create(Carbon::now()->year, 1, 1));
            $admins = User::where('role_id', Role::where('name','admin')->first()->id)->where('created_at','>',Carbon::create(Carbon::now()->year, 1, 1));
  
            $reviewers = User::where('role_id', Role::where('name','reviewer')->first()->id)->where('created_at','>',Carbon::create(Carbon::now()->year, 1, 1));

            $authors = User::where('role_id', Role::where('name','author')->first()->id)->where('created_at','>',Carbon::create(Carbon::now()->year, 1, 1));

            $users = User::where('role_id', Role::where('name','user')->first()->id)->where('created_at','>',Carbon::create(Carbon::now()->year, 1, 1));

        $dateFirstDayOfYear = Carbon::create(Carbon::now()->year, 1, 1);
            $monthsAdmin = $admins->get()->groupBy(function($d) {
     return Carbon::parse($d->created_at)->format('m');
 })->toArray();
            $dataAdmin=[count(array_get($monthsAdmin, $dateFirstDayOfYear->format("m"), []))];
            $labels=[$dateFirstDayOfYear->format("F")];
            for ($i=1; $i < Carbon::now()->month; $i++) { 
                $val=array_get($monthsAdmin, $dateFirstDayOfYear->addMonths(1)->format("m"), []);
                $labels[]=$dateFirstDayOfYear->format("F");
                $dataAdmin[]= count($val);
            }

$dataRev = $this->getDataArray($reviewers);
$dataAuthors = $this->getDataArray($authors);
$dataUsers = $this->getDataArray($users);


        return view('pages.admin.reports.new_users',[
	 		'admin' => User::find(Auth::id()),
            'dataAdmin'=> $dataAdmin,
            'labels'=>$labels,
            'dataRev'=> $dataRev,
            'dataAuthors'=> $dataAuthors,
            'dataUsers'=> $dataUsers,
            'allUsers'=>$allUsers->get(),
            'year'=>Carbon::now()->year
	 	]);
    }
}
