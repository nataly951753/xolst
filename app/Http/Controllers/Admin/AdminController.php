<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke()
    {
        $count = Role::find(3)->users_requests()->count() + Role::find(4)->users_requests()->count();
        return view('pages.admin.admin',[
	 		'admin' => Auth::user(),
            'reqs'=>$count
	 	]);
    }
}
