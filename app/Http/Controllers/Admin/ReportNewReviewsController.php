<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Review;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ReportNewReviewsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke()
    {
        $dateFirstDayOfYear = Carbon::create(Carbon::now()->year, 1, 1);
            $revs = Review::where('created_at','>',Carbon::create(Carbon::now()->year, 1, 1));

            $months = $revs->get()->groupBy(function($d) {
     return Carbon::parse($d->created_at)->format('m');
 })->toArray();
            $data=[count(array_get($months, $dateFirstDayOfYear->format("m"), []))];
            $labels=[$dateFirstDayOfYear->format("F")];
            for ($i=1; $i < Carbon::now()->month; $i++) { 
                $val=array_get($months, $dateFirstDayOfYear->addMonths(1)->format("m"), []);
                $labels[]=$dateFirstDayOfYear->format("F");
                $data[]= count($val);
            }
        return view('pages.admin.reports.new_reviews',[
	 		'admin' => User::find(Auth::id()),
            'data'=> $data,
            'labels'=>$labels,
            'revs'=>$revs->get(),
            'year'=>Carbon::now()->year
	 	]);
    }
}
