<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use App\Models\User;

class ChangeRoleRequestsController extends Controller
{
	
	 //просмотр заявок 
	 public function index()
	 {	 
	 	return view('pages.admin.show_requests',[
	 		'admin' => Auth::user(),

	 		'roles' => Role::where('id','>',2)->get()
	 	]);
	 }
	 public function show_one($user_id)
	 {	 
	 	return view('pages.admin.one_request',[
	 		'admin' => Auth::user(),
	 		
	 		'role' => User::find($user_id)->status_requests()->first(),
	 		'user' => User::find($user_id)
	 	]);
	 }
	 //одобрение заявки
	 public function request_approve($user_id,$role_id)
	 {	
	 	$user= 	User::find($user_id);
	 	$user->role_id=$role_id;
	 	$user->save();
		User::find($user_id)->status_requests()->detach($role_id);		 		 		 	
	 	return redirect()->route('role_requests');	    
	 }
	 //отклонение заявки
	 public function request_refuse($user_id,$role_id)
	 {	 	
		User::find($user_id)->status_requests()->detach($role_id);		 		 		 	

	 	return redirect()->route('role_requests');	    
	 }
}
