<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use App\Models\User;
use Validator;
use Illuminate\Support\Facades\Hash;

class CreateAdminController extends Controller
{
	
	 
	 public function index()
	 {	 
	 	return view('pages.admin.show_admins',[
	 		'admin' => Auth::user(),

	 		'admins' => User::where('role_id',1)->latest()->paginate(15)
	 	]);
	 }
	 public function create_admin(Request $request)
	 {	

        $mes = [
    'unique' => 'Користувач з такою електроною поштою вже зареєстрований',];
	 	$validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ], $mes);
        if ($validator->fails())
            return redirect()->route('admins')->withInput()->withErrors($validator);
	 	$user= 	User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
	 	$user->role_id=1;
	 	$user->save();
	 	return redirect()->route('admins');	    
	 }
}
