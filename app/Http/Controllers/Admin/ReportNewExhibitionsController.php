<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Exhibition;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ReportNewExhibitionsController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function __invoke()
    {
        $dateFirstDayOfYear = Carbon::create(Carbon::now()->year, 1, 1);
            $exhs = Exhibition::has('pictures', '>=', config('variables.pictures_count_min'))->where('created_at','>',Carbon::create(Carbon::now()->year, 1, 1));

            $months = $exhs->get()->groupBy(function($d) {
     return Carbon::parse($d->created_at)->format('m');
 })->toArray();
            $data=[count(array_get($months, $dateFirstDayOfYear->format("m"), []))];
            $labels=[$dateFirstDayOfYear->format("F")];
            for ($i=1; $i < Carbon::now()->month; $i++) { 
                $val=array_get($months, $dateFirstDayOfYear->addMonths(1)->format("m"), []);
                $labels[]=$dateFirstDayOfYear->format("F");
                $data[]= count($val);
            }
        return view('pages.admin.reports.new_exhibitions',[
	 		'admin' => User::find(Auth::id()),
            'data'=> $data,
            'labels'=>$labels,
            'exhs'=>$exhs->get(),
            'year'=>Carbon::now()->year
	 	]);
    }
}
