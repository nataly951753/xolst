<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MainMenuController extends Controller
{
    
    public function __invoke()
	 {
	 return view('pages.main_menu', [
	 	"user" => Auth::id() ? Auth::id() : 0
	 ]);
	 }
}
