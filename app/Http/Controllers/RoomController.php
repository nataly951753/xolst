<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Exhibition;
use Illuminate\Support\Facades\Auth;


class RoomController extends Controller
{
    //страница редактирования выставочного зала
     public function index($id)
	 {	 	
	 	$exh=Exhibition::where('id', $id)->first();
	 	//получение коллекции картин выставки
	 	$pictures=$exh->pictures;
	 	//преобразование адреса
	 	$mas=collect();
	 	foreach ($pictures as $picture) {
	 		$mas->push(asset('storage/pictures/'.$picture->url));
	 	}
	 	//приведение к Json
	 	$mas->toJson();

	 	return view('pages.room.room_settings',[
	 		//для сохранения изменений
	 		'route' => route('room_save',['id'=>$id]),
	 		//картины
	 		'textures'=>$mas,
	 		//сохраненные в бд изменения
	 		'scene'=>$exh->code,
	 		'name'=>$exh->name
	 	]);
	 }
	 //страница просмотра зала
	 public function show($id){
	 	$exh=Exhibition::where('id', $id)->first();
	 	$pictures=$exh->pictures;
	 	$mas=collect();
	 	foreach ($pictures as $picture) {
	 		$mas->push(asset('storage/pictures/'.$picture->url));
	 	}
	 	$mas->toJson();
	 	return view('pages.room.room_show',[
	 		'name'=>$exh->name,
	 		'textures'=>$mas,
	 		'scene'=>$exh->code,
	 		'pictures'=>$pictures,
	 		'ex_id'=>$exh->id,
	 		'user_id'=>Auth::id() ? Auth::id() : -1
	 	]);
	 }
	 //сохранение изменений зала
	 public function save($id,Request $request){
	 	$exh=Exhibition::find($id);
	 	$exh->code=$request->code;
	 	$exh->save();
	 	return response()->json(['status' => 'ok']);
	 }
}
