<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Carbon\Carbon;

class AuthorsController extends Controller
{
	//страница "Авторы"
    public function index()
	 {	 	
	 	return view('pages.all_authors',[
	 		//авторы, за которыми закреплены выставки, по 15 на страницу
	 		'authors' => User::has('exhibitions')->paginate(15)
	 	]);
	 } 
	 //динамическая страница автора
	 public function show_one($id)
	 {	
	 	return view('pages.one_author',[
	 		//дата для определения статуса выставок автора
	 		'date'=>Carbon::now(),
	 		//модель автора
	 		'author' => User::where('id',$id)->firstOrFail(),
	 		//количество подписчиков автора
	 		'subscribers'=>User::where('id',$id)->first()->subscribers()->count(),
	 		//подписан ли пользователь на автора
	 		'sub'=>(Auth::check()) ? User::find(Auth::id())->favourites_user_authors()->where('author_id', $id)->count() : 0,
	 	]);
	 }
}
