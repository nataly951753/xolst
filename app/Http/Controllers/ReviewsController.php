<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Review;

class ReviewsController extends Controller
{
	//страница "Рецензии"
    public function index()
	 {	 	
	 	return view('pages.all_reviews',[
	 		//авторы, за которыми закреплены выставки, по 15 на страницу
	 		'reviews' => Review::orderBy('created_at', 'desc')->paginate(15)
	 	]);
	 } 
	 //динамическая страница рецензии
	 public function show_one($id)
	 {	
	 	return view('pages.one_review',[
	 		//модель 
	 		'review' => Review::where('id', $id)->firstOrFail()
	 	]);
	 }
}
