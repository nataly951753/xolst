<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Exhibition;
use App\Models\User;

class PrivateExhibitionController extends Controller
{
	//отправка запроса на посещение
    public function request_add($id)
	 {	 	
	 	if(User::find(Auth::id())->exhibitions_of_requests()->where('exhibition_id',$id)->count()==0)
	 	User::find(Auth::id())->exhibitions_of_requests()->attach($id);		 		 	
	 	return redirect()->route('one_exhibition', ['id' => $id]);	    
	 }
	 //отмена запроса на посещение
	 public function request_delete($id)
	 {	 	
		User::find(Auth::id())->exhibitions_of_requests()->detach($id);
	 	return redirect()->route('one_exhibition', ['id' => $id]);	    
	 }
	 //просмотр заявок автором
	 public function show_requests()
	 {	 
	 	return view('pages.account.show_requests',[
	 		'requests' => Exhibition::where('author_id', Auth::id())->paginate(15)
	 	]);
	 }
	 //одобрение заявки
	 public function request_approve($user_id,$exh_id)
	 {	 	
		User::find($user_id)->exhibitions_of_requests()->updateExistingPivot($exh_id, ['response'=>true]);		 		 		 	
	 	return redirect()->route('show_requests');	    
	 }
	 //отклонение заявки
	 public function request_refuse($user_id,$exh_id)
	 {	 	
		User::find($user_id)->exhibitions_of_requests()->updateExistingPivot($exh_id, ['response'=>false]);		 		 		 	
	 	return redirect()->route('show_requests');	    
	 }
}
