<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Exhibition;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class FavouriteController extends Controller
{
	//добавление автора в Избранное
	 public function favourite_author(Request $request)
	 {	 	
	 	if(User::find(Auth::id())->favourites_user_authors()->where('author_id',$request->id)->count())
		$f=User::find(Auth::id())->favourites_user_authors()->detach($request->id);
	 	else
	 	$f=User::find(Auth::id())->favourites_user_authors()->attach($request->id);
	 	return response()->json($f, 200);	    
	 }
	 	//просмотр избранных авторов
	 public function show_authors()
	 {	 	
	 	return view('pages.account.show_favourites_authors',[
	 		'authors' => User::find(Auth::id())->favourites_user_authors()->latest()->paginate(15),
	 		'author' => User::where('id',Auth::id())->first()
	 	]);
	 }
    //добавление выставки в Избранное
    public function favourite_exhibition(Request $request)
	 {	 		 	
	 	if(User::find(Auth::id())->favourites_exhibition()->where('exhibition_id',$request->id)->count()) 		
				$f=User::find(Auth::id())->favourites_exhibition()->detach($request->id);
			else
	 			$f=User::find(Auth::id())->favourites_exhibition()->attach($request->id);
	 	return response()->json($f, 200);	    
	 }
	 //просмотр избранных выставок
	 public function show_exhibitions()
	 {	 	
	 	return view('pages.account.show_favourite_exhibitions',[
	 		'my_exhibitions' => User::find(Auth::id())->favourites_exhibition()->latest()->paginate(15),
	 		'date'=>Carbon::now(),
	 		'author' => User::where('id',Auth::id())->first()
	 	]);
	 }
	 
}
