<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Picture;
use App\Models\Exhibition;

use Illuminate\Support\Facades\Storage;

class EditPicturesController extends Controller
{
    //страница вывода всех картин выставки
     public function index($id)
	 {
	 	return view('pages.account.show_pictures',[
	 		'id'=>$id,
	 		'pictures' => Exhibition::where('id', $id)->first()->pictures
	 	]);
	 }
	 //страница редактирования информации о картине
	 public function edit($id,$p_id,Request $request)
	 {

	 	return view('pages.account.edit_picture',[
	 		'id'=>$id,
	 		'picture' => Picture::where('id', $p_id)->first()
	 	]);
	 }
	 //сохранение изменений в бд
	 public function save($id,$p_id,Request $request){

	 	$p=Picture::where('id', $p_id)->first();
	 	$p->name=$request->input('name');
	 	$p->description=$request->input('description');
	 	$p->save();
    	return redirect()->route('pictures_show', ['id' => $id]);
	 }
	 //удаление выбранных картин
	 public function delete($id,Request $request){
	 	foreach (Exhibition::where('id', $id)->first()->pictures as $picture) {
	 		if($request->input($picture->id))
	 		{
	 			Storage::delete('public/pictures/'.$picture->url);
	 			(Picture::find($picture->id))->delete();
	 		}
	 	}
    	return redirect()->route('pictures_show', ['id' => $id]);
	 }

}
