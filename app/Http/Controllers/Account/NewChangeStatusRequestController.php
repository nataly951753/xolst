<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Validator;


class NewChangeStatusRequestController extends Controller
{
	//страница редактирования профиля
    public function index()
	 {	 	
	 	return view('pages.account.change_status');
	 }
	 //сохранение информации
	 public function save(Request $request){
	 	 $validator = Validator::make($request->all(), [
        	'message' => ['required']
        ]);

        if ($validator->fails())
        	return view('pages.account.change_status')->withErrors($validator);

    	$role_id=Role::where('name',$request->role)->first()->id;
        $message=$request->input('message');


		if(User::find(Auth::id())->status_requests()->where('role_id',$role_id)->count()==0)
	 	User::find(Auth::id())->status_requests()->attach($role_id, ['message' => $message]);

	    		
    	return redirect()->route('account');

	 	
	 }
}
