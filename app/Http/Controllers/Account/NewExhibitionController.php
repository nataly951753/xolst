<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Validator;
use Illuminate\Http\Request;
use App\Models\Style;
use App\Models\TypeExhibition;
use App\Models\Exhibition;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;



class NewExhibitionController extends Controller
{
	
    //страница создания новой выставки
    public function index()
	 {	 	
	 	return view('pages.account.new_exhibition',[
	 		'styles' => Style::orderBy('name')->get(),
	 		'types' => TypeExhibition::orderBy('name')->get()
	 	]);
	 }
	 //сохранение выставки
	 public function save(Request $request){
        //выставка длится не больше месяца
        //дата открытия не раньше послезавтра
	 	$d=((new Carbon($request->date_started))->addMonth())->toDateTimeString();
        $messages = [
    'after' => 'Поле дата відкриття повинно бути датою після наступного дня.',
    'before' => 'Поле дата закриття повинно бути датою до :date.',
];
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'photo' => ['image'],
        	'about' => ['required', 'string']
            ,
        	'date_started' =>'required|date|after:tomorrow', 
        	'date_finished' =>"required|date|after:date_started|before:$d"  
        ], $messages);
        //при ошибочном вводе вывод ошибки
        if ($validator->fails())
            return redirect()->route('new_exhibition')->withInput()->withErrors($validator);
        //создание выставки
        $photo = $request->file('photo');      
        $exh=new Exhibition();        
        $exh->name=$request->input('name');
        if($request->input('about'))
        	$exh->about=$request->input('about');
        if($request->input('page_description'))
            $exh->description=$request->input('page_description');
        $exh->started_at=$request->input('date_started');
        $exh->finished_at=$request->input('date_finished');
        if($request->input('private'))
        	$exh->private=true;
    	$exh->author_id=Auth::id();
    	$exh->style_id=Style::where('name',$request->style_exhibition)->first()->id;
    	$exh->type_id=TypeExhibition::where('name',$request->type_exhibition)->first()->id;
    	$exh->save();
        if($request->input('tags')) {
            $tags = explode(",", $request->input('tags'));
            foreach ($tags as $t) {
                $tag = Tag::where('name', trim($t))->first();
                if(!$tag) {
                    $tag = new Tag();
                    $tag->name = trim($t);
                    $tag->save();
                }
            $exh->tags()->attach($tag->id);
            }
        }
        if($photo)
        {
            $filename = $exh->id. '.' . $photo->getClientOriginalExtension();
            $exh->photo_url=$filename;
            $photo->storeAs('public/exhibitions', $filename);
        }
        $exh->save();
        
    	return redirect()->route('pictures_for_exhibition', ['id' => $exh->id]);

	 	
	 }
}
