<?php

namespace App\Http\Controllers\Account;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;


class EditAccountController extends Controller
{
	//страница редактирования профиля
    public function index()
	 {	 	
	 	return view('pages.account.edit_account');
	 }
	 //сохранение информации
	 public function save(Request $request){
	 	$validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'photo' => ['image']
        ]);
        if ($validator->fails())
            return redirect()->route('account_edit')->withInput()->withErrors($validator);
    	$photo = $request->file('photo');
        $user=User::find(Auth::id());
        //сохранение фотографии
    	if($photo)
    	{
	    	$filename = Auth::id(). '.' . $photo->getClientOriginalExtension();
	    	$user->photo_url=$filename;
	    	$photo->storeAs('public/users', $filename);
    	}

        $user->name=$request->input('name');
        $user->about=$request->input('about');
        $user->information=$request->input('page_description');

        $user->save();
	    		
    	return redirect()->route('account');

	 	
	 }
}
