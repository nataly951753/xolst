<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Exhibition;
use App\Models\Style;
use App\Models\TypeExhibition;
use App\Models\Tag;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Validator;



class UsersExhibitionsController extends Controller
{
    //страница Мои выставки
    public function index()
	 {	 	
	 	return view('pages.account.show_exhibitions',[
	 		'my_exhibitions' => Exhibition::where('author_id', Auth::id())->latest()->paginate(10),
	 		'date'=>Carbon::now()
	 		
	 	]);
	 }
     //удаление выставки
	 public function delete($id)
	 {
	 	$directory='public/pictures/'.Auth::id().'/'.$id;
		Storage::deleteDirectory($directory);
		(Exhibition::find($id))->delete();
    	return redirect()->route('user_exhibitions');	 		
	 }
     //редактирование информации выставки
	 public function edit($id){
        $tags = Exhibition::where('id', $id)->first()->tags()->get()->implode('name', ', ');

	 	return view('pages.account.edit_exhibition',[
	 		'exhibition'=>Exhibition::where('id', $id)->first(),
	 		'styles' => Style::orderBy('name')->get(),
	 		'types' => TypeExhibition::orderBy('name')->get(),
            'tags' => $tags
	 	]);
	 }
     //сохранение выставки
	  public function save($id,Request $request){

	 	$d=((new Carbon($request->date_started))->addMonth())->toDateTimeString();
        //правила проверки валидатора
        $validator = Validator::make($request->all(), [
        	'name' => ['required', 'string', 'max:255'],
            'photo' => ['image'],
            'about' => ['required', 'string'],
        	// 'date_started' =>'required|date|after:tomorrow', 
        	// 'date_finished' =>"required|date|after:date_started|before:$d"  
        ]);

        if ($validator->fails())
        	return view('pages.account.new_exhibition',[
	 		'styles' => Style::orderBy('name')->get(),
	 		'types' => TypeExhibition::orderBy('name')->get()
	 	])->withErrors($validator);
        $photo = $request->file('photo');
        $exh=Exhibition::where('id', $id)->first();
        if($photo)
        {
            $filename = $exh->id. '.' . $photo->getClientOriginalExtension();
            $exh->photo_url=$filename;
            $photo->storeAs('public/exhibitions', $filename);
        }
        $exh->name=$request->input('name');
         if($request->input('about'))
        	$exh->about=$request->input('about');
        if($request->input('page_description'))
            $exh->description=$request->input('page_description');
        // $exh->started_at=$request->input('date_started');
        // $exh->finished_at=$request->input('date_finished');
        if($request->input('private'))
        	$exh->private=true;
        else
        	$exh->private=false; 
    	$exh->author_id=Auth::id();
    	$exh->style_id=Style::where('name',$request->style_exhibition)->first()->id;
    	$exh->type_id=TypeExhibition::where('name',$request->type_exhibition)->first()->id;
    	$exh->save();
        if($request->input('tags')) {
            $tags = explode(",", $request->input('tags'));
            $exh->tags()->detach();
            foreach ($tags as $t) {
                $tag = Tag::where('name', trim($t))->first();
                if(!$tag) {
                    $tag = new Tag();
                    $tag->name = trim($t);
                    $tag->save();
                }
                $exh->tags()->attach($tag->id);
            }
        }

    	return redirect()->route('edit_exhibition', ['id' => $id]);

	 	
	 }
}
