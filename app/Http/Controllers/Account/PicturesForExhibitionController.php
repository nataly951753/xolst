<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Exhibition;
use App\Models\Picture;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;




class PicturesForExhibitionController extends Controller
{
    //страница добавления картин к выставке
    public function index(string $id)
	 {
	 	return view('pages.account.add_pictures',[
	 		'id'=>$id,
	 		'count'=>Picture::where('exhibition_id', $id)->count()
	 	]);
	 }
	 //сохранение картин
	 public function save(string $id,Request $request){
    	$photos = $request->file('file');
	 //проверка допустимого количества картин
		if(Picture::where('exhibition_id', $id)->count()+count($photos)<=config('variables.pictures_count_max'))
		{
	    	$time = Carbon::now();	 
	        if (!is_array($photos)) {
	            $photos = [$photos];
	        }
	        for ($i = 0; $i < count($photos); $i++) {
	        	//сохранение изображения на сервер
	            $photo = $photos[$i];
	    		$directory = Auth::id() . '/' . $id;
	    		$filename = str_random(5).date_format($time,'d').rand(1,9).date_format($time,'h'). '.' . $photo->getClientOriginalExtension();
	    		$upload_success = $photo->storeAs('public/pictures/' .$directory, $filename);
	    		//сохранение записи в бд
	            $upload = new Picture();
	            $upload->name = $filename;
	            $upload->exhibition_id = $id;
	            $upload->url =$directory . '/' . $filename;
	            $upload->save();
	        }
	     	if ($upload_success) {
	        	return response()->json($upload_success, 200);
	    	}
		    
		}
	        	return response()->json('error', 400);

	}

	 
}
