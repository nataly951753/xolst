<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Review;
use App\Models\Exhibition;
use Illuminate\Support\Facades\Auth;
use Validator;



class UsersReviewsController extends Controller
{
    //страница Мои рецензии
    public function index()
	 {	 	
	 	return view('pages.account.show_reviews',[
	 		'reviews' => User::find(Auth::id())->reviews()->latest()->paginate(10)
	 	]);
	 }
     //удаление рецензии
	 public function delete($id)
	 {
		(Review::find($id))->delete();
    	return redirect()->route('user_reviews');	 		
	 }

	 public function create($id){
	 	return view('pages.account.create_review');
	 }
	  public function save($id, Request $request){
        //правила проверки валидатора
        $validator = Validator::make($request->all(), [
        	'name' => ['required', 'string', 'max:255'],
          'about' => ['required'],
          'page_description' => ['required'],
        ]);

        if ($validator->fails())
        	return view('pages.account.create_review')->withErrors($validator);
        $author = User::find(Auth::id());
        $review = new Review();
        $review->name = $request->input('name');
        $review->about = $request->input('about');
        $review->description = $request->input('page_description');
        $review->author()->associate($author);
        Exhibition::where('id', $id)->firstOrFail()->reviews()->save($review);
    	  return redirect()->route('one_review', ['id' => $review->id]);
	 }
   public function edit($id){
    return view('pages.account.create_review', [
      'review'=>Review::where('id',$id)->firstOrFail()
    ]);
   }
   public function save_changes($id, Request $request){
        //правила проверки валидатора
        $validator = Validator::make($request->all(), [
          'name' => ['required', 'string', 'max:255'],
          'about' => ['required'],
          'page_description' => ['required'],
        ]);

        if ($validator->fails())
          return view('pages.account.create_review')->withErrors($validator);
        $review = Review::where('id',$id)->first();
        $review->name = $request->input('name');
        $review->about = $request->input('about');
        $review->description = $request->input('page_description');
        $review->save();
        return redirect()->route('user_reviews');
   }
}
