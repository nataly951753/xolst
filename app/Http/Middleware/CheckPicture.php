<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Picture;

class CheckPicture
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($e=Picture::where('id',$request->p_id)->first()) {
            if($e->exhibition_id==$request->id)
                return $next($request);
        }
            return redirect('account');
    }
}
