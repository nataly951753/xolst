<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;

use Closure;

class CheckAuthorRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
                if ($e=User::where('id',Auth::id())->first()) {
            if($e->role_id == Role::where('name','author')->first()->id)
        return $next($request);
                
        }
    return redirect('/account');
    }
}
