<?php

namespace App\Http\Middleware;

use Closure;
use Carbon\Carbon;
use App\Models\Exhibition;
use Illuminate\Support\Facades\Auth;



class CheckStartedAt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $exh=Exhibition::where('id',$request->id)->firstOrFail();
        if (Carbon::now()->between($exh->started_at, $exh->finished_at) || $exh->author_id==Auth::id()) {
                return $next($request);
        }
            return redirect()->route('all_exhibitions');
    }
}
