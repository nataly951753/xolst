<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Exhibition;
use Illuminate\Support\Facades\Auth;


class CheckExhibition
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

         if ($e=Exhibition::where('id',$request->id)->first()) {
            if($e->author_id==Auth::id())
                return $next($request);
        }
            return redirect('account');

    }
}
