<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Exhibition;
use Illuminate\Support\Facades\Auth;



class CheckCountPictures
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (Exhibition::where('id',$request->id)->first()->pictures->count()>=config('variables.pictures_count_min')) {
                return $next($request);
        }
            return redirect()->route('pictures_for_exhibition',['id'=>$request->id]);
    }
}
