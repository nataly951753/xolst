<?php

namespace App\Http\Middleware;
use App\Models\Exhibition;
use Illuminate\Support\Facades\Auth;

use Closure;

class CheckExhibitionPrivate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Exhibition::find($request->id)->private!==1 || Exhibition::find($request->id)->author->id ==Auth::id())return $next($request);
         if (Exhibition::find($request->id)->requests_exhibition()->where('user_id',Auth::id())->where('response',true)->first()) {
                return $next($request);
        }
            return redirect()->route('one_exhibition',['id'=>$request->id]);
    }
}
