<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Exhibition extends Model
{
    use Searchable;
    
    protected $dates = ['started_at', 'finished_at'];
    /**
     * Получить индексируемый массив данных для модели.
     *
     * @return array
     */
    public function toSearchableArray()
    {
      $array = $this->toArray();
  
      // Изменение массива...
  
      return array('id' => $array['id'],'name' => $array['name'], 'about' => $array['about']);
    }
    public function author()
    {
        return $this->belongsTo(User::class);
    }
    public function style()
    {
        return $this->belongsTo(Style::class);
    }
    public function type()
    {
        return $this->belongsTo(TypeExhibition::class);
    }
    public function pictures()
    {
        return $this->hasMany(Picture::class);
    }
    public function reviews()
    {
        return $this->hasMany(Review::class);
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'tags_to_exhibitions', 'exhibition_id');
    }
    public function favourites()
    {
        return $this->belongsToMany(User::class, 'favourites_exhibitions', 'exhibition_id');
    }
    public function views_exhibition()
    {
        return $this->hasMany(View::class);
    }
    public function ratings_exhibition()
    {
        return $this->belongsToMany(User::class, 'ratings_exhibition', 'exhibition_id')->withPivot('rating', 'id');
    }

    public function requests_exhibition()
    {
        return $this->belongsToMany(User::class, 'request_exhibition', 'exhibition_id')->withPivot('response', 'id');
    }
}
