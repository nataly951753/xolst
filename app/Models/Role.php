<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //
      public function users()
    {
        return $this->hasMany(User::class);
    }

     public function users_requests()
    {
        return $this->belongsToMany(User::class, 'change_role_requests', 'role_id')->withPivot('message','id')->withTimestamps();
    }
}
