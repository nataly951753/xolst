<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    //
    public function exhibition()
    {
        return $this->belongsTo(Exhibition::class);
    }
    public function author()
    {
        return $this->belongsTo(User::class);
    }
}
