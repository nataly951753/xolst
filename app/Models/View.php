<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class View extends Model
{
	protected $table = 'views_exhibition';
	protected $fillable = [
        'visitor'
    ];

   public function exhibition()
    {
        return $this->belongsTo(Exhibition::class);
    }
}
