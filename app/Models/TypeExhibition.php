<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TypeExhibition extends Model
{
	protected $table = 'types_exhibitions';

	public function exhibitions()
    {
        return $this->hasMany(Exhibition::class);
    }
    
    //
}
