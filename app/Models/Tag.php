<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    public function exhibitions()
    {
        return $this->belongsToMany(Exhibition::class, 'tags_to_exhibitions', 'tag_id');
    }
}
