<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
     public function role()
    {
        return $this->belongsTo(Role::class);
    }
    public function reviews()
    {
        return $this->hasMany(Review::class,'author_id');
    }
    public function exhibitions()
    {
        return $this->hasMany(Exhibition::class,'author_id');
    }
    public function favourites_exhibition()
    {
        return $this->belongsToMany(Exhibition::class,'favourites_exhibitions', 'user_id');
    }
    public function favourites_user_authors()
    {
        return $this->belongsToMany(User::class,'favourites_authors', 'user_id', 'author_id');
    }
    public function subscribers()
    {
        return $this->belongsToMany(User::class,'favourites_authors', 'author_id', 'user_id');
    }
    public function rating_for_exhibitions()
    {
        return $this->belongsToMany(Exhibition::class, 'ratings_exhibition', 'user_id')->withPivot('rating');
    }
    public function exhibitions_of_requests()
    {
        return $this->belongsToMany(Exhibition::class, 'request_exhibition', 'user_id')->withPivot('response', 'id');
    }

      public function status_requests()
    {
        return $this->belongsToMany(Role::class, 'change_role_requests', 'user_id')->withPivot('message','id')->withTimestamps();
    }
}
