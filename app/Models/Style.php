<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Style extends Model
{
    //
    public function exhibitions()
    {
        return $this->hasMany(Exhibition::class);
    }
    
}
