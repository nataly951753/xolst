<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    //
    public function exhibition()
    {
        return $this->belongsTo(Exhibition::class);
    }
}
