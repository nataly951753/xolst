<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Exhibition;
use App\Mail\FavoriteExhibitionStartedMail;
use Carbon\Carbon;
use Mail;


class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendEmails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send emails with favorite exhibition started';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Exhibition::whereDate('started_at', Carbon::now())->get()->each(function($exhibition){
            $exhibition->favourites()->each(function($user) use ($exhibition){
                Mail::to($user)
                     ->queue(new FavoriteExhibitionStartedMail($exhibition));
                     $this->info("Отправлено $user->email");
            });
        });
    }
}
