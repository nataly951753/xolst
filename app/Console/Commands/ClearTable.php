<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\View;
use App\Models\Exhibition;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;



class ClearTable extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clearTable';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'For clearing table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        View::truncate();

        Exhibition::where('finished_at','<',Carbon::now())->get()->each(function($ex){
            Storage::deleteDirectory('public/pictures/'.$ex->author->id.'/'.$ex->id);
            $ex->pictures()->delete();
        });

    }
}
