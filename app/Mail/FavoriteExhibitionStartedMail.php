<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Exhibition;

class FavoriteExhibitionStartedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
   * Экземпляр выставки.
   *
   * @var Exhibition
   */
    public $exhibition;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Exhibition $exhibition)
    {
        $this->exhibition = $exhibition;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $exh = ($this->exhibition)->name;
        return $this->view('emails.favorite_exhibition_started')
                    ->subject("Выставка $exh открылась!");
    }
}
