
function initRenderer(additionalProperties) {

    var props = (typeof additionalProperties !== 'undefined' && additionalProperties) ? additionalProperties : {
                        alpha: !0,
                        antialias: !0};
    var renderer = new THREE.WebGLRenderer(props);
    renderer.shadowMap.enabled = true;
    renderer.shadowMapSoft = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    renderer.setClearColor(new THREE.Color(0x000000));
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
   $("#webgl-output").append(renderer.domElement);

    return renderer;
}


function pointLight(scene,position,color,intensity){
  var pointLight = new THREE.PointLight(color,intensity);
  pointLight.position.copy(position  );
  pointLight.castShadow = true;
  pointLight.name="lamp"+(scene.children.length-1);
  scene.add(pointLight);
}
function initLighting(scene, initialPosition,roomWidth,roomHeight,roomDepth) {
    var position = (initialPosition !== undefined) ? initialPosition : new THREE.Vector3(-10, 30, 40);
    
    
  var pointLight = new THREE.PointLight(0xffffff,1);//цвет и интенсивность
  pointLight.position.copy(position  );
  pointLight.castShadow = true;

  scene.add(pointLight);

    var ambientLight = new THREE.AmbientLight(0x343434,1.3);
    ambientLight.name = "ambientLight";
    scene.add(ambientLight);
    
}

function  initPersonControls(camera)  {
     var fpControls = new THREE.FirstPersonControls(camera);
  fpControls.lookSpeed = 0.03;
  fpControls.movementSpeed = 100;
  fpControls.lookVertical = true;
  fpControls.constrainVertical = true;
  fpControls.verticalMin = 1.0;
  fpControls.verticalMax = 2.0;
  fpControls.lon = 180;//куда направлена камера
  fpControls.lat = 0;//когда сцена отражается в первый раз
  fpControls.enabled=false;
  

  return fpControls;

}


  