
  var camera, moveControls;
  var renderer;
  var scene;
  var clock = new THREE.Clock();
  var raycaster;
  var mouse;
  var skyBox;
  var light1, light2, light3, light4, light5, light6;
  var text;
  var loader;
  var audioLoader;
  var listener;
  var isSelect = false;
  var isSelectLoopMode = false;
  var selectObject=null;
  //y - rotation Y
  var pagePosition = {
  page_first: {x:-2.5, y:-0.5, z:4.3, tX:-1, tZ:2, title:"РЕЦЕНЗІЇ"},
  page_second: {x:-2.5, y:0.5, z:-4.3,  tX:-1, tZ:-2, title:"О Xolst"},
  page_third: {x:-5, y:1.6, z:0, tX:-2.2, tZ:0, title:"ВИСТАВКИ"},
  page_four: {x:2.5, y:0.5, z:4.3, tX:1, tZ:2, title:"АВТОРИ"},
  page_five_register: {x:2.5, y:-0.5, z:-4.3, tX:1, tZ:-2, title:"ПРИЄДНАТИСЬ"},
  page_five_cabinet: {x:2.5, y:-0.5, z:-4.3, tX:1, tZ:-2, title:"ОСОБИСТИЙ КАБІНЕТ"},  
  page_six_login: {x:5, y:1.6, z:0, tX:2.2, tZ:0, title:"УВІЙТИ"},
  page_six_exit: {x:5, y:1.6, z:0, tX:2.5, tZ:0, title:""},
  };
  var pageTitle;
  var mouseX = 0, mouseY = 0;
      var windowHalfX = window.innerWidth / 2;
      var windowHalfY = window.innerHeight / 2;

      function init() {
        var container = document.getElementById( 'webgl-output' );
        renderer = initRenderer();
        container.appendChild( renderer.domElement );

        scene = new THREE.Scene();
        raycaster = new THREE.Raycaster();
        mouse = new THREE.Vector2();

        camera = new THREE.PerspectiveCamera( 90, window.innerWidth / window.innerHeight, 0.1, 100 );
        camera.position.z = 0.01;
camera.lookAt(-6,0.5,-4.3);
        moveControls = initPersonControls(camera);
        moveControls.enabled = false;

        audioLoader = new THREE.AudioLoader();
        listener = new THREE.AudioListener();
        camera.add( listener );


        var blocker = document.getElementById( 'blocker' );
        pageTitle = document.getElementById( 'page_title' );
        blocker.addEventListener( 'click', function () {
                document.addEventListener( 'mousemove', onDocumentMouseMove, false );
                document.addEventListener( 'mouseup', onDocumentMouseUp, false );
          blocker.style.display = 'none';
          moveControls.enabled=true;
          start_back_sound();          
        }, false );


        var textures = getTexturesFromAtlasFile( xost + "/images/cloud.jpg", 6 );
        var materials = [];
        for ( var i = 0; i < 6; i ++ ) {
          materials.push( new THREE.MeshBasicMaterial( { map: textures[ i ] } ) );
        }

        skyBox = new THREE.Mesh(new THREE.BoxBufferGeometry( 30, 30, 30 ), materials );
        skyBox.geometry.scale( 1, 1, - 1 );
        scene.add( skyBox );

        add_page("/images/reviews.PNG", pagePosition.page_first, 'page_first');
        add_box(-2.6, -0.5, 4.5);
        add_page("/images/about.PNG", pagePosition.page_second,  'page_second');
        add_box(-2.6, 0.5, -4.5);
        add_page("/images/exhibitions.PNG", pagePosition.page_third, 'page_third');
        add_box(-5.2, 1.6, 0);
        add_page("/images/authors.PNG",  pagePosition.page_four, 'page_four');
        add_box(2.6, 0.5, 4.5);
        if(!user) {
          add_page("/images/register.PNG",  pagePosition.page_five_register, 'page_five_register');
          add_page("/images/login.PNG",  pagePosition.page_six_login, 'page_six_login');
        } else {
          add_page("/images/cabinet.PNG", pagePosition.page_five_cabinet, 'page_five_cabinet');
          add_page("/images/exit.png",  pagePosition.page_six_exit, 'page_six_exit');
        }
        add_box(2.6, -0.5, -4.5);
        add_box(5.2, 1.6, 0);

        pointLight = new THREE.PointLight( 0xffffff, 1.5 );
        pointLight.position.set( 0, -4, 8 );
        pointLight.intensity = 4;
        pointLight.decay = 1;
        pointLight.distance = 10;
        scene.add( pointLight );

        var intensity = 1.0;
        var distance = 12;
        var decay = 1.0;
        var c1 = "#151515", c2 = "#f1f1f1", c3 = "#41B3A3", c4 = "#182628", c5 = "#303c6c", c6 = "#05386B";
        light1 = new THREE.PointLight( c1, intensity, distance, decay );
        scene.add( light1 );
        light2 = new THREE.PointLight( c2, intensity, distance, decay );
        scene.add( light2 );
        light3 = new THREE.PointLight( c3, intensity, distance, decay );
        scene.add( light3 );
        light4 = new THREE.PointLight( c4, intensity, distance, decay );
        scene.add( light4 );
        light5 = new THREE.PointLight( c5, intensity, distance, decay );
        scene.add( light5 );
        light6 = new THREE.PointLight( c6, intensity, distance, decay );
        scene.add( light6 );
        window.addEventListener( 'resize', onWindowResize, false );
}

      function start_back_sound() {
        audioLoader.load( 'audio/background.mp3', function( buffer ) {
        // create a global audio source
        var sound = new THREE.Audio( listener );
          sound.setBuffer( buffer );
          sound.setLoop( true );
          sound.setVolume( 0.3 );
          sound.play();
        });
      }

      function add_page(url, pagePosition, name ) {
          let texture_page = new THREE.TextureLoader().load( xost + url);
          let page = new THREE.Mesh(new THREE.BoxBufferGeometry( 5, 2.5, 1 ), 
                                    new THREE.MeshBasicMaterial({
                                    map: texture_page}) 
                                    );
          page.position.z = pagePosition.z;
          page.position.x = pagePosition.x;
          page.rotation.y = pagePosition.y;
          page.name = name;
          scene.add(page);
          return page;          
        }

        function add_box(x, y, z) {
          let page = new THREE.Mesh(new THREE.BoxBufferGeometry( 5.3, 2.7, 1 ), 
                                    new THREE.MeshPhongMaterial({color:0xffffff}) 
                                    );
          page.position.z = z;
          page.position.x = x;
          page.rotation.y = y;
          scene.add(page);
          return page;          
        }

        function onDocumentMouseMove( event ) {
            mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
            raycaster.setFromCamera( mouse, camera );
            var intersects = raycaster.intersectObjects(scene.children);
            if ( intersects.length > 0 ) {
              var intersect = intersects[ 0 ];
              if(intersect.object.name=="page_first" || intersect.object.name=="page_second"
                || intersect.object.name=="page_third" || intersect.object.name=="page_four"
                || intersect.object.name=="page_five_register" || intersect.object.name=="page_six_login"
                || intersect.object.name=="page_five_cabinet" || intersect.object.name=="page_six_exit"){
              document.body.style.cursor="pointer";
              if(!isSelect) {
              audioLoader.load( 'audio/select.wav', function( buffer ) {
              // create a global audio source
              var sound = new THREE.Audio( listener );
                sound.setBuffer( buffer );
                sound.setVolume( 0.3 );
                sound.play();
              });
              isSelect = true;
              }
              if(!isSelectLoopMode && mouse.x > -0.02 && mouse.x < 0.02 && mouse.y > -0.02 && mouse.y < 0.02) {
              // console.log("yes");
              isSelectLoopMode = true;
              moveControls.enabled = false;
              selectObject = intersect.object;
              let objPos = pagePosition[selectObject.name];
              let position = { x : selectObject.position.x, z: selectObject.position.z};
              let target = { x : objPos.tX, z: objPos.tZ };
              camera.lookAt( selectObject.position);
              pageTitle.style.display = 'flex';

              // var textWrapper = document.querySelector('.ml3');
               var textWrapper3 = document.querySelector('.ml3');
              textWrapper3.innerHTML=objPos.title;

              textWrapper3.innerHTML = textWrapper3.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

            anime.timeline({loop: true})
              .add({
                targets: '.ml3 .letter',
                opacity: [0,1],
                easing: "easeInOutQuad",
                duration: 2250,
                delay: function(el, i) {
                  return 550 * (i+1)
                }
              }).add({
                targets: '.ml3',
                opacity: 0,
                duration: 1000,
                easing: "easeOutExpo",
                delay: 4000
              });
              
              let tween = new TWEEN.Tween(position).to(target, 2000);
              tween.onUpdate(function(){
                  selectObject.position.x = position.x;
                  selectObject.position.z = position.z;
              }); 
              tween.start();
              }
              if (isSelectLoopMode && selectObject.name == intersect.object.name) {
                mouseX = ( event.clientX - windowHalfX );
                mouseY = ( event.clientY - windowHalfY );
              }


              }
              else {
                document.body.style.cursor="default";
                if(isSelect) isSelect = false;
                if(isSelectLoopMode) {
                  isSelectLoopMode = false;
                  moveControls.enabled = true;
                  pageTitle.style.display = 'none';

                  let objPos = pagePosition[selectObject.name];
                  let position = { x : selectObject.position.x, z: selectObject.position.z};
                  let target = { x : objPos.x, z: objPos.z };
                  let tween = new TWEEN.Tween(position).to(target, 2000);
                  tween.onUpdate(function(){
                      selectObject.position.x = position.x;
                      selectObject.position.z = position.z;
                  }); 
                  tween.start();
                  selectObject.rotation.x=0;
                  selectObject.rotation.y=objPos.y;
                  selectObject.rotation.z=0;
                }
              }
            }

        }

      function lookAtPage() {
        if(selectObject.rotation.y>pagePosition[selectObject.name].y-0.2 
          && selectObject.rotation.y<pagePosition[selectObject.name].y+0.2
          && selectObject.rotation.y + ( mouseX - camera.position.x ) * 0.000001 > pagePosition[selectObject.name].y-0.2
          &&selectObject.rotation.y + ( mouseX - camera.position.x ) * 0.000001 < pagePosition[selectObject.name].y+0.2)
        selectObject.rotation.y += ( mouseX - camera.position.x ) * 0.000001;
        if(selectObject.rotation.x > -0.4 && selectObject.rotation.x < 0.4 &&
          selectObject.rotation.x + ( - mouseY - camera.position.y ) * 0.000001 > -0.4 &&
          selectObject.rotation.x + ( - mouseY - camera.position.y ) * 0.000001 < 0.4)
          selectObject.rotation.x += ( - mouseY - camera.position.y ) * 0.000001;
      }

      function onDocumentMouseUp( event ) {
        mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
        raycaster.setFromCamera( mouse, camera );
        var intersects = raycaster.intersectObjects(scene.children);
        if ( intersects.length > 0 ) {
          var intersect = intersects[ 0 ];
          let isLocation = false;

          if(intersect.object.name=="page_first") {
            document.location.href= xost + "/reviews/all";
            isLocation = true;
          }
          else if(intersect.object.name=="page_second") {
            document.location.href= xost;

            isLocation = true;
          }
          else if(intersect.object.name=="page_third") {
            document.location.href= xost + "/exhibitions";
            isLocation = true;
          }
          else if(intersect.object.name=="page_four"){
            document.location.href= xost + "/authors/all";
            isLocation = true;
          }
          else if(intersect.object.name=="page_five_register"){
            document.location.href= xost + "/register";
            isLocation = true;
          }
          else if(intersect.object.name=="page_six_login"){
            document.location.href= xost + "/login";
            isLocation = true;
          }
          else if(intersect.object.name=="page_five_cabinet"){
            document.location.href= xost + "/account";
            isLocation = true;
          }
          else if(intersect.object.name=="page_six_exit") {
            event.preventDefault(); 
            document.getElementById('logout-form').submit();
            isLocation = true;
          }
          if(isLocation) {
            $("#p_prldr").css("display", "flex");
              $svg_anm   = $("#p_prldr").find('.svg_anm');
              $svg_anm.fadeIn();
          }

        }

     }


      function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {
        var textures = [];
        for ( var i = 0; i < tilesNum; i ++ ) {
          textures[ i ] = new THREE.Texture();
        }
        var imageObj = new Image();
        imageObj.onload = function () {
          var canvas, context;
          var tileWidth = imageObj.height;
          for ( var i = 0; i < textures.length; i ++ ) {
            canvas = document.createElement( 'canvas' );
            context = canvas.getContext( '2d' );
            canvas.height = tileWidth;
            canvas.width = tileWidth;
            context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
            textures[ i ].image = canvas;
            textures[ i ].needsUpdate = true;
          }
        };
        imageObj.src = atlasImgUrl;
        return textures;
      }



      function onWindowResize() {
        windowHalfX = window.innerWidth / 2;
        windowHalfY = window.innerHeight / 2;
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
      }

      function animate() {
        requestAnimationFrame( animate );
        skyBox.rotation.y+=0.0002;
        skyBox.rotation.x+=0.0002;
        skyBox.rotation.z+=0.0002;
        TWEEN.update();
        var time = Date.now() * 0.00025;
        var d = 5;
        light1.position.x = Math.sin( time * 0.7 ) * d;
        light1.position.z = Math.cos( time * 0.3 ) * d;
        light2.position.x = Math.cos( time * 0.3 ) * d;
        light2.position.z = Math.sin( time * 0.7 ) * d;
        light3.position.x = Math.sin( time * 0.7 ) * d;
        light3.position.z = Math.sin( time * 0.5 ) * d;
        light4.position.x = Math.sin( time * 0.3 ) * d;
        light4.position.z = Math.sin( time * 0.5 ) * d;
        light5.position.x = Math.cos( time * 0.3 ) * d;
        light5.position.z = Math.sin( time * 0.5 ) * d;
        light6.position.x = Math.cos( time * 0.7 ) * d;
        light6.position.z = Math.cos( time * 0.5 ) * d;
        if(isSelectLoopMode) {
          lookAtPage();
        }
        moveControls.update(clock.getDelta());
        renderer.render( scene, camera );
      }
  

