var camera, renderer, scene, raycaster, mouse;
var clock = new THREE.Clock();
var loader, audioLoader, listener;
var mouseX = 0, mouseY = 0;
var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
var elemPosition = [{x:0,z:-2.5},
					{x:-5,z:-5},
					{x:5,z:-5}];
var searches = [];
var page = 1;
var query = "";
var current_element = 0;
var isSelect = false, isSelectOtherObj = false, isStart=false, isLoopClick = false, isRotate = true;
var isSelectLoopMode = false;
var selectObject=null;

var skyBox, loop, torusKnot;

function init() {
	var container = document.getElementById( 'webgl-output' );
	renderer = initRenderer();
	container.appendChild( renderer.domElement );

	scene = new THREE.Scene();
	raycaster = new THREE.Raycaster();
	mouse = new THREE.Vector2();

	camera = new THREE.PerspectiveCamera( 90, window.innerWidth / window.innerHeight, 0.1, 100 );
	camera.position.z = 0.01;

	audioLoader = new THREE.AudioLoader();
	listener = new THREE.AudioListener();
	camera.add( listener );

	var textures = getTexturesFromAtlasFile( xost + "/images/cloud.jpg", 6 );
	var materials = [];
	for ( var i = 0; i < 6; i ++ ) {
	  materials.push( new THREE.MeshBasicMaterial( { map: textures[ i ] } ) );
	}

	skyBox = new THREE.Mesh(new THREE.BoxBufferGeometry( 30, 30, 30 ), materials );
	skyBox.geometry.scale( 1, 1, - 1 );
	scene.add( skyBox );

	pointLight = new THREE.PointLight( 0xffffff, 1.5 );
	pointLight.position.set( 0, -4, 8 );
	pointLight.intensity = 4;
	pointLight.decay = 1;
	pointLight.distance = 10;
	scene.add( pointLight );

	add_loop_object();

	window.addEventListener( 'resize', onWindowResize, false );

	var blocker = document.getElementById( 'blocker' );
	pageTitle = document.getElementById( 'page_title' );
	blocker.addEventListener( 'click', function () {
	        document.addEventListener( 'mousemove', onDocumentMouseMove, false );
	        document.addEventListener( 'mouseup', onDocumentMouseUp, false );
	    	blocker.style.display = 'none';
	  		start_back_sound();
	}, false );

	$("button").on('click', function (e) {
	    query = $("input[type=text]").val();
	    if(query) {
	        search_ajax("/search");
	        $("input[type=text]").val("");
	        $( ".input-search" ).fadeTo( "slow" , 0);
	        let tween = tweenScaleElement(torusKnot, {x:0,y:0,z:0}, 500);
			tween.onComplete(()=>{
				scene.remove(torusKnot);
		        scene.add(loop);
		        let tween = tweenScaleElement(loop, {x:1,y:1,z:1}, 300);
				tween.start();
		    });
			tween.start();
		}
	});
	$(".fa-chevron-right").on('click', () => onPaginateClick(page + 1));
	$(".fa-chevron-left").on('click', () => onPaginateClick(page - 1));
	$("#search").on('click', function (e) {
	 	e.preventDefault();
	 	isLoopClick = true;
		clearScene();
	    searches = [];
	    if (scene.getObjectByName("loop")) scene.remove(loop);
	    if (!scene.getObjectByName("torusKnot"))
		    scene.add(torusKnot);
	    animateTorusKnot();
	});
}

function onPaginateClick(nextPage) {
	search_ajax('/search?page=' + nextPage);
	clearScene();
	scene.add(loop);
    let tween = tweenScaleElement(loop, {x:1,y:1,z:1}, 300);
	tween.start();
}

function clearScene() {
	for (var i = 0; i < searches.length; i++) {
		scene.remove(scene.getObjectByName(searches[i].name));
	}
	loop.rotation.set(0,0,-0.5);
	$(".paginate").css("display", "none");   
    $(".search-no-content").css("display", "none");
    $(".search-exh-title").css("display", "none");
    if($(".search-exh-info").css("opacity") == 1)
	  	setOpacityElement(".search-exh-info",1,0);
}

function tweenScaleElement(element, target, time) {
	let scales = {x:element.scale.x,
				  y:element.scale.y,
				  z:element.scale.z};
	let tween = new TWEEN.Tween(scales).to(target, time).easing(TWEEN.Easing.Quadratic.Out);
		tween.onUpdate(function(){
			element.scale.set(scales.x, scales.y, scales.z);
		}); 
	return tween;
}

function add_torus_knot() {
	let torusGeometry = new THREE.TorusKnotGeometry(10,20,250,140, 3,7,3);
	torusKnot = createParticleSystem(torusGeometry);
	torusKnot.position.z = -10;
	torusKnot.scale.set(0,0,0);
	torusKnot.name = "torusKnot";
	scene.add(torusKnot);
	animateTorusKnot();
}

function animateTorusKnot() {
	let tween = tweenScaleElement(torusKnot, {x:1,y:1,z:0.7}, 1000);
	tween.onComplete(()=>{
    	$( ".input-search" ).css("display", "flex");
		if($(".input-search").css("opacity") == 0)        	
			setOpacityElement(".input-search",0,1);
    });
	tween.start();
}

function add_loop_object() {

	let loopGeometry = new THREE.Geometry();

	let geometryTorus = new THREE.TorusGeometry( 2, 0.5, 32, 100 );
	let geometryCylinder = new THREE.CylinderGeometry( 0.5, 0.4, 2.5, 32, 32 );

	let boxMesh = new THREE.Mesh(geometryTorus);
	let sphereMesh = new THREE.Mesh(geometryCylinder);

	sphereMesh.position.x = 3.5;
	sphereMesh.rotation.z = 1.55;

	// boxMesh.updateMatrix(); // as needed
	loopGeometry.merge(boxMesh.geometry, boxMesh.matrix);

	sphereMesh.updateMatrix(); // as needed
	loopGeometry.merge(sphereMesh.geometry, sphereMesh.matrix);

	loop = createParticleSystem(loopGeometry);

	loop.position.set(-1,0.5,-6);
	loop.rotation.z = -0.5;
	loop.name = "loop";

	scene.add( loop );
}

// from THREE.js examples
function generateSprite() {

    var canvas = document.createElement('canvas');
    canvas.width = 16;
    canvas.height = 16;

    var context = canvas.getContext('2d');
    var gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2);
    gradient.addColorStop(0, 'rgba(255,255,255,1)');
    gradient.addColorStop(0.2, 'rgba(0,255,255,1)');
    gradient.addColorStop(0.4, 'rgba(0,0,64,1)');
    gradient.addColorStop(1, 'rgba(0,0,0,1)');

    context.fillStyle = gradient;
    context.fillRect(0, 0, canvas.width, canvas.height);

    var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;
    return texture;

}

function createParticleSystem(geom) {
    var material = new THREE.PointsMaterial({
        color: 0xffffff,
        size: 0.1,
        transparent: true,
        blending: THREE.AdditiveBlending,
        map: generateSprite()
    });

    var system = new THREE.Points(geom, material);
    system.sortParticles = true;
    return system;
}

function addInfoToExhibition() {
	// Wrap every letter in a span
	var textWrapper = document.querySelector('.ml16');
	textWrapper.innerHTML=searches[current_element].name;
	textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");
	$(".search-exh-title").css("display", "flex");
	anime.timeline({loop: true})
	  .add({
	    targets: '.ml16 .letter',
	    translateY: [-100,0],
	    easing: "easeOutExpo",
	    duration: 2400,
	    delay: (el, i) => 30 * i
	  }).add({
	    targets: '.ml16',
	    opacity: 0,
	    duration: 1000,
	    easing: "easeOutExpo",
	    delay: 1000
	  });
	//date for exh  
	let finish=new Date(searches[current_element].finished_at);
	let start=new Date(searches[current_element].started_at);
	let today=Date.now();
	$(".badge").css("display", "none");
	if (today <=finish && today >= start)
		$(".exh-open").css("display", "inline-block");
	else if (today < start)
		$(".exh-future").css("display", "inline-block");
	else
		$(".exh-last").css("display", "inline-block");
	if (searches[current_element].about)
		$(".about").text(searches[current_element].about);
	else
		$(".about").text("Нет информации...");

	setOpacityElement(".search-exh-info", 0, 1);
  
}

function setOpacityElement(element, start, end) {
	let opacity={x:start};
    let tween = new TWEEN.Tween(opacity).to({x:end}, 3000).easing(TWEEN.Easing.Quadratic.Out);
	tween.onUpdate(function(){
      $(element).css("opacity", opacity.x);
	}); 
    tween.start();
}

function onDocumentMouseUp( event ) {
    mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects(scene.children);
    if ( intersects.length > 0 ) {
    	var intersect = intersects[ 0 ];
        if (intersect.object.name=="loop" && !isStart){
		  	let tween = tweenScaleElement(loop, {x:0,y:0,z:0}, 300); 
			tween.onComplete(()=>{
				scene.remove(loop);
				defaultModeForElementOn();
				isStart = true;
				$(".menu-search").css("display", "flex");
				add_torus_knot();			
		    });
			tween.start();
		}
		for (var i = 0; i < searches.length; i++) {
		    if(intersect.object.name==searches[i].name) {
		    	if (intersect.object.name==searches[current_element].name) {
		    		//переход на страницу выставки
		        	document.location.href= xost + "/exhibition/" + searches[i].id;
		            $("#p_prldr").css("display", "flex");
		            $svg_anm   = $("#p_prldr").find('.svg_anm');
		    		$svg_anm.fadeIn();
		    	} else {
		      		//смена элементов
		      		audioLoader.load( 'audio/move.mp3', function( buffer ) {
				        // create a global audio source
				    	var sound = new THREE.Audio( listener );
				        sound.setBuffer( buffer );
				        sound.setVolume( 0.5 );
				        sound.play();
			    	});
		      		moveElement(scene.getObjectByName(searches[current_element].name),
		      		 			scene.getObjectByName(searches[i].name));
		      		moveElement(scene.getObjectByName(searches[i].name),
		      		 			scene.getObjectByName(searches[current_element].name));
		      		current_element = i;
		      		//добавление блока с информацией
		      		addInfoToExhibition();
		      		break;
		      	}
		    }
  		}      
    }
}

     //смена элементов, при нажатии на дальний
function moveElement(obj1, obj2) {
	let coords = {x:obj1.position.x,
				  z:obj1.position.z};
	let target = {x:obj2.position.x,
		          z:obj2.position.z};
  	let tween = new TWEEN.Tween(coords).to(target, 1000).easing(TWEEN.Easing.Quadratic.Out);
	tween.onUpdate(function(){
      obj1.position.x = coords.x;
      obj1.position.z = coords.z;
	}); 
    tween.start();
}

function defaultModeForElementOn() {
	document.body.style.cursor="default";
    if(isSelect) isSelect = false;
    if(isSelectLoopMode) {
		isSelectLoopMode = false;
		let angles = {x:selectObject.rotation.x,
			      	y:selectObject.rotation.y,
			      	z:selectObject.rotation.z};
      	let tween = new TWEEN.Tween(angles).to({x:0,y:0,z:-0.5}, 1000).easing(TWEEN.Easing.Quadratic.Out);
      		tween.onUpdate(function(){
      			selectObject.rotation.set(angles.x, angles.y, angles.z);
     		}); 
        tween.start();
	}
}


function onDocumentMouseMove( event ) {
    mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects(scene.children);
    if ( intersects.length > 0 ) {
    	var intersect = intersects[ 0 ];
        if (intersect.object.name=="loop" && !isStart) {
		    document.body.style.cursor="pointer";
	      	if (!isSelect) {
			    audioLoader.load( 'audio/select.wav', function( buffer ) {
			        // create a global audio source
			    	var sound = new THREE.Audio( listener );
			        sound.setBuffer( buffer );
			        sound.setVolume( 0.3 );
			        sound.play();
			    });
			    isSelect = true;
		    }
		    if(!isSelectLoopMode) {
			    isSelectLoopMode = true;
			    selectObject = intersect.object;
		    }
		    if (isSelectLoopMode) {
			    mouseX = ( event.clientX - windowHalfX );
			    mouseY = ( event.clientY - windowHalfY );
			}
        } else if (!isStart) {
	        defaultModeForElementOn();      
	    }
        for (var i = 0; i < searches.length; i++) {
	        if(intersect.object.name==searches[i].name) {
		        document.body.style.cursor="pointer";
		        if(!isSelect) {
			        audioLoader.load( 'audio/select.wav', function( buffer ) {
				        // create a global audio source
				        var sound = new THREE.Audio( listener );
				        sound.setBuffer( buffer );
				        sound.setVolume( 0.3 );
				        sound.play();
			        });
			        isSelect = true;
		        }
		        if (!isSelectLoopMode && intersect.object.name==searches[current_element].name) {
			        isSelectLoopMode = true;
			        selectObject = intersect.object;
		      		selectObject.rotation.y = 0;
		        } else if (intersect.object.name!=searches[current_element].name && !isSelectOtherObj) {
			      	isSelectOtherObj = true;
			      	let angle = {y:0, x:0};
			      	let tween = new TWEEN.Tween(angle).to({y:3.15, x:-0.1}, 1000).easing(TWEEN.Easing.Quadratic.Out);
	              		tween.onUpdate(function(){
							intersect.object.rotation.y=angle.y;
							intersect.object.rotation.x=angle.x;
	             		}).onComplete(()=>{
							new TWEEN.Tween(angle).to({x:0}, 500).onUpdate(function(){
								intersect.object.rotation.x=angle.x;
	             			}).start();
	             		}); 
	                tween.start();
		        }
		        if (isSelectLoopMode) {
		          mouseX = ( event.clientX - windowHalfX );
		          mouseY = ( event.clientY - windowHalfY );
		        }
		        break;
	        }
	        else if (i == searches.length - 1) {
		        document.body.style.cursor="default";
		        if(isSelectOtherObj) isSelectOtherObj = false;
		        if(isSelect) isSelect = false;
		        if(isSelectLoopMode) {
		            isSelectLoopMode = false;
		            let angles = {x:selectObject.rotation.x,
					          	  y:selectObject.rotation.y,
					          	  z:selectObject.rotation.z};
			      	let tween = new TWEEN.Tween(angles).to({x:0,y:0,z:0}, 1000).easing(TWEEN.Easing.Quadratic.Out);
	              		tween.onUpdate(function(){
	              			selectObject.rotation.set(angles.x, angles.y, angles.z);
	             		}); 
	                tween.start();
	        	}
	        }
        }
    }
}

function lookAtElement() {
	let maxX = 0.2, maxY = 0.4, k = 0.00001;
    if (selectObject.rotation.y > -maxY  && selectObject.rotation.y < maxY
        && selectObject.rotation.y + ( mouseX - camera.position.x ) * k > -maxY
        && selectObject.rotation.y + ( mouseX - camera.position.x ) * k < maxY)
    		selectObject.rotation.y += ( mouseX - camera.position.x ) * k;
    if (selectObject.rotation.x > -maxX && selectObject.rotation.x < maxX
        && selectObject.rotation.x + (  mouseY - camera.position.y ) * k > -maxX
        && selectObject.rotation.x + (  mouseY - camera.position.y ) * k < maxX)
    		selectObject.rotation.x += ( mouseY - camera.position.y ) * k;
}

function search_ajax(query_url) {
	isLoopClick = false;
	$.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        method: 'post',
        data: {
            text: query
        },
        url: query_url,
        success(elements) {
        	if (!isLoopClick) {
        		load_search_data(elements);
			  	let tween = tweenScaleElement(loop, {x:0,y:0,z:0}, 500); 
					tween.onComplete(()=>{
						scene.remove(loop);
					});
				tween.start();
        	}
        },
    });
}

function load_search_data (elements) {
	if (searches) {
		for (var i = 0; i < searches.length; i++) {
			scene.remove(scene.getObjectByName(searches[i].name));
		}
	}
	current_element = 0;
	searches = elements.data;
    console.log(elements);
    page = elements.current_page;
    $(".paginate").css("display", "flex");
    $(".paginate span .fas").css("display", "inline-block");
    if (page == 1) {
        $(".fa-chevron-left").css("display", "none");
    }
    if (page == elements.last_page || !elements.data.length) {
        $(".fa-chevron-right").css("display", "none");
    }
    if(searches.length) {
    	for (var i = 0; i < searches.length; i++) {
        	add_element(searches[i].photo_url, elemPosition[i], searches[i].name);
    	}
    	addInfoToExhibition();
    } else {
        $(".search-no-content").css("display", "flex");
    }
}

function add_element(url, position, name ) {
	if(url) {
		url = "/storage/exhibitions/" + url;
	} else {
		url = "/images/back_example.jpg"
	}
	let texture_page = new THREE.TextureLoader().load( xost + url);
	let page = new THREE.Mesh(new THREE.BoxBufferGeometry( 2.5, 3.5, 0.05 ), 
	                          new THREE.MeshBasicMaterial({
	                          map: texture_page}));
	page.position.z = position.z;
	page.position.x = position.x;
	page.name = name;
	scene.add(page);
	return page;          
}

function start_back_sound() {
	audioLoader.load( 'audio/background.mp3', function( buffer ) {
		// create a global audio source
		var sound = new THREE.Audio( listener );
		sound.setBuffer( buffer );
		sound.setLoop( true );
		sound.setVolume( 0.3 );
		sound.play();
	});
}

function getTexturesFromAtlasFile( atlasImgUrl, tilesNum ) {
	var textures = [];
	for ( var i = 0; i < tilesNum; i ++ ) {
	  textures[ i ] = new THREE.Texture();
	}
	var imageObj = new Image();
	imageObj.onload = function () {
		var canvas, context;
		var tileWidth = imageObj.height;
		for ( var i = 0; i < textures.length; i ++ ) {
			canvas = document.createElement( 'canvas' );
			context = canvas.getContext( '2d' );
			canvas.height = tileWidth;
			canvas.width = tileWidth;
			context.drawImage( imageObj, tileWidth * i, 0, tileWidth, tileWidth, 0, 0, tileWidth, tileWidth );
			textures[ i ].image = canvas;
			textures[ i ].needsUpdate = true;
		}
	};
	imageObj.src = atlasImgUrl;
	return textures;
}

function onWindowResize() {
	windowHalfX = window.innerWidth / 2;
	windowHalfY = window.innerHeight / 2;
	camera.aspect = window.innerWidth / window.innerHeight;
	camera.updateProjectionMatrix();
	renderer.setSize( window.innerWidth, window.innerHeight );
}

function animate() {
	requestAnimationFrame( animate );
	skyBox.rotation.y+=0.0002;
	skyBox.rotation.x+=0.0002;
	skyBox.rotation.z+=0.0002;
	if(torusKnot) {
	    torusKnot.rotation.y+=0.009;
	    torusKnot.rotation.x+=0.005;
	    torusKnot.rotation.z+=0.005;
	}
	if(isSelectLoopMode) {
	  lookAtElement();
	} else if (scene.getObjectByName("loop")) {
		if(loop.rotation.z + 0.002 > 0.5) isRotate = false;
		else if (loop.rotation.z - 0.002 < -0.5) isRotate = true;

		if(isRotate) loop.rotation.z += 0.002;
		else loop.rotation.z -= 0.002;
	}
	camera.lookAt( scene.position );
	TWEEN.update();
	renderer.render( scene, camera );
}