var pictures = null;
var isExhibitorVisible = false, wasExhibitorShowedAtStart = false;

async function init(picturesTextures) {

  var maxPictures = picturesTextures.length;
  var wallsForPicturesCount = 3;
  var picturesOnTheWallCount = Math.ceil(maxPictures / wallsForPicturesCount);

  var pictureDefaultWidth = 300;
  var pictureDefaultHeight = 400;
  var distanceOfPictures = 30;


  var roomWidth = (pictureDefaultWidth + distanceOfPictures * 2) * picturesOnTheWallCount;
  var roomHeight = 600;
  var roomDepth = (pictureDefaultWidth + distanceOfPictures * 2) * picturesOnTheWallCount;

  var object;
  var data_lamps = [];


  var renderer = initRenderer();
  var scene = new THREE.Scene();


  var lamps = new THREE.Group();
  scene.add(lamps);


  var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 30000);
  camera.position.set(roomWidth / 2 - 80, roomHeight / 2, 0);
  camera.lookAt(0, roomHeight / 2, 0);

  function move_to_center() {
    $({x: camera.position.x, z:camera.position.z}).animate({x:200, z:0}, {
      duration:3000,
      step: function(){
     camera.position.set(this.x, 400, this.z);

      },
      complete: function(){
  camera.lookAt(450, roomHeight / 2, -950);

        $({x: 450}).animate({x:-450}, {
      duration:3000,
      step: function(){
  camera.lookAt(this.x, roomHeight / 2, -950);

      }, complete: function(){
         $({z: -950}).animate({z:950}, {
      duration:5000,
      step: function(){
  camera.lookAt(-450, roomHeight / 2, this.z);

      },complete: function(){
         $({x: -450}).animate({x:450}, {
      duration:3000,
      step: function(){
  camera.lookAt(this.x, roomHeight / 2, 950);

      }
    })
      }
    })
      }
    })
      }
    })
  }

  function move_to_first(id){
  camera.lookAt(pictures.children[id].position.x - 250, pictures.children[id].position.y, pictures.children[id].position.z);

    $({x: camera.position.x, z:camera.position.z}).animate({x:pictures.children[id].position.x, z:pictures.children[id].position.z-470}, {
      duration:3000,
      step: function(){
     camera.position.set(this.x, 400, this.z);

      }
    });
  }

   function u(id){
    $({x: camera.position.x, z:camera.position.z}).animate({x:-500, z:camera.position.z - 100}, {
      duration:3000,
      step: function(){
     camera.position.set(this.x, 400, this.z);

      }
    });
  }


  var moveControls = initPersonControls(camera);

  hideExhibitorDialog();
  moveControls.enabled = false;

  var blocker = document.getElementById('blocker');
  var instructions = document.getElementById('instructions');
  instructions.addEventListener('click', function () {
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    document.addEventListener('mousedown', onDocumentMouseDown, false);
    document.addEventListener('mouseup', onDocumentMouseUp, false);
    instructions.style.display = 'none';
    blocker.style.display = 'none';
    moveControls.enabled = true;
    if (!wasExhibitorShowedAtStart) {
      showExhibitorDialog();
      wasExhibitorShowedAtStart = true;
    }
  }, false);
  var pics = $('div');
  for (var i = 0; i < pics.length; i++) {
    if ($(pics[i]).attr("class"))
      $(pics[i]).click((e) => {
        $(e.currentTarget).css("display", "none");
        document.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('mousedown', onDocumentMouseDown, false);
        document.addEventListener('mouseup', onDocumentMouseUp, false);
        moveControls.enabled = true;

      });
  }

  var onKeyDown = function (event) {
    switch (event.keyCode) {
      case 27:
        /*Escape*/
        {
          moveControls.enabled = false;
          blocker.style.display = 'block';
          instructions.style.display = '';
          document.removeEventListener('mousemove', onDocumentMouseMove, false);
          document.removeEventListener('mousedown', onDocumentMouseDown, false);
          document.removeEventListener('mouseup', onDocumentMouseUp, false);
          break;
        }
      case 13:
      /*Enter*/
      {
activateExhibitor(move_to_center,move_to_first,u);
      }
    }
  };
  document.addEventListener('keydown', onKeyDown, false);


  //получение времени для точного рендеринга
  var clock = new THREE.Clock();
  //для загрузки текстур
  var textureLoader = new THREE.TextureLoader();


  // пол
  var floorGeometry = new THREE.BoxGeometry(roomWidth, 5, roomDepth);
  var floorMaterial = makeMaterials(texturesForRoom.wood1.url, 1, 1);
  var floor = new THREE.Mesh(floorGeometry, floorMaterial);
  floor.name = "floor";
  //потолок
  var ceilGeometry = new THREE.BoxGeometry(roomWidth, 5, roomDepth);
  var ceilMaterial = makeMaterials(texturesForRoom.wallpaper1.url, 1, 1);
  var ceil = new THREE.Mesh(ceilGeometry, ceilMaterial);
  ceil.position.y = roomHeight;
  ceil.name = "ceil";

  //материал для стен
  var WallMaterial = makeMaterials(texturesForRoom.wallpaper1.url, texturesForRoom.wallpaper1.x, texturesForRoom.wallpaper1.y);
  var WallMaterial2 = makeMaterials(texturesForRoom.wallpaper1.url, texturesForRoom.wallpaper1.x, texturesForRoom.wallpaper1.y);
  // стена
  var rightWallGeometry = new THREE.BoxGeometry(5, roomHeight, roomDepth);
  var rightWall = new THREE.Mesh(rightWallGeometry, WallMaterial2);
  rightWall.position.x = roomWidth / 2;
  rightWall.position.y = roomHeight / 2;
  rightWall.name = "rightWall";

  // стена
  var leftWallGeometry = new THREE.BoxGeometry(5, roomHeight, roomDepth);
  var leftWall = new THREE.Mesh(leftWallGeometry, WallMaterial2);
  leftWall.position.x = -roomWidth / 2;
  leftWall.position.y = roomHeight / 2;
  leftWall.name = "leftWall";

  // стена
  var topWallGeometry = new THREE.BoxGeometry(roomWidth, roomHeight, 5);
  var topWall = new THREE.Mesh(topWallGeometry, WallMaterial);
  topWall.position.z = roomDepth / 2;
  topWall.position.y = roomHeight / 2;
  topWall.name = "topWall";

  // стена
  var bottomWallGeometry = new THREE.BoxGeometry(roomWidth, roomHeight, 5);
  var bottomWall = new THREE.Mesh(bottomWallGeometry, WallMaterial);
  bottomWall.position.z = -roomDepth / 2;
  bottomWall.position.y = roomHeight / 2;
  bottomWall.name = "bottomWall";
  //все картины объединены в группу
  pictures = new THREE.Group();
  //добавление картин
  var picture = 0;
  picture = picturesOnWall(-roomWidth / 2 + distanceOfPictures * 8, roomHeight / 2, roomDepth / 2 - 5, picture, 0, true);
  picture = picturesOnWall(-roomWidth / 2 + distanceOfPictures * 8, roomHeight / 2, -roomDepth / 2 + 5, picture, 0, true);
  picture = picturesOnWall(-roomWidth / 2 + 5, roomHeight / 2, -roomDepth / 3 + distanceOfPictures * 2, picture, Math.PI / 2, false);
  scene.add(pictures);


  //объединение потолка пола и стен в группу комнаты
  var room = new THREE.Group();
  room.add(ceil);
  room.add(floor);
  room.add(topWall);
  room.add(bottomWall);
  room.add(rightWall);
  room.add(leftWall);
  scene.add(room);

  room.receiveShadow = true;
  //комната как ограничение для контроллера управления перемещением
  moveControls.box = room;


  var raycaster = new THREE.Raycaster();
  var mouse = new THREE.Vector2();
  var isWorkingWithPicture = false;
  var isWorkingWithScale = false;
  var isWorkingWithLamp = false;

  var pObj;
  var lockPictures = 10;
  var lampSize;
  var lampX, lampZ;

  function showExhibitorDialog() {
    $("#ai-exhibitor-dialog").animate({
      bottom: "1%"
    }, 1000, function () {});
    $("#ai-exhibitor-icon").animate({
      left: "11%"
    }, 1000, function () {});
    document.removeEventListener('mousemove', onDocumentMouseMove, false);
    moveControls.enabled = false;
    isExhibitorVisible = true;
    document.body.style.cursor = 'default';
    scene.remove(scene.getObjectByName("cOnFloor"));
    $("#ai-exhibitor-answer").focus();
  }

  function hideExhibitorDialog() {
    $("#ai-exhibitor-dialog").animate({
      bottom: -120
    }, 1, function () {});
    $("#ai-exhibitor-icon").animate({
      left: -520
    }, 1, function () {});
    document.addEventListener('mousemove', onDocumentMouseMove, false);
    moveControls.enabled = true;
    isExhibitorVisible = false;
  }

  function onDocumentMouseMove(event) {
    if (!isExhibitorVisible) {
      var sectionHeight = $(window).height();
      var vertical = event.offsetY;
      if ((vertical > (sectionHeight - 50)) && event.offsetX < 100) {
        showExhibitorDialog();
        return;
      }
    }
    mouse.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1);
    scene.remove(scene.getObjectByName("cOnFloor"));
    scene.remove(scene.getObjectByName("outline"));


    raycaster.setFromCamera(mouse, camera);
    //пересекает пол
    var intersects = raycaster.intersectObjects(room.children.concat(pictures.children));

    if (intersects.length > 0) {
      var intersect = intersects[0];
      if (intersect.object.name == "floor") {
        var gCircle = new THREE.CylinderGeometry(25, 25, 1, 25, 25);
        var mCircle = new THREE.MeshBasicMaterial({
          color: 16777215,
          opacity: 0.5,
          transparent: !0,
          depthTest: !1
        });
        var Circle = new THREE.Mesh(gCircle, mCircle);
        Circle.name = "cOnFloor";
        Circle.position.copy(intersect.point).add(intersect.face.normal);
        scene.add(Circle);
        document.body.style.cursor = 'pointer';
      } else if (intersect.object.name == "topPicture" || intersect.object.name == "bottomPicture" || intersect.object.name == "leftPicture" || intersect.object.name == "rightPicture") {
        document.body.style.cursor = 'pointer';
      } else {
        document.body.style.cursor = 'default';
      }
    }
  }

  function onDocumentMouseDown(event) {
    mouse.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1);
    raycaster.setFromCamera(mouse, camera);
    var intersects = raycaster.intersectObjects(room.children.concat(pictures.children));

    if (intersects.length > 0) {
      var intersect = intersects[0];
      if (intersect.object.name == "floor") {
        var constant = 200;
        if (intersect.point.x < roomWidth / 2 * room.scale.x - 100)
          (intersect.point.x < 0) ?
          camera.position.x = intersect.point.x + constant :
          camera.position.x = intersect.point.x - constant;
        if (intersect.point.z < roomDepth / 2 * room.scale.z - 100)
          (intersect.point.z < 0) ?
          camera.position.z = intersect.point.z + constant :
          camera.position.z = intersect.point.z - constant;
      }


    }

  }

  function onDocumentMouseUp(event) {
    if (isExhibitorVisible) {
      var sectionHeight = $(window).height();
      var vertical = event.offsetY;
      if (vertical < (sectionHeight - 100)) {
        hideExhibitorDialog();
        return;
      }
    }
    mouse.set((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1);
    raycaster.setFromCamera(mouse, camera);
    var intersects = raycaster.intersectObjects(pictures.children.concat(scene.getObjectByName("door").children));
    if (intersects.length > 0) {
      var intersect = intersects[0];
      if (intersect.object.name == "topPicture" || intersect.object.name == "bottomPicture" || intersect.object.name == "leftPicture" || intersect.object.name == "rightPicture") {
        $("#picture").css('display', 'none');

        var q = intersect.object.material.name + "";
        var url_dir = xost + "/storage/pictures/";
        q = q.substring(url_dir.length);
        var t = $("div");
        for (var i = 0; i < t.length; i++) {
          if ($(t[i]).attr("class") == q)
            $(t[i]).css('display', 'flex');

        }
        moveControls.enabled = false;
        document.removeEventListener('mousemove', onDocumentMouseMove, false);
        document.removeEventListener('mousedown', onDocumentMouseDown, false);
        document.removeEventListener('mouseup', onDocumentMouseUp, false);


      } else if (intersect.object.name == "door") {

        document.location.href = xost;
        moveControls.enabled = false;
        blocker.style.display = 'block';
        instructions.style.display = '';
        document.removeEventListener('mousemove', onDocumentMouseMove, false);
        document.removeEventListener('mousedown', onDocumentMouseDown, false);
        document.removeEventListener('mouseup', onDocumentMouseUp, false);
        document.removeEventListener('keydown', onKeyDown, false);


      }

    }
  }


  var loader = new THREE.TextureLoader();
  var doorMap = loader.load('../../../assets/textures/front_door.jpg');
  var normal = loader.load('../../../assets/textures/front_door_normal.png');

  var loader = new THREE.TDSLoader();
  loader.load('../../../assets/models/Custom Front door.3ds', function (object) {
    object.traverse(function (child) {
      if (child instanceof THREE.Mesh) {
        child.material = new THREE.MeshStandardMaterial({
          metalness: 0.5,
          roughness: 0.8
        });
        child.material.map = doorMap;
        child.material.normalMap = normal;
        child.name = "door";

      }
    });
    object.position.set(roomWidth / 2 - 20, 0, 0);
    object.scale.x = 85;
    object.scale.z = 85;
    object.rotation.x = -Math.PI / 2;
    object.rotation.z = Math.PI / 2;
    object.name = "door";
    scene.add(object);
  });


  //объект элемента управления 
  var controls = new function () {


    this.width = room.scale.x;
    this.height = room.scale.y;
    this.depth = room.scale.z;
    this.materialFloor = JSON.stringify(texturesForRoom.wood1);
    this.materialWalls = JSON.stringify(texturesForRoom.wallpaper1);
    this.materialCeil = JSON.stringify(texturesForRoom.wallpaper1);
    this.lampType = "Тип 1";
    this.colorFloor = 0xffffff;
    this.colorWalls = 0xffffff;
    this.colorCeil = 0xffffff;
    this.colorLight = 0xffffff;
    this.colorLamp = 0xffffff;
    this.lightIntensity = 1;
    this.wallPaint = false;
    this.ceilPaint = false;
    this.floorPaint = false;


    this.deleteLamp = function (e) {
      lamps.remove(lamps.children[lamps.children.length - 1]);
      lamps.remove(lamps.children[lamps.children.length - 1]);
      data_lamps.pop();
    };

    this.addLamp = function (e) {
      if (controls.lampType == "Тип 1") {
        var loader = new THREE.TDSLoader();
        loader.load('../../../assets/models/Shade+Lamp-1.3ds', function (object) {
          object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
              child.material = new THREE.MeshStandardMaterial({
                color: controls.colorLamp,
                metalness: 0.5,
                roughness: 0.8
              });
              // child.material.map = la;
              child.name = "lamp" + lamps.children.length;
            }
          });


          lampSize = 210;
          lampZ = 2;
          lampX = 1;
          object.position.set(0, roomHeight * controls.height - lampSize, 0);
          object.scale.x = 3.5;
          object.scale.z = 3.5;
          object.scale.y = 3.5;
          object.rotation.x = -Math.PI / 2;
          object.name = "lamp" + lamps.children.length;
          lamps.add(object);
          pointLight(lamps, object.position, controls.colorLight, controls.lightIntensity);
          let v = new function () {
            this.color_light = controls.colorLight;
            this.intens = controls.lightIntensity;
            this.pos = lamps.children[lamps.children.length - 1].position;
            this.color_lamp = controls.colorLamp;
            this.type_lamp = controls.lampType;
          };
          data_lamps.push(v);

        });
      } else if (controls.lampType == "Тип 2") {
        var loader = new THREE.TDSLoader();
        loader.load('../../../assets/models/lamp.3ds', function (object) {
          object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
              child.material = new THREE.MeshStandardMaterial({
                color: controls.colorLamp,
                metalness: 0.5,
                roughness: 0.8
              });
              // child.material.map = la;
              child.name = "lamp" + lamps.children.length;
            }
          });

          lampSize = 210;
          lampZ = 21;
          lampX = 40;
          object.position.set(0, roomHeight * controls.height - lampSize, 0);
          object.scale.x = 1 / 7;
          object.scale.z = 1 / 7;
          object.scale.y = 1 / 7;
          object.rotation.x = -Math.PI / 2;
          object.name = "lamp" + lamps.children.length;
          lamps.add(object);
          pointLight(lamps, object.position, controls.colorLight, controls.lightIntensity);
          let v = new function () {
            this.color_light = controls.colorLight;
            this.intens = controls.lightIntensity;
            this.pos = lamps.children[lamps.children.length - 1].position;
            this.color_lamp = controls.colorLamp;
            this.type_lamp = controls.lampType;
          };
          data_lamps.push(v);

        });
      } else if (controls.lampType == "Тип 3") {
        var loader = new THREE.OBJLoader();
        loader.load('../../../assets/models/1.obj', function (object) {
          object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
              child.material = new THREE.MeshStandardMaterial({
                color: controls.colorLamp,
                metalness: 0.5,
                roughness: 0.8
              });
              // child.material.map = la;
              child.name = "lamp" + lamps.children.length;
            }
          });

          lampSize = 150;
          lampZ = 21;
          lampX = 40;
          object.position.set(0, roomHeight * controls.height - lampSize, 0);
          object.scale.x = 4;
          object.scale.z = 4;
          object.scale.y = 4;
          // object.rotation.x=-Math.PI/2;
          object.name = "lamp" + lamps.children.length;
          lamps.add(object);
          pointLight(lamps, object.position, controls.colorLight, controls.lightIntensity);
          let v = new function () {
            this.color_light = controls.colorLight;
            this.intens = controls.lightIntensity;
            this.pos = lamps.children[lamps.children.length - 1].position;
            this.color_lamp = controls.colorLamp;
            this.type_lamp = controls.lampType;
          };
          data_lamps.push(v);

        });
      }

    };

    this.update = function (e) {

      //удаляем существующее выделение картины
      scene.remove(scene.getObjectByName("outline"));

      camera.position.set(0, 400, 0);
      //изменение размеров комнаты
      room.scale.x = controls.width;
      room.scale.y = controls.height;
      room.scale.z = controls.depth;
      movePictures();
      moveDoor();
      moveLamp();

      //сохранение пропорций материала стен,потолка и пола при изменении размера
      for (var i = 0; i < room.children.length; i++) {
        if (room.children[i].material.map) {
          (i >= 0 && i <= 1) ?
          room.children[i].material.map.repeat.set(controls.width, controls.depth):
            (i <= 3) ?
            room.children[i].material.map.repeat.set(roomWidth * controls.width / 500, controls.height * roomHeight / 200) :
            room.children[i].material.map.repeat.set(roomDepth * controls.depth / 300, controls.height * roomHeight / 200);
          room.children[i].material.map.needsUpdate = true;
        }
      }
    };
    this.updateFloorColor = function (e) {
      controls.floorPaint = true;
      scene.getObjectByName("floor").material = new THREE.MeshStandardMaterial({
        color: controls.colorLamp,
        color: new THREE.Color(e)
      });
    };
    this.updateFloorTexture = function (e) {
      controls.floorPaint = false;
      var l = JSON.parse(e);
      scene.getObjectByName("floor").material = makeMaterials(l.url, l.x, l.y);

    };
    this.updateWallsColor = function (e) {
      controls.wallPaint = true;
      for (var i = 2; i < room.children.length; i++)
        room.children[i].material = new THREE.MeshStandardMaterial({
          color: new THREE.Color(e),
          metalness: 0.3,
          roughness: 0.9
        });
    };
    this.updateWallsTexture = function (e) {
      controls.wallPaint = false;
      var l = JSON.parse(e);
      for (var i = 2; i < room.children.length; i++)
        room.children[i].material = makeMaterials(l.url, l.x, l.y);
    };
    this.updateCeilColor = function (e) {
      controls.ceilPaint = true;
      scene.getObjectByName("ceil").material = new THREE.MeshStandardMaterial({
        color: new THREE.Color(e)
      });
    };
    this.updateCeilTexture = function (e) {
      controls.ceilPaint = false;
      var l = JSON.parse(e);
      scene.getObjectByName("ceil").material = makeMaterials(l.url, l.x, l.y);
    };
    this.updateLightIntensity = function (e) {
      lamps.children[lamps.children.length - 1].intensity = controls.lightIntensity;
      data_lamps[data_lamps.length - 1].intens = controls.lightIntensity;
    }
  };
  //если есть сохраненная комната в бд
  if (code) {
    controls.width = code[code.length - 1].width;
    controls.height = code[code.length - 1].height;
    controls.depth = code[code.length - 1].depth;

    controls.materialFloor = code[code.length - 1].materialFloor;
    controls.materialWalls = code[code.length - 1].materialWalls;
    controls.materialCeil = code[code.length - 1].materialCeil;

    controls.wallPaint = code[code.length - 1].wallPaint;
    controls.ceilPaint = code[code.length - 1].ceilPaint;
    controls.floorPaint = code[code.length - 1].floorPaint;

    controls.colorFloor = code[code.length - 1].colorFloor;
    controls.colorWalls = code[code.length - 1].colorWalls;
    controls.colorCeil = code[code.length - 1].colorCeil;


    window.onload = function () {
      (!controls.ceilPaint) ?
      controls.updateCeilTexture(controls.materialCeil):
        controls.updateCeilColor(controls.colorCeil);
      (!controls.wallPaint) ?
      controls.updateWallsTexture(controls.materialWalls):
        controls.updateWallsColor(controls.colorWalls);
      (!controls.floorPaint) ?
      controls.updateFloorTexture(controls.materialFloor):
        controls.updateFloorColor(controls.colorFloor);
      for (var i = 0; i < pictures.children.length; i++) {
        for (var j = 0; j < code.length; j++) {
          if (pictures.children[i].material.name == code[j].m_id) {
            pictures.children[i].name = code[j].p_name;
            pictures.children[i].position.set(code[j].pos.x, code[j].pos.y, code[j].pos.z);
            pictures.children[i].rotation.set(code[j].rot._x, code[j].rot._y, code[j].rot._z);
            pictures.children[i].scale.set(code[j].scale.x, code[j].scale.y, code[j].scale.z);
          }
        }
      }
      for (var i = maxPictures; i < code.length - 1; i++) {
        addSavingLamp(code[i]);

      }
      controls.update();
      camera.position.set((roomWidth * room.scale.x) / 2 - 80, roomHeight / 2, 0);

    }
  } else {
    controls.addLamp();
  }


  //обработчик изменения размеров экрана
  window.addEventListener('resize', onWindowResize, false);

  //отрисовка сцены
  render();


  function render() {
    // stats.update();
    //обновление данных о размере комнаты для контроллера управления перемещением
    moveControls.box = room;
    moveControls.update(clock.getDelta());
    requestAnimationFrame(render);
    renderer.render(scene, camera);
    // composer.render();
  }

  function addSavingLamp(c) {


    if (c.type_lamp == "Тип 1") {
      var loader = new THREE.TDSLoader();
      loader.load('../../../assets/models/Shade+Lamp-1.3ds', function (object) {
        object.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.material = new THREE.MeshStandardMaterial({
              color: c.color_lamp,
              metalness: 0.5,
              roughness: 0.8
            });
            // child.material.map = la;
            child.name = "lamp" + lamps.children.length;
          }
        });

        lampSize = 210;
        lampZ = 2;
        lampX = 1;
        object.position.set(c.pos.x, c.pos.y, c.pos.z);
        object.scale.x = 3.5;
        object.scale.z = 3.5;
        object.scale.y = 3.5;
        object.rotation.x = -Math.PI / 2;
        object.name = "lamp" + lamps.children.length;
        lamps.add(object);
        pointLight(lamps, object.position, c.color_light, c.intens);
        let v = new function () {
          this.color_light = c.color_light;
          this.intens = c.intens;
          this.pos = c.pos;
          this.color_lamp = c.color_lamp;
          this.type_lamp = c.type_lamp;
        };
        data_lamps.push(v);

      });
    } else if (c.type_lamp == "Тип 2") {
      var loader = new THREE.TDSLoader();
      loader.load('../../../assets/models/lamp.3ds', function (object) {
        object.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.material = new THREE.MeshStandardMaterial({
              color: c.color_lamp,
              metalness: 0.5,
              roughness: 0.8
            });
            // child.material.map = la;
            child.name = "lamp" + lamps.children.length;
          }
        });

        lampSize = 210;
        lampZ = 21;
        lampX = 40;
        object.position.set(c.pos.x, c.pos.y, c.pos.z);
        object.scale.x = 1 / 7;
        object.scale.z = 1 / 7;
        object.scale.y = 1 / 7;
        object.rotation.x = -Math.PI / 2;
        object.name = "lamp" + lamps.children.length;
        lamps.add(object);
        pointLight(lamps, object.position, c.color_light, c.intens);
        let v = new function () {
          this.color_light = c.color_light;
          this.intens = c.intens;
          this.pos = c.pos;
          this.color_lamp = c.color_lamp;
          this.type_lamp = c.type_lamp;
        };
        data_lamps.push(v);

      });
    } else if (c.type_lamp == "Тип 3") {
      var loader = new THREE.OBJLoader();
      loader.load('../../../assets/models/1.obj', function (object) {
        object.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.material = new THREE.MeshStandardMaterial({
              color: c.color_lamp,
              metalness: 0.5,
              roughness: 0.8
            });
            // child.material.map = la;
            child.name = "lamp" + lamps.children.length;
          }
        });

        lampSize = 150;
        lampZ = 21;
        lampX = 40;
        object.position.set(c.pos.x, c.pos.y, c.pos.z);
        object.scale.x = 4;
        object.scale.z = 4;
        object.scale.y = 4;
        // object.rotation.x=-Math.PI/2;
        object.name = "lamp" + lamps.children.length;
        lamps.add(object);
        pointLight(lamps, object.position, c.color_light, c.intens);
        let v = new function () {
          this.color_light = c.color_light;
          this.intens = c.intens;
          this.pos = c.pos;
          this.color_lamp = c.color_lamp;
          this.type_lamp = c.type_lamp;
        };
        data_lamps.push(v);
      });
    }
  }


  //создание текстур
  function makeMaterials(url, repeatX, repeatY) {
    return new THREE.MeshStandardMaterial({
      map: textureLoader.load(url, function (texture) {
        texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        texture.magFilter = THREE.LinearFilter;
        texture.repeat.set(repeatX, repeatY);
      }),
      metalness: 0.5,
      roughness: 0.9
    });
  }


  //перемещение двери при изменении размеров комнаты
  function moveDoor() {
    scene.getObjectByName("door").position.x = (roomWidth * controls.width) / 2 - 20;
  }
  //перемещение ламп
  function moveLamp() {
    for (var i = 0; i < lamps.children.length; i++) {
      lamps.children[i].position.y = (roomHeight * controls.height) - 210;
    }
  }
  //перемещение картин вслед за стеной
  function movePictures() {
    for (var i = 0; i < pictures.children.length; i++) {
      (pictures.children[i].name == "topPicture") ?
      pictures.children[i].position.z = (roomDepth * controls.depth) / 2 - 8:
        (pictures.children[i].name == "bottomPicture") ?
        pictures.children[i].position.z = (-roomDepth * controls.depth) / 2 + 8 :
        (pictures.children[i].name == "leftPicture") ?
        pictures.children[i].position.x = (-roomWidth * controls.width) / 2 + 8 :
        pictures.children[i].position.x = (roomWidth * controls.width) / 2 - 8;
    }
  }
  //размещение картин на стене
  function picturesOnWall(x, y, z, picture, deg, flag) {
    for (var j = 1; j <= picturesOnTheWallCount; j++) {
      pictureOnWall(picturesTextures[picture], x, y, z, deg, picture);
      picture++;
      if (flag) x += pictureDefaultWidth + distanceOfPictures;
      else z += pictureDefaultWidth + distanceOfPictures;
    }
    return picture;
  }
  //создание одной картины
  function pictureOnWall(url, posX, posY, posZ, deg, pic) {
    //создание материала
    var texture = new THREE.TextureLoader().load(url, function (texture) {
      texture.minFilter = THREE.LinearFilter;
      texture.magFilter = THREE.NearestFilter;
      texture.anisotropy = renderer.capabilities.getMaxAnisotropy();
      var material = new THREE.MeshStandardMaterial({
        name: url,
        map: texture,
        metalness: 0.5,
        roughness: 0.9
      });
      //сохранение пропорций изображения
      var w = texture.image.naturalWidth;
      var h = texture.image.naturalHeight;
      var p = (w > h) ? w / h : h / w;
      if (w >= h && w > pictureDefaultWidth) {
        h -= (w - pictureDefaultWidth) / p;
        w = pictureDefaultWidth;
      } else if (h > w && h > pictureDefaultHeight) {
        w -= (h - pictureDefaultHeight) / p;
        h = pictureDefaultHeight;
      }
      //создание меша с картиной
      var plane = new THREE.BoxBufferGeometry(w, h, 5, 100, 100);
      var quad = new THREE.Mesh(plane, material);
      quad.position.x = posX;
      quad.position.z = posZ;
      quad.position.y = posY;
      quad.rotation.y = deg;
      quad.castShadow = true;
      (pic <= picturesOnTheWallCount - 1) ? quad.name = 'topPicture':
        (pic <= picturesOnTheWallCount * 2 - 1) ? quad.name = 'bottomPicture' :
        quad.name = 'leftPicture';
      pictures.add(quad);

    });
  }

  function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight);
  }

}