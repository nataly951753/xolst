var container2, camera2, cameraTarget, scene2, renderer2;
var pointLight, loop2;

init2();
animate2();

function init2() {
    container2 = document.getElementById( 'search' );
    camera2 = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 0.1, 100 );

    scene2 = new THREE.Scene();
    scene2.fog = new THREE.Fog( 0xffffff, 0.1, 15 );

    renderer2 = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
    renderer2.setClearColor( 0x000000, 0 );
    renderer2.setPixelRatio( window.devicePixelRatio );
    renderer2.setSize( 200,130 );

    var pointLight = new THREE.PointLight( 0xffffff, 1.5 );
    pointLight.position.set( 1, 1, 1 );
    scene2.add( pointLight );

    container2.appendChild( renderer2.domElement );

    add_loop_object();
}

function add_loop_object() {

	let loopGeometry = new THREE.Geometry();

	let geometryTorus = new THREE.TorusGeometry( 2, 0.5, 32, 100 );
	let geometryCylinder = new THREE.CylinderGeometry( 0.5, 0.4, 2.5, 32, 32 );

	let boxMesh = new THREE.Mesh(geometryTorus);
	let sphereMesh = new THREE.Mesh(geometryCylinder);

	sphereMesh.position.x = 3.5;

	sphereMesh.rotation.z = 1.55;
	loopGeometry.merge(boxMesh.geometry, boxMesh.matrix);


	sphereMesh.updateMatrix(); // as needed
	loopGeometry.merge(sphereMesh.geometry, sphereMesh.matrix);
	loopGeometry.scale(0.3,0.3,0.3);
	loop2 = new THREE.Mesh(loopGeometry, new THREE.MeshPhongMaterial( { color: "#4a707a" } ));
	loop2.position.z = -6;
	loop2.position.y = 0.5;

	loop2.rotation.z = -0.5;
	loop2.name = "loop";

	scene2.add( loop2 );
}



function animate2() {
requestAnimationFrame( animate2 );
loop2.rotation.y-=0.02;
loop2.rotation.x-=0.002;
loop2.rotation.z-=0.002;
camera2.lookAt( scene2.position );
renderer2.render( scene2, camera2 );
}