var containerL,  hex;
var cameraL, cameraTargetL, sceneL, rendererL;
var group, textMesh1, textMesh2, textGeo, materials;
var firstLetter = true;
var text = "Xolst",
    height = 40,
    size = 160,
    hover = 30,
    curveSegments = 54,
    bevelThickness = 2,
    bevelSize = 1.5,
    bevelEnabled = true,
    font = undefined,
    fontName = "playball", // helvetiker, optimer, gentilis, droid sans, droid serif
    fontWeight = "regular"; // normal bold

var targetRotation = 0;
var targetRotationOnMouseDown = 0;
var mouseX = 0;
var mouseXOnMouseDown = 0;
var windowHalfX = window.innerWidth / 2;
var fontIndex = 1;
initL();
animateL();

function initL() {
    containerL = document.getElementById( 'logo' );
    // CAMERA
    cameraL = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 1, 1500 );
    cameraL.position.set( 0, 200, 700 );
    cameraTargetL = new THREE.Vector3( 0, 150, 0 );
    // SCENE
    sceneL = new THREE.Scene();
    sceneL.fog = new THREE.Fog( 0xffffff, 250, 1400 );

    // LIGHTS
    var dirLight = new THREE.DirectionalLight( 0xffffff, 0.125 );
    dirLight.position.set( 0, 0, 1 ).normalize();
    sceneL.add( dirLight );
    var pointLight = new THREE.PointLight( 0xffffff, 1.5 );
    pointLight.position.set( 500, 500, 500 );
    sceneL.add( pointLight );
    
    materials = [
        new THREE.MeshPhongMaterial( { color: "#4a707a", flatShading: true } ), // front
        new THREE.MeshPhongMaterial( { color: "#4a707a" } ) // side
    ];
    group = new THREE.Group();
    group.position.y = 100;
    sceneL.add( group );
    loadFont();
    // RENDERER
    rendererL = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
    rendererL.setClearColor( 0x000000, 0 )
    rendererL.setPixelRatio( window.devicePixelRatio );
    rendererL.setSize( 200,130 );
    containerL.appendChild( rendererL.domElement );
    
    // EVENTS
    document.getElementById('logo').addEventListener( 'mousedown', onDocumentMouseDown2, false );
    document.getElementById('logo').addEventListener( 'touchstart', onDocumentTouchStart2, false );
    document.getElementById('logo').addEventListener( 'touchmove', onDocumentTouchMove2, false );

    
}

function loadFont() {
    var loader = new THREE.FontLoader();
    var url='/' + fontName + '_' + fontWeight + '.typeface.json';
    loader.load("../../../../fonts/" +url , function ( response ) {
        font = response;
        refreshText();
    } );
}
function createText() {
    textGeo = new THREE.TextGeometry( text, {
        font: font,
        size: size,
        height: height,
        curveSegments: curveSegments,
        bevelThickness: bevelThickness,
        bevelSize: bevelSize,
        bevelEnabled: bevelEnabled
    } );
    textGeo.computeBoundingBox();
    textGeo.computeVertexNormals();
    
    var centerOffset = - 0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );
    textGeo = new THREE.BufferGeometry().fromGeometry( textGeo );
    textMesh1 = new THREE.Mesh( textGeo, materials );
    textMesh1.position.x = centerOffset;
    textMesh1.position.y = hover;
    textMesh1.position.z = 0;
    textMesh1.rotation.x = 0;
    textMesh1.rotation.y = Math.PI * 2;
    group.add( textMesh1 );
}
function refreshText() {
    group.remove( textMesh1 );
    if ( ! text ) return;
    createText();
}
var logo_rotate=false;

function onDocumentMouseDown2( event ) {
    event.preventDefault();
    document.getElementById('logo').addEventListener( 'mousemove', onDocumentMouseMove2, false );
    document.getElementById('logo').addEventListener( 'mouseup', onDocumentMouseUp2, false );
    document.getElementById('logo').addEventListener( 'mouseout', onDocumentMouseOut2, false );
    mouseXOnMouseDown = event.clientX - windowHalfX;
    targetRotationOnMouseDown = targetRotation;
}
function onDocumentMouseMove2( event ) {
    mouseX = event.clientX - windowHalfX;
    logo_rotate=true;
    targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.02;
}
function onDocumentMouseUp2() {
    document.getElementById('logo').removeEventListener( 'mousemove', onDocumentMouseMove2, false );
    document.getElementById('logo').removeEventListener( 'mouseup', onDocumentMouseUp2, false );
    document.getElementById('logo').removeEventListener( 'mouseout', onDocumentMouseOut2, false );
        logo_rotate=false;    
        $("#p_prldr").css("display", "flex");
        $svg_anm   = $("#p_prldr").find('.svg_anm');
        $svg_anm.fadeIn();              

}
function onDocumentMouseOut2() {
    document.getElementById('logo').removeEventListener( 'mousemove', onDocumentMouseMove2, false );
    document.getElementById('logo').removeEventListener( 'mouseup', onDocumentMouseUp2, false );
    document.getElementById('logo').removeEventListener( 'mouseout', onDocumentMouseOut2, false );
        logo_rotate=false;                  

}
function onDocumentTouchStart2( event ) {
    if ( event.touches.length == 1 ) {
        // event.preventDefault();
        mouseXOnMouseDown = event.touches[ 0 ].pageX - windowHalfX;
        logo_rotate=true;                   
        targetRotationOnMouseDown = targetRotation;
    }
}
function onDocumentTouchMove2( event ) {
    if ( event.touches.length == 1 ) {
        event.preventDefault();
        mouseX = event.touches[ 0 ].pageX - windowHalfX;
        logo_rotate=true;                   
        targetRotation = targetRotationOnMouseDown + ( mouseX - mouseXOnMouseDown ) * 0.05;
    }
}
//ANIMATE
function animateL() {
    requestAnimationFrame( animateL );
    group.rotation.y +=  0.01;
    if(logo_rotate)
    group.rotation.y += ( targetRotation - group.rotation.y ) * 0.05;
    renderL();
}
function renderL() {
    cameraL.lookAt( cameraTargetL );
    rendererL.clear();
    rendererL.render( sceneL, cameraL );
}