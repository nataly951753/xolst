//массив загруженных изображений работ
var pictures = null;

function init(picturesTextures) {
  //кол-во картин
  var maxPictures=picturesTextures.length;
  //кол-во стен, на которых будут размещены картины
  var wallsForPicturesCount=3;
  //кол-во картин на одной стене
  var picturesOnTheWallCount=Math.ceil(maxPictures/wallsForPicturesCount);
  //изначальные размеры картин
  var pictureDefaultWidth=300;
  var pictureDefaultHeight=400;
  var distanceOfPictures=30;
//длина, ширина и высота комнаты с расчетом картин
  var roomWidth= (pictureDefaultWidth + distanceOfPictures*2)* picturesOnTheWallCount;
  var roomHeight=600;
  var roomDepth=(pictureDefaultWidth + distanceOfPictures*2) * picturesOnTheWallCount;

//информация о источниках освещения для сохранения
  var data_lamps=[];

  var renderer = initRenderer();
  var scene = new THREE.Scene();

  //источники света
  var lamps=new THREE.Group();
  scene.add(lamps);

  var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 30000); 
    camera.position.set(roomWidth/2-80,roomHeight/2,0);
    camera.lookAt(0,roomHeight/2,0);
//контроллеры поворота камеры при перемещении мыши
  var moveControls = initPersonControls(camera);

 

//экран инструкций
  var blocker = document.getElementById( 'blocker' );
  var instructions = document.getElementById( 'instructions' );
        instructions.addEventListener( 'click', function () {
          document.addEventListener( 'mousemove', onDocumentMouseMove, false );
          document.addEventListener( 'mousedown', onDocumentMouseDown, false );
          document.addEventListener( 'mouseup', onDocumentMouseUp, false );
          instructions.style.display = 'none';
          blocker.style.display = 'none';
          moveControls.enabled=true;
        }, false );

        //закрытие экрана инструкций
        var onKeyDown = function ( event ) {
          switch ( event.keyCode ) {
            case 27:/*Escape*/{ 
        moveControls.enabled=false;
        blocker.style.display = 'block';
          instructions.style.display = '';
          document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
          document.removeEventListener( 'mousedown', onDocumentMouseDown, false );
          document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
          break;}
          }
        };
        document.addEventListener( 'keydown', onKeyDown, false );


  //получение времени для точного рендеринга
  var clock = new THREE.Clock();
  //для загрузки текстур
  var textureLoader = new THREE.TextureLoader();


  // пол
 var floorGeometry = new THREE.BoxGeometry(roomWidth,5,roomDepth);
 var floorMaterial = makeMaterials(texturesForRoom.wood1.url,1,1);
 var floor=new THREE.Mesh(floorGeometry,floorMaterial);
 floor.name="floor";
//потолок
var ceilGeometry = new THREE.BoxGeometry(roomWidth,5,roomDepth);
 var ceilMaterial = makeMaterials(texturesForRoom.wallpaper1.url,1,1);
 var ceil=new THREE.Mesh(ceilGeometry,ceilMaterial);
 ceil.position.y=roomHeight;
 ceil.name="ceil";

//материал для стен
var WallMaterial =  makeMaterials(texturesForRoom.wallpaper1.url,texturesForRoom.wallpaper1.x,texturesForRoom.wallpaper1.y);
var WallMaterial2=  makeMaterials(texturesForRoom.wallpaper1.url,texturesForRoom.wallpaper1.x,texturesForRoom.wallpaper1.y);
// стена
var rightWallGeometry = new THREE.BoxGeometry(5,roomHeight,roomDepth);
 var rightWall=new THREE.Mesh(rightWallGeometry,WallMaterial2);
 rightWall.position.x=roomWidth/2;
 rightWall.position.y=roomHeight/2;
 rightWall.name="rightWall";

// стена
var leftWallGeometry = new THREE.BoxGeometry(5,roomHeight,roomDepth);
 var leftWall=new THREE.Mesh(leftWallGeometry,WallMaterial2);
 leftWall.position.x=-roomWidth/2;
 leftWall.position.y=roomHeight/2;
 leftWall.name="leftWall";

 // стена
 var topWallGeometry = new THREE.BoxGeometry(roomWidth,roomHeight,5);
 var topWall=new THREE.Mesh(topWallGeometry,WallMaterial);
 topWall.position.z=roomDepth/2;
 topWall.position.y=roomHeight/2;
 topWall.name="topWall";

 // стена
var bottomWallGeometry = new THREE.BoxGeometry(roomWidth,roomHeight,5);
 var bottomWall=new THREE.Mesh(bottomWallGeometry,WallMaterial);
 bottomWall.position.z=-roomDepth/2;
 bottomWall.position.y=roomHeight/2;
 bottomWall.name="bottomWall";
//все картины объединены в группу
pictures=new THREE.Group();
//добавление картин
var picture=0;
picture=picturesOnWall(-roomWidth/2+distanceOfPictures*8,roomHeight/2,roomDepth/2 -5,picture,0,true);
picture=picturesOnWall(-roomWidth/2+distanceOfPictures*8,roomHeight/2,-roomDepth/2 +5,picture,0,true);
picture=picturesOnWall(-roomWidth/2+5,roomHeight/2,-roomDepth/3 +distanceOfPictures*2,picture,Math.PI/2,false);
scene.add(pictures);


//объединение потолка пола и стен в группу комнаты
var room=new THREE.Group();
room.add(ceil);
room.add(floor);
room.add(topWall);
room.add(bottomWall);
room.add(rightWall);
room.add(leftWall);
scene.add(room);
//отбрасывание теней на поверхности комнаты
room.receiveShadow=true;
//комната как ограничение для контроллера управления перемещением
moveControls.box=room;

//луч, пересекающий объекты
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var isWorkingWithPicture=false;
var isWorkingWithScale=false;
var isWorkingWithLamp=false;

var pObj;//объект, с которым работают
var lockPictures=10;//ограничение смещение картины
var lampSize;//высота лампы
var lampX, lampZ;



function onDocumentMouseMove( event ) {
  //получение пользовательских координат
        mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
        //удаление выделения курсора на поверхности пола и рамки картины
        scene.remove(scene.getObjectByName("cOnFloor"));
        scene.remove(scene.getObjectByName("outline"));
     // строится луч
        raycaster.setFromCamera( mouse, camera );
        //ппроверка пересечения картин и комнаты
        var intersects = raycaster.intersectObjects( room.children.concat(pictures.children) );

        if ( intersects.length > 0 ) {
          var intersect = intersects[ 0 ];
          if(!isWorkingWithLamp){
          if(!isWorkingWithScale){
          if(!isWorkingWithPicture){
            //создание на полу круга
          if(intersect.object.name=="floor")
          {
            var gCircle =  new THREE.CylinderGeometry(25, 25, 1,25,25);
            var mCircle= new THREE.MeshBasicMaterial({
                color: 16777215,
                opacity: 0.5,
                transparent: !0,
                depthTest: !1
            });
            var Circle=new THREE.Mesh(gCircle,mCircle);
            Circle.name="cOnFloor";
            Circle.position.copy( intersect.point ).add( intersect.face.normal);
            scene.add(Circle);
            document.body.style.cursor = 'pointer';
            
          }
          else if(intersect.object.name=="topPicture"||intersect.object.name=="bottomPicture"||intersect.object.name=="leftPicture"||intersect.object.name=="rightPicture"){
            //создание рамки вокруг картины
            var outlineMaterial1 = new THREE.MeshBasicMaterial( { color: 0xffffff, side: THREE.BackSide } );
            var f=2;
            var outlineMesh1 = new THREE.Mesh( new THREE.BoxBufferGeometry(intersect.object.geometry.parameters.width*intersect.object.scale.x,intersect.object.geometry.parameters.height*intersect.object.scale.y,intersect.object.geometry.parameters.depth-f)
              , outlineMaterial1 );
            outlineMesh1.position.copy(intersect.object.position);
            outlineMesh1.rotation.copy(intersect.object.rotation);

            outlineMesh1.scale.multiplyScalar(1.05);
            outlineMesh1.name="outline";
            scene.add( outlineMesh1 );
            document.body.style.cursor = 'pointer';
          } else{
            document.body.style.cursor = 'default';

          }
        }
        else
        {
            //передвижение картин вслед за мышью
          if(pObj.name=="topPicture" ||pObj.name=="bottomPicture"){

            var lock=((roomWidth/2)*room.scale.x)-lockPictures;            
            if(intersect.point.x<=lock&&intersect.point.x>=-lock)
            pObj.position.x= intersect.point.x;
            else
            {
              pObj.rotation.y=Math.PI/2;
              if(intersect.point.x<-lock){
                pObj.name="leftPicture";
                pObj.position.z= intersect.point.z+lockPictures;
              }
              else
              {
                pObj.name="rightPicture";
                pObj.position.z= intersect.point.z-lockPictures;
              }
              movePictures();
              
            }
            pObj.position.y= intersect.point.y;


          }
          if(pObj.name=="rightPicture" || pObj.name=="leftPicture")
          {
            var lock=((roomDepth/2)*room.scale.z)-lockPictures;
            if(intersect.point.z<=lock&&intersect.point.z>=-lock)
            pObj.position.z= intersect.point.z;
            else
            {
              pObj.rotation.y=Math.PI;
              if(intersect.point.z<-lock){
                pObj.name="bottomPicture";
                pObj.position.x= intersect.point.x+lockPictures;                
              }
              else
              {
                pObj.name="topPicture";
                pObj.position.x= intersect.point.x-lockPictures;
              }
              movePictures();
            }
            pObj.position.y= intersect.point.y;
          }
        }
        }
      }
      else
      {
        //перемещение модели лампы и источника света
        if(intersect.object.name=="ceil")
          {
            var lock=((roomWidth/2)*controls.width);   
            var lock2=((roomDepth/2)*controls.depth);  
              lamps.children.forEach(function(item,i,arr) {
                  if(item.name==pObj.name ){
                    if(item.type=="PointLight")
                    {
                    if(intersect.point.x<=lock-50&&intersect.point.x>=-lock+50)
                      item.position.x= intersect.point.x;
                    if(intersect.point.z<=lock2-50&&intersect.point.z>=-lock2+50)
                      item.position.z= intersect.point.z;
                    }
                    else{
                    if(intersect.point.x<=lock-50&&intersect.point.x>=-lock+50)
                      item.position.x= intersect.point.x+lampSize/lampX;
                    if(intersect.point.z<=lock2-50&&intersect.point.z>=-lock2+50)
                      item.position.z= intersect.point.z+lampSize/lampZ;
                    }
                    
                  }
              });
          }
      }
      }
      }

      function onDocumentMouseDown( event ) {

        mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
        raycaster.setFromCamera( mouse, camera );
        var intersects = raycaster.intersectObjects( room.children.concat(pictures.children) );
        
        if ( intersects.length > 0 ) {
          var intersect = intersects[ 0 ];
          //перемещение пользователя по комнате
          if(intersect.object.name=="floor")
          {            
            var constant=200;
            if(intersect.point.x<roomWidth/2*room.scale.x-100)
            (intersect.point.x<0) ?
            camera.position.x=intersect.point.x+constant:
            camera.position.x=intersect.point.x-constant;
            if(intersect.point.z<roomDepth/2*room.scale.z-100)
            (intersect.point.z<0) ?
            camera.position.z=intersect.point.z+constant:
            camera.position.z=intersect.point.z-constant;
          }

          
        }
 
      }
      function onDocumentMouseUp( event ) {
        mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
        raycaster.setFromCamera( mouse, camera );
        //помещение всех деталей модели лампы в массив, для проверки на пересечение лучем
        var m=[];
        for (var i = 0; i < lamps.children.length; i++) {
          if(i%2!=0)
          m=m.concat(lamps.children[i]);
          else
          m=m.concat(lamps.children[i].children);
        }
        var intersects = raycaster.intersectObjects( pictures.children.concat(scene.getObjectByName("door").children, m ) );        
        if ( intersects.length > 0 ) {
          var intersect = intersects[ 0 ];
          if(!isWorkingWithLamp&&( intersect.object.name=="topPicture"||intersect.object.name=="bottomPicture"||intersect.object.name=="leftPicture"||intersect.object.name=="rightPicture"))
          {            
            //правая кнопка мыши
            if(event.which==1 && !isWorkingWithScale){
            isWorkingWithPicture=!isWorkingWithPicture;
            pObj=intersect.object;//объект, который будет менять позицию
            moveControls.enabled=!isWorkingWithPicture;

            }
            //левая кнопка мыши
            else if(event.which==3)
            {
              isWorkingWithScale=!isWorkingWithScale;
              if(isWorkingWithScale)
              control.attach( intersect.object );//контроллер изменения размера
              else
              control.detach();
              moveControls.enabled=!isWorkingWithScale;

            }

          }
          else if(intersect.object.name=="door"){
            //данные, которые передаются в контроллер для сохранения
            var mas=[];
            for (var i = 0; i < pictures.children.length; i++) {
              let v=new function(){
                this.p_name=pictures.children[i].name;
                this.pos=pictures.children[i].position;
                this.rot=pictures.children[i].rotation;
                this.scale=pictures.children[i].scale;
                this.m_id=pictures.children[i].material.name;
              };
              mas.push(v);
            }
            mas=mas.concat(data_lamps);
            mas=mas.concat(controls);
            

            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              method:'post',
              url: route,
              dataType : 'json',
              data: {
                code:JSON.stringify(mas),
              },
              sucess:function(data){
                console.log(data);
              },
              error:function(data){
                console.log(data);
              }

            });
                document.location.href=xost;//выход
                moveControls.enabled=false;
                blocker.style.display = 'block';
                instructions.style.display = '';
                document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
                document.removeEventListener( 'mousedown', onDocumentMouseDown, false );
                document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
                document.removeEventListener( 'keydown', onKeyDown, false );

          }
          else
          {
            isWorkingWithLamp=!isWorkingWithLamp;
            pObj=intersect.object;
            moveControls.enabled=!isWorkingWithLamp;
          }
        } 
      }

var loader = new THREE.TextureLoader();
        var doorMap = loader.load( '../../../assets/textures/front_door.jpg' );
        var normal = loader.load( '../../../assets/textures/front_door_normal.png' );

        var loader = new THREE.TDSLoader( );
        loader.load( '../../../assets/models/Custom Front door.3ds', function ( object ) {
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({metalness:0.5,roughness:0.8});
              child.material.map = doorMap;
              child.material.normalMap = normal;
              child.name="door";

            }
          } );
          object.position.set(roomWidth/2-20,0,0);
          object.scale.x=85;
          object.scale.z=85;
          object.rotation.x=-Math.PI/2;
          object.rotation.z=Math.PI/2;
          object.name="door";
          scene.add( object );
        } );


  //объект элемента управления 
  var controls = new function () {

    
    this.width = room.scale.x;
    this.height = room.scale.y;
    this.depth = room.scale.z;
    this.materialFloor=JSON.stringify(texturesForRoom.wood1);
    this.materialWalls= JSON.stringify(texturesForRoom.wallpaper1);
    this.materialCeil= JSON.stringify(texturesForRoom.wallpaper1);
    this.lampType="Тип 1";
    this.colorFloor=0xffffff;
    this.colorWalls=0xffffff;
    this.colorCeil=0xffffff ;
    this.colorLight=0xffffff;
    this.colorLamp=0xffffff;
    this.lightIntensity=1;
    this.wallPaint=false;
    this.ceilPaint=false;
    this.floorPaint=false;

    


    this.deleteLamp=function(e){
      lamps.remove(lamps.children[lamps.children.length-1]);
      lamps.remove(lamps.children[lamps.children.length-1]);
      data_lamps.pop();
    };

    this.addLamp=function(e) {
            

        if(controls.lampType=="Тип 1"){
        var loader = new THREE.TDSLoader();
        loader.load( '../../../assets/models/Shade+Lamp-1.3ds', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({color:controls.colorLamp,metalness:0.5,roughness:0.8});
              child.name="lamp"+lamps.children.length;
            }
          } );
      

          lampSize=210; lampZ=2; lampX=1;
          object.position.set(0,roomHeight*controls.height-lampSize,0);
          object.scale.x=3.5;
          object.scale.z=3.5;
          object.scale.y=3.5;
          object.rotation.x=-Math.PI/2;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
          pointLight(lamps,object.position,controls.colorLight, controls.lightIntensity);
          let v=new function(){
                this.color_light=controls.colorLight;
                this.intens=controls.lightIntensity;
                this.pos=lamps.children[lamps.children.length-1].position;
                this.color_lamp=controls.colorLamp;
                this.type_lamp=controls.lampType;
              };
            data_lamps.push(v);

        } );
        }
        else if(controls.lampType=="Тип 2"){
          var loader = new THREE.TDSLoader();
          loader.load( '../../../assets/models/lamp.3DS', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({color:controls.colorLamp,metalness:0.5,roughness:0.8});
              child.name="lamp"+lamps.children.length;
            }
          } );

          lampSize=210;lampZ=21;lampX=40;
          object.position.set(0,roomHeight*controls.height-lampSize,0);
          object.scale.x=1/7;
          object.scale.z=1/7;
          object.scale.y=1/7;
          object.rotation.x=-Math.PI/2;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
          pointLight(lamps,object.position,controls.colorLight, controls.lightIntensity);
          let v=new function(){
                this.color_light=controls.colorLight;
                this.intens=controls.lightIntensity;
                this.pos=lamps.children[lamps.children.length-1].position;
                this.color_lamp=controls.colorLamp;
                this.type_lamp=controls.lampType;
              };
            data_lamps.push(v);

        } );
        }
        else if(controls.lampType=="Тип 3"){
        var loader = new THREE.OBJLoader();
          loader.load( '../../../assets/models/1.obj', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({color:controls.colorLamp,metalness:0.5,roughness:0.8});
              child.name="lamp"+lamps.children.length;
            }
          } );

          lampSize=150;lampZ=21;lampX=40;
          object.position.set(0,roomHeight*controls.height-lampSize,0);
          object.scale.x=4;
          object.scale.z=4;
          object.scale.y=4;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
          pointLight(lamps,object.position,controls.colorLight, controls.lightIntensity);
          let v=new function(){
                this.color_light=controls.colorLight;
                this.intens=controls.lightIntensity;
                this.pos=lamps.children[lamps.children.length-1].position;
                this.color_lamp=controls.colorLamp;
                this.type_lamp=controls.lampType;
              };
          data_lamps.push(v);
         
        } );
        }

      };

    this.update= function (e) {

      //удаляем существующее выделение картины
      scene.remove(scene.getObjectByName("outline"));

      camera.position.set(0,400,100);
      //изменение размеров комнаты
      room.scale.x=controls.width;
      room.scale.y=controls.height;
      room.scale.z=controls.depth;
      movePictures();
      moveDoor();
      moveLamp();

      //сохранение пропорций материала стен,потолка и пола при изменении размера
      for (var i = 0; i < room.children.length; i++) {
      if(room.children[i].material.map){
      (i>=0 && i<=1) ?
      room.children[i].material.map.repeat.set(controls.width,controls.depth)
      :
      (i<=3) ?
      room.children[i].material.map.repeat.set(roomWidth*controls.width/500, controls.height*roomHeight/200)
      :
      room.children[i].material.map.repeat.set(roomDepth*controls.depth/300, controls.height*roomHeight/200);
      room.children[i].material.map.needsUpdate = true;
      }}
      };
      this.updateFloorColor=function(e){
        controls.floorPaint=true;
        scene.getObjectByName("floor").material=new THREE.MeshStandardMaterial({color:controls.colorLamp,color:new THREE.Color(e)});
      };
      this.updateFloorTexture=function(e){
        controls.floorPaint=false; 
        var l=JSON.parse(e);
        scene.getObjectByName("floor").material=makeMaterials(l.url,l.x,l.y);

      };
      this.updateWallsColor=function(e){
        controls.wallPaint=true;
        for (var i = 2; i < room.children.length; i++) 
          room.children[i].material=new THREE.MeshStandardMaterial({
            color:new THREE.Color(e),
            metalness: 0.3,
            roughness: 0.9});    
      };
      this.updateWallsTexture=function(e){
        controls.wallPaint=false;
        var l=JSON.parse(e);
         for (var i = 2; i < room.children.length; i++) 
          room.children[i].material=makeMaterials(l.url,l.x,l.y);
      };
      this.updateCeilColor=function(e){
        controls.ceilPaint=true;      
        scene.getObjectByName("ceil").material=new THREE.MeshStandardMaterial({color:new THREE.Color(e)});
      };
      this.updateCeilTexture=function(e){
        controls.ceilPaint=false;        
        var l=JSON.parse(e);
        scene.getObjectByName("ceil").material=makeMaterials(l.url,l.x,l.y);
      };
      this.updateLightIntensity = function (e){
        lamps.children[lamps.children.length-1].intensity=controls.lightIntensity;
        data_lamps[data_lamps.length-1].intens=controls.lightIntensity;
      }
};
//если есть сохраненная комната в бд
if(code){
    controls.width = code[code.length-1].width;
    controls.height = code[code.length-1].height;
    controls.depth = code[code.length-1].depth;

    controls.materialFloor=code[code.length-1].materialFloor;
    controls.materialWalls=code[code.length-1].materialWalls;
    controls.materialCeil=code[code.length-1].materialCeil;

    controls.wallPaint=code[code.length-1].wallPaint;
    controls.ceilPaint=code[code.length-1].ceilPaint;
    controls.floorPaint=code[code.length-1].floorPaint;

    controls.colorFloor=code[code.length-1].colorFloor;
    controls.colorWalls=code[code.length-1].colorWalls;
    controls.colorCeil=code[code.length-1].colorCeil;
   


    window.onload = function() {
    (!controls.ceilPaint) ?
    controls.updateCeilTexture(controls.materialCeil) :
    controls.updateCeilColor(controls.colorCeil);
    (!controls.wallPaint) ?
    controls.updateWallsTexture(controls.materialWalls):
    controls.updateWallsColor(controls.colorWalls);
    (!controls.floorPaint) ?
    controls.updateFloorTexture(controls.materialFloor) :
    controls.updateFloorColor(controls.colorFloor);
        for (var i = 0; i < pictures.children.length; i++) {
          for (var j = 0; j < code.length; j++) {    
            if(pictures.children[i].material.name==code[j].m_id){
              pictures.children[i].name=code[j].p_name;
              pictures.children[i].position.set(code[j].pos.x,code[j].pos.y,code[j].pos.z);
              pictures.children[i].rotation.set(code[j].rot._x,code[j].rot._y,code[j].rot._z);
              pictures.children[i].scale.set(code[j].scale.x,code[j].scale.y,code[j].scale.z);
            }
          }   
        }
        for (var i = maxPictures; i < code.length-1; i++) {
            addSavingLamp(code[i]);            
        }
    controls.update();
    camera.position.set((roomWidth*room.scale.x)/2-80,roomHeight/2,0);

    }

}
else
{
  controls.addLamp();
}


      //элементы управления
      var gui = new dat.GUI({width:400});
      var f1 = gui.addFolder('Кімната');
      f1.add(controls, 'width',  1,2).onChange(controls.update);
      f1.add(controls, 'height', 1,2).onChange(controls.update);
      f1.add(controls, 'depth', 1,2).onChange(controls.update);
      var f2 = f1.addFolder('Підлога');
      f2.addColor(controls,'colorFloor').onChange(controls.updateFloorColor);
      f2.add(controls, 'materialFloor', { 
        "Дерево1": JSON.stringify(texturesForRoom.wood1),
        "Дерево2": JSON.stringify(texturesForRoom.wood2),
        "Дерево3": JSON.stringify(texturesForRoom.wood3),
        "Ламінат": JSON.stringify(texturesForRoom.laminate),
        "Ковролін1": JSON.stringify(texturesForRoom.carpet1),
        "Ковролін2": JSON.stringify(texturesForRoom.carpet2), 
        "Фарба": JSON.stringify(texturesForRoom.paint1),
        "Лід": JSON.stringify(texturesForRoom.ice2),
        "Мармур1": JSON.stringify(texturesForRoom.marble1),
        "Мармур2": JSON.stringify(texturesForRoom.marble2),
        "Мармур3": JSON.stringify(texturesForRoom.marble3),
        "Хвилі": JSON.stringify(texturesForRoom.sea),
        "Мушлі": JSON.stringify(texturesForRoom.seamless),
        "Плитка1": JSON.stringify(texturesForRoom.tile1),
        "Плитка2": JSON.stringify(texturesForRoom.tile3),
      } ).onChange(controls.updateFloorTexture);
      var f3 = f1.addFolder('Стіни');
      f3.addColor(controls,'colorWalls').onChange(controls.updateWallsColor);
      f3.add(controls, 'materialWalls', { 
        "Шпалери1": JSON.stringify(texturesForRoom.wallpaper1),
        "Шпалери2": JSON.stringify(texturesForRoom.wallpaper2),
        "Шпалери3": JSON.stringify(texturesForRoom.wallpaper4),
        "Шпалери4": JSON.stringify(texturesForRoom.wallpaper5),         
      } ).onChange(controls.updateWallsTexture);
      var f4 = f1.addFolder('Стеля');
      f4.addColor(controls,'colorCeil').onChange(controls.updateCeilColor);
      f4.add(controls, 'materialCeil', { 
        "Шпалери1": JSON.stringify(texturesForRoom.wallpaper1),
        "Шпалери2": JSON.stringify(texturesForRoom.wallpaper2),
        "Шпалери3": JSON.stringify(texturesForRoom.wallpaper4),
        "Шпалери4": JSON.stringify(texturesForRoom.wallpaper5),         
      } ).onChange(controls.updateCeilTexture);
      var f5=gui.addFolder('Освітлення');
      f5.addColor(controls,'colorLight');
      f5.addColor(controls,'colorLamp');
      f5.add(controls, 'lightIntensity', 0,2).onChange(controls.updateLightIntensity);;

       f5.add(controls, 'lampType', [
        "Тип 1",
        "Тип 2",
        "Тип 3",
      ] );
      f5.add(controls,'addLamp');
      f5.add(controls,'deleteLamp');
      $(".dg  .property-name").first().text('Довжина');
      $(".dg  .property-name").eq(1).text('Висота');
      $(".dg  .property-name").eq(2).text('Ширина');
      $(".dg  .property-name").eq(3).text('Фарба');
      $(".dg  .property-name").eq(4).text('Підлогове покриття');
      $(".dg  .property-name").eq(5).text('Фарба');
      $(".dg  .property-name").eq(6).text("Текстура");
      $(".dg  .property-name").eq(7).text('Фарба');
      $(".dg  .property-name").eq(8).text("Текстура");
      $(".dg  .property-name").eq(9).text("Віддтінок світла");
      $(".dg  .property-name").eq(10).text("Колір лампи");
      $(".dg  .property-name").eq(11).text("Інтенсивність світла");
      $(".dg  .property-name").eq(12).text("Тип лампи");
      $(".dg  .property-name").eq(13).text("Додати лампу");
      $(".dg  .property-name").eq(14).text("Видалити лампу");


      var control = new THREE.TransformControls( camera, renderer.domElement );
        control.setMode( "scale" );
        scene.add( control );


     

      //обработчик изменения размеров экрана
      window.addEventListener( 'resize', onWindowResize, false );

      //отрисовка сцены
      render();


      function render() {
        //обновление данных о размере комнаты для контроллера управления перемещением
        moveControls.box=room;
        moveControls.update(clock.getDelta());
        requestAnimationFrame(render);
        renderer.render(scene, camera);
      }
      function addSavingLamp(c){
        
        
        if(c.type_lamp=="Тип 1"){
        var loader = new THREE.TDSLoader();
        loader.load( '../../../assets/models/Shade+Lamp-1.3ds', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({color:c.color_lamp,metalness:0.5,roughness:0.8});
              child.name="lamp"+lamps.children.length;
            }
          } );
        
          lampSize=210; lampZ=2; lampX=1;
          object.position.set(c.pos.x,c.pos.y,c.pos.z);
          object.scale.x=3.5;
          object.scale.z=3.5;
          object.scale.y=3.5;
          object.rotation.x=-Math.PI/2;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
          pointLight(lamps,object.position,c.color_light, c.intens);
          let v=new function(){
                this.color_light=c.color_light;
                this.intens=c.intens;
                this.pos=c.pos;
                this.color_lamp=c.color_lamp;
                this.type_lamp=c.type_lamp;
              };
            data_lamps.push(v);
            
        } );
        }
        else if(c.type_lamp=="Тип 2"){
          var loader = new THREE.TDSLoader();
          loader.load( '../../../assets/models/lamp.3ds', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({color:c.color_lamp,metalness:0.5,roughness:0.8});
              child.name="lamp"+lamps.children.length;
            }
          } );
         
          lampSize=210;lampZ=21;lampX=40;          
          object.position.set(c.pos.x,c.pos.y,c.pos.z);
          object.scale.x=1/7;
          object.scale.z=1/7;
          object.scale.y=1/7;
          object.rotation.x=-Math.PI/2;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
         pointLight(lamps,object.position,c.color_light, c.intens);
          let v=new function(){
                this.color_light=c.color_light;
                this.intens=c.intens;
                this.pos=c.pos;
                this.color_lamp=c.color_lamp;
                this.type_lamp=c.type_lamp;
              };
            data_lamps.push(v);
            
        } );
        }
        else if(c.type_lamp=="Тип 3"){
        var loader = new THREE.OBJLoader();
          loader.load( '../../../assets/models/1.obj', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({color:c.color_lamp,metalness:0.5,roughness:0.8});
              child.name="lamp"+lamps.children.length;
            }
          } );
         
          lampSize=150;lampZ=21;lampX=40;
          object.position.set(c.pos.x,c.pos.y,c.pos.z);          
          object.scale.x=4;
          object.scale.z=4;
          object.scale.y=4;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
          pointLight(lamps,object.position,c.color_light, c.intens);
          let v=new function(){
                this.color_light=c.color_light;
                this.intens=c.intens;
                this.pos=c.pos;
                this.color_lamp=c.color_lamp;
                this.type_lamp=c.type_lamp;
              };
          data_lamps.push(v);
        } );
        }
      }


      //создание текстур
      function makeMaterials(url,repeatX,repeatY){
        return new THREE.MeshStandardMaterial({
      map: textureLoader.load(url,function(texture){
        texture.wrapS=texture.wrapT=THREE.RepeatWrapping;
        texture.magFilter=THREE.LinearFilter;
        texture.repeat.set(repeatX,repeatY);
      }),
      metalness: 0.5,
      roughness: 0.9
      });
      }

      
        //перемещение двери при изменении размеров комнаты
        function  moveDoor(){
          scene.getObjectByName("door").position.x=(roomWidth*controls.width)/2-20;
        }
        //перемещение ламп
        function  moveLamp(){
          for (var i = 0; i < lamps.children.length; i++) {
            lamps.children[i].position.y=(roomHeight*controls.height)-210;
          }           
        }
        //перемещение картин вслед за стеной
      function movePictures(){
      for (var i = 0; i < pictures.children.length; i++) {
        (pictures.children[i].name=="topPicture") ?
        pictures.children[i].position.z=(roomDepth*controls.depth)/2 -8
        :
        (pictures.children[i].name=="bottomPicture") ?
        pictures.children[i].position.z=(-roomDepth*controls.depth)/2 +8
        :
        (pictures.children[i].name=="leftPicture") ?
        pictures.children[i].position.x=(-roomWidth*controls.width)/2+8
        :
        pictures.children[i].position.x=(roomWidth*controls.width)/2-8;
      }
      }
      //размещение картин на стене
      function picturesOnWall(x,y,z,picture,deg,flag){
        for (var j = 1; j <= picturesOnTheWallCount; j++) { 
        pictureOnWall(picturesTextures[picture],x,y,z,deg,picture);
        picture++;
        if(flag) x+=pictureDefaultWidth+distanceOfPictures;
        else z+=pictureDefaultWidth+distanceOfPictures;
        }
        return picture;
      }
      //создание одной картины
    function pictureOnWall(url,posX,posY,posZ,deg,pic){
      //создание материала
      var texture = new THREE.TextureLoader().load( url, function(texture) {
          texture.minFilter = THREE.LinearFilter;
          texture.magFilter = THREE.NearestFilter;
          texture.anisotropy = renderer.capabilities.getMaxAnisotropy();
         var material= new THREE.MeshStandardMaterial({
          name:url,
        map: texture,metalness: 0.5,
      roughness: 0.9});
      //сохранение пропорций изображения
         var w= texture.image.naturalWidth;
         var h= texture.image.naturalHeight;
         var p =(w>h) ? w/h:h/w;
  		if(w>=h && w>pictureDefaultWidth)     {
  		h-=(w-pictureDefaultWidth)/p;
  		w=pictureDefaultWidth;
  		}
  		else if(h>w && h>pictureDefaultHeight)     {
  		w-=(h-pictureDefaultHeight)/p;
  		h=pictureDefaultHeight;
  		}
      //создание меша с картиной
      var plane = new THREE.BoxBufferGeometry(w,h, 5,100,100 );
      var quad = new THREE.Mesh( plane, material);      
      quad.position.x = posX;
      quad.position.z = posZ;
      quad.position.y = posY;    
      quad.rotation.y=deg;
      quad.castShadow=true;
      (pic<=picturesOnTheWallCount-1) ? quad.name='topPicture':
      (pic<=picturesOnTheWallCount*2-1) ? quad.name='bottomPicture':
      quad.name='leftPicture';
      pictures.add(quad);

});
}
  function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
      }
  
}
