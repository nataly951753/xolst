  var texturesForRoom= new function () {

this.carpet1=new function (){
  this.url="../../../assets/textures/carpet1.jpg";
  this.x=10;
  this.y=10;
}
this.carpet2=new function(){
  this.url="../../../assets/textures/carpet2.jpg";
  this.x=10;
  this.y=10;
}
this.wood1=new function (){
  this.url="../../../assets/textures/floor-wood.jpg";
  this.x=1;
  this.y=1;
}
this.wood2=new function(){
  this.url="../../../assets/textures/wood3.jpg";
  this.x=3;
  this.y=3;
}
this.wood3=new function(){
  this.url="../../../assets/textures/wood4.jpg";
  this.x=5;
  this.y=5;
}
this.laminate=new function(){
  this.url="../../../assets/textures/laminate.jpg";
  this.x=4;
  this.y=4;
}
this.paint1=new function(){
  this.url="../../../assets/textures/paint2.jpg";
  this.x=3;
  this.y=3;
}
this.marble3=new function(){
  this.url="../../../assets/textures/ice.jpg";
  this.x=3;
  this.y=3;
}
this.ice2=new function(){
  this.url="../../../assets/textures/ice2.jpg";
  this.x=3;
  this.y=3;
}
this.marble1=new function(){
  this.url="../../../assets/textures/marble1.jpg";
  this.x=4;
  this.y=4;
}
this.marble2=new function(){
  this.url="../../../assets/textures/marble2.jpg";
  this.x=7;
  this.y=7;
}
this.sea=new function(){
  this.url="../../../assets/textures/sea.jpg";
  this.x=1;
  this.y=1;
}
this.seamless=new function(){
  this.url="../../../assets/textures/seamless.jpg";
  this.x=3;
  this.y=3;
}
this.tile1=new function(){
  this.url="../../../assets/textures/tile1.jpg";
  this.x=8;
  this.y=8;
}
this.tile3=new function(){
  this.url="../../../assets/textures/tile3.jpg";
  this.x=3;
  this.y=3;
}
this.wallpaper1=new function(){
  this.url="../../../assets/textures/wallpaper1.jpg";
  this.x=7;
  this.y=3;
}
this.wallpaper2=new function(){
  this.url="../../../assets/textures/wallpaper2.jpg";
  this.x=7;
  this.y=2;
}
this.wallpaper4=new function(){
  this.url="../../../assets/textures/wallpaper4.jpg";
  this.x=3;
  this.y=1;
}
this.wallpaper5=new function(){
  this.url="../../../assets/textures/wallpaper5.jpg";
  this.x=3;
  this.y=1;
}
  };