var cameraExhibitorIcon, sceneExhibitor, rendererExhibitor;
var torusKnot1, torusKnot2, torusKnot3, torusKnot4;
var mode = false;

initExhibitorIcon();
animateExhibitorIcon();

function initExhibitorIcon() {
    search_container = document.getElementById( 'ai-exhibitor-icon' );
    cameraExhibitorIcon = new THREE.PerspectiveCamera( 30, window.innerWidth / window.innerHeight, 0.1, 150 );

    sceneExhibitor = new THREE.Scene();
    sceneExhibitor.fog = new THREE.Fog( 'rgba(0,100,200,1)', 0.1, 0.5 );

    rendererExhibitor = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
    rendererExhibitor.setClearColor( 0x000000, 0 );
    rendererExhibitor.setPixelRatio( window.devicePixelRatio );
    rendererExhibitor.setSize( 200,130 );

    // var pointLight = new THREE.PointLight( 0xffffff, 1.5 );
    // pointLight.position.set( 1, 1, -100 );
    // sceneExhibitor.add( pointLight );

    search_container.appendChild( rendererExhibitor.domElement );

    add_loop_object();
}

function add_loop_object() {
  // let dodecahedron = new THREE.DodecahedronGeometry(1, 0);
let torusGeometry = new THREE.TorusKnotGeometry(10,10,1600,0, 7,10,5);
  // loopExhibitor = new THREE.Mesh(dodecahedron, new THREE.MeshPhongMaterial( { color: "#4a707a" } ));
  torusKnot1 = createParticleSystem(torusGeometry);
  torusKnot1.position.z = -100;
  // torusKnot1.position.y = 5;

  torusKnot1.name = "torusKnot1";

  sceneExhibitor.add( torusKnot1 );

  let torusGeometry2 = new THREE.TorusKnotGeometry(8,8,2600,3, 6,11,25);

  torusKnot2 = createParticleSystem(torusGeometry2);
  torusKnot2.position.z = -100;
  // torusKnot2.position.y = 5;
  torusKnot2.name = "torusKnot2";
  sceneExhibitor.add(torusKnot2);

  torusKnot3 = createParticleSystem(torusGeometry2);
  torusKnot3.position.z = -100;
  // torusKnot3.position.y = 5;
  torusKnot3.name = "torusKnot3";
  sceneExhibitor.add(torusKnot3);

  torusKnot4 = createParticleSystem(torusGeometry);
  torusKnot4.position.z = -100;
  // torusKnot3.position.y = 5;
  torusKnot4.name = "torusKnot4";
  sceneExhibitor.add(torusKnot4);
}

function animateExhibitorIcon() {
  requestAnimationFrame( animateExhibitorIcon );
  torusKnot1.rotation.y-=0.02;
  torusKnot1.rotation.x-=0.002;
  // torusKnot1.rotation.z-=0.002;
  torusKnot2.rotation.x+=0.02;
torusKnot2.rotation.z+=0.002;
// torusKnot2.rotation.y+=0.02;
torusKnot3.rotation.y+=0.002;
  torusKnot3.rotation.x+=0.002;
  torusKnot3.rotation.z+=0.002;

    torusKnot4.rotation.x-=0.002;
  torusKnot4.rotation.z-=0.002;
torusKnot4.rotation.y+=0.002;
  if (torusKnot3.scale.x < -10) mode = true;
  if (torusKnot3.scale.x > 10) mode = false;
  torusKnot3.scale.x+= mode ? 0.01 : -0.01;
  torusKnot4.scale.x+= mode ? 0.01 : -0.01;
  torusKnot3.scale.z+= mode ? 0.01 : -0.01;
  torusKnot4.scale.z+= mode ? 0.01 : -0.01;
    torusKnot3.scale.y+= mode ? 0.01 : -0.01;
  torusKnot4.scale.y+= mode ? 0.01 : -0.01;
  cameraExhibitorIcon.lookAt( sceneExhibitor.position );
  rendererExhibitor.render( sceneExhibitor, cameraExhibitorIcon );
}

// from THREE.js examples
function generateSprite() {

    var canvas = document.createElement('canvas');
    canvas.width = 16;
    canvas.height = 16;

    var context = canvas.getContext('2d');
    var gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2);
    gradient.addColorStop(0, 'rgba(25,48,70,1)');
    gradient.addColorStop(0.4, 'rgba(0,100,200,1)');
    gradient.addColorStop(0.8, 'rgba(170,191,208,1)');
    gradient.addColorStop(1, 'rgba(0,0,0,0)');

    context.fillStyle = gradient;
    context.fillRect(0, 0, canvas.width, canvas.height);

    var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;
    return texture;

}

function createParticleSystem(geom) {
    var material = new THREE.PointsMaterial({
        color: 0xffffff,
        size: 0.4,
        transparent:true,
        blending: THREE.AdditiveBlending,
        map: generateSprite()
    });

    var system = new THREE.Points(geom, material);
    system.sortParticles = true;
    return system;
}