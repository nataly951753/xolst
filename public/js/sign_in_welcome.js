var container2, camera2, cameraTarget, scene2, renderer2;
var pointLight, torusKnot, torusKnot2, mouseX=0, mouseY=0;
var move=true;

var windowHalfX = window.innerWidth / 2;
var windowHalfY = window.innerHeight / 2;
init2();
animate2();

function init2() {
    container2 = document.getElementById( 'fifth' );
    camera2 = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 0.1, 100 );
    camera2.position.z = 15;
    // camera2.position.y = -0.003;

    scene2 = new THREE.Scene();
    // scene2.fog = new THREE.Fog( 0xffffff, 0.1, 15 );

    renderer2 = new THREE.WebGLRenderer( { antialias: true, alpha: true } );
    renderer2.setClearColor( 0x000000, 0 );
    renderer2.setPixelRatio( window.devicePixelRatio );
    renderer2.setSize(window.innerWidth, window.innerHeight);

    container2.appendChild( renderer2.domElement );

    add_torus_knot();

    window.addEventListener( 'resize', onWindowResize, false );
    container2.addEventListener( 'mousemove', onDocumentMouseMove, false );

}

function onDocumentMouseMove(event) {
	mouseX = ( event.clientX - windowHalfX );
    mouseY = ( event.clientY - windowHalfY );
}

function add_torus_knot() {
	let torusGeometry = new THREE.TorusKnotGeometry(10,10,1600,1, 7,10,5);
	torusKnot = createParticleSystem(torusGeometry);
	torusKnot.position.z = -25;
	torusKnot.name = "torusKnot";
	scene2.add(torusKnot);
	let torusGeometry2 = new THREE.TorusKnotGeometry(35,35,2600,3, 6,11,25);

	torusKnot2 = createParticleSystem(torusGeometry2);
	torusKnot2.position.z = -25;
	torusKnot2.name = "torusKnot";
	scene2.add(torusKnot2);
}

// from THREE.js examples
function generateSprite() {

    var canvas = document.createElement('canvas');
    canvas.width = 16;
    canvas.height = 16;

    var context = canvas.getContext('2d');
    var gradient = context.createRadialGradient(canvas.width / 2, canvas.height / 2, 0, canvas.width / 2, canvas.height / 2, canvas.width / 2);
    gradient.addColorStop(0, 'rgba(25,48,70,1)');
    gradient.addColorStop(0.4, 'rgba(0,100,200,1)');
    gradient.addColorStop(0.8, 'rgba(170,191,208,1)');
    gradient.addColorStop(1, 'rgba(0,0,0,0)');

    context.fillStyle = gradient;
    context.fillRect(0, 0, canvas.width, canvas.height);

    var texture = new THREE.Texture(canvas);
    texture.needsUpdate = true;
    return texture;

}

function createParticleSystem(geom) {
    var material = new THREE.PointsMaterial({
        color: 0xffffff,
        size: 0.4,
        transparent:true,
        blending: THREE.AdditiveBlending,
        map: generateSprite()
    });

    var system = new THREE.Points(geom, material);
    system.sortParticles = true;
    return system;
}

function onWindowResize() {
windowHalfX = window.innerWidth / 2;
windowHalfY = window.innerHeight / 2;
camera2.aspect = window.innerWidth / window.innerHeight;
camera2.updateProjectionMatrix();
renderer2.setSize( window.innerWidth, window.innerHeight );
}

function animate2() {
requestAnimationFrame( animate2 );
torusKnot.rotation.y+=0.02;
torusKnot.rotation.z += ( mouseY - camera2.position.z ) * 0.0001;
torusKnot.rotation.y += ( mouseX - camera2.position.y ) * 0.0001;
torusKnot2.rotation.x-=0.02;
torusKnot2.rotation.z -= ( mouseY - camera2.position.z ) * 0.0001;
torusKnot2.rotation.y -= ( mouseX - camera2.position.y ) * 0.0001;
camera2.lookAt( scene2.position );
renderer2.render( scene2, camera2 );
}