
function setExhibitorText(text) {
  $("#ai-exhibitor-response").empty();
  $("#ai-exhibitor-answer").val("");

  $("<p>" + text + "</p>").appendTo("#ai-exhibitor-response");

  animateExhibitorText();
}

function animateExhibitorText() {
  $.fn.animate_Text = function() {
  var string = this.text();
  return this.each(function(){
   var $this = $(this);
   $this.html(string.replace(/./g, '<span class="new">$&</span>'));
   $this.find('span.new').each(function(i, el){
    setTimeout(function(){ $(el).addClass('div_opacity'); }, 20 * i);
   });
  });
 };
 $('#ai-exhibitor-response').animate_Text();
}


function activateExhibitor(ex_id, user_id) {
  sendAIrequest(
    "post", 
    JSON.stringify({
      "ex_id": ex_id,
      "user_id": user_id
    })
  );
}

function sendAIrequest(method_, data_) {
  $.ajax({ 
        url: "http://127.0.0.1:5000/start", 
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        // contentType: 'application/json',
        dataType: "json", // Для использования JSON формата получаемых данных
        method: method_, // Что бы воспользоваться POST методом, меняем данную строку на POST   
        data: data_,
        success: function(data) {
          handleAIresponse(data);
        },
        error: function(data) {
          handleAIresponseError(data);
        }
    });
}

function handleAIresponse(data) {
  response = JSON.parse(JSON.parse(data));
  console.log(response); // Возвращаемые данные выводим в консоль
  if (response.action_type == 1) {
    setExhibitorText(response.msg);
  }
} 

function handleAIresponseError(error) {
  console.log(error); // Возвращаемые данные выводим в консоль
} 

















