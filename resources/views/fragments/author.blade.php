 <div class="card mt-4 col-5" >
  <h4>
  <span class="badge badge-success author-badge exh-open">
  	<i class="fas fa-crown"><span class="icon-text">{{$a->subscribers()->count()}}</span></i>
  	<i class="fas fa-clipboard-check"><span class="icon-text">{{$a->exhibitions()->count()}}</span></i>
  </span>
  </h4>
  <div class="photo_url">
    @if($a->photo_url)
    <img class="mt-1"style="width: 100%" src="{{asset('storage/users/'.$a->photo_url)}}">
    @else
    <img class="mt-1"style="width: 100%;" src="{{asset('images/back_example.jpg')}}">
    
    @endif
  </div>
  
  <a href="{{ route('one_author', ['id'=>$a->id])}}" class="exh-author my-2">{{$a->name}}</a>      
</div>
 