<i class="fa fas fa-eye"><span class="icon-text">  {{$exh->views}}  </span></i>
<i class="fa fas fa-crown"><span class="icon-text">{{$exh->favourites()->count()}}</span></i> 
@if($exh->ratings_count>0)
<i class="fa fas fa-star"><span class="icon-text">  {{round($exh->ratings/$exh->ratings_count,2)}}  </span></i>
@else
<i class="fa fas fa-star"><span class="icon-text">  0  </span></i>
@endif
<i class="fa fas fa-edit"><span class="icon-text">  {{ $exh->reviews->count() }}  </span></i>
<i class="fa fas fa-image"><span class="icon-text">  {{ $exh->pictures->count() }}  </span></i>