 <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 mb-4" >
  <div >
    @if($a->photo_url)
    <img class="mt-1 mb-4"style="width: 100%" src="{{asset('storage/users/'.$a->photo_url)}}">
    @else
    <img class="mt-1 mb-4"style="width: 100%;" src="{{asset('images/back_example.jpg')}}">
    
    @endif
  </div>
  
  <a href="{{ route('one_author', ['id'=>$a->id])}}" class="f-auth-link py-3  pl-4" >{{$a->name}}</a>      
  <div class="pl-2 mt-3">{{$a->about}}</div>      
</div>
 