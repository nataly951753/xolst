 <div class="card mt-4" >
  <h4>@if($date->between($exh->started_at, $exh->finished_at))
  <span class="badge badge-success author-badge exh-open">Відкрилась!</span>
  @elseif($date->lessThan($exh->started_at))
  <span class="badge badge-primary author-badge exh-future">Запланована</span>
  @else
  <span class="badge badge-danger author-badge exh-last">Закрилася</span>
  @endif
  </h4>
  <div class="photo_url">
    @if($exh->photo_url)
    <img class="mt-1 pr-1"style="width: 100%" src="{{asset('storage/exhibitions/'.$exh->photo_url)}}">
    @else
    <img class="mt-1 pr-1"style="width: 100%;" src="{{asset('images/back_example.jpg')}}">
    
    @endif
  </div>
  
  <a href="{{ route('one_exhibition', ['id'=>$exh->id])}}" class="exh-author my-2">{{$exh->name}}</a>      
</div>