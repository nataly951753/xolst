<div class="card mt-4 all-reviews slider-review" >
<div class="card-body">
<h5 class="card-title mt-2"><a class="all-reviews-name text-uppercase" href="{{route('one_review',['id'=>$review->id])}}">{{$review->name}}</a></h5>
	@if(isset($author))
<h6 class="card-title mt-2 text-right all-reviews-author-name"> ВИСТАВКА - <a href="{{route('one_exhibition',['id'=>$review->exhibition_id])}}">{{$review->exhibition->name}}</a></h6>
@else
<h6 class="card-title mt-2 text-right all-reviews-author-name"> АВТОР РЕЦЕНЗІЇ - <a href="{{route('one_author',['id'=>$review->author_id])}}">{{$review->author->name}}</a></h6>
@endif
<p class="card-text">{{ $review->about }}</p>
<p class="review-date text-right"> Опубліковано {{$review->created_at}}</p>
</div>
</div>