<div class="row">
    <div class="col-1 bg-white pl-0 pr-0 mr-2"></div>
    <div class="col-10  ">
      <div class="row">
           <div class="col-12">
             <div class="row bg-white">
               <div class="col-12">
                
                 <h4 class="mt-3 exh-title">{{$exh->name}}
                  @if($date->between($exh->started_at, $exh->finished_at))
                  <span class="badge badge-success exh-badge exh-open">Відкрилась!</span>
                  @elseif($date->lessThan($exh->started_at))
                  <span class="badge badge-primary exh-badge exh-future">Запланована</span>
                  @else
                  <span class="badge badge-danger exh-badge exh-last">Закрилась</span>
                  @endif

                 </h4>
               </div>
             </div>
              <div class="row justify-content-between">
              <div class="col-1 bg-white"></div>
               <div class="col-10 exh-line"></div>
              </div>
              <div class="row bg-white">
               <div class="col-12 mt-2 mb-3 text-center">
                 <a href="{{route('one_author',['id'=>$exh->author->id])}}" class="exh-author">{{$exh->author->name}}</a>
               </div>
             </div>
             <div class="row exh-type bg-white pb-2">
              <div class="col-12">
               {{$exh->type->name}} - {{$exh->style->name}}
             </div>
             </div>
             <div class="row bg-white"  style="min-height: 110px">
              <div class="col-12">
                 {{$exh->about}}
              </div>
            </div>
            <div class="row bg-white" style="min-height: 25px">
              @if($exh->tags->count())
                  @foreach($exh->tags->forPage(1,3) as $tag)
                   <a href="{{route('all_exhibitions',['type'=>$exh_type, 'style'=>$exh_style,'tag'=>$tag->name])}}" class="exh-author"><span class="badge badge-primary exh-one-tag ml-3 mt-2"> # {{$tag->name}}</span></a>
                  @endforeach
              @endif
             </div>
             
              <div class="row justify-content-end pt-2 bg-white icons">
                @include('fragments.exh-icons')
              </div>
              @if($date->between($exh->started_at, $exh->finished_at))
              <div class="row justify-content-center exh-date py-2 bg-white" style="color: #4caf50">
              @else
              <div class="row justify-content-center exh-date py-2 bg-white">
              @endif
                з  {{$exh->started_at->format('d-m-Y')}}       до  {{$exh->finished_at->format('d-m-Y')}}
              </div>
              <div class="row justify-content-center bg-white">
               <a href="{{ route('one_exhibition', ['id'=>$exh->id])}}" class="btn  col-4 exh-btn mb-3">
                ДОКЛАДНІШЕ
               </a>
                
              </div>
         
      </div>
    </div>

  </div>
   
</div>