    <a class="dropdown-item" href="{{route('account')}}" class="card-link">ПРОФІЛЬ</a>
    <a class="dropdown-item" href="{{route('user_favourites_exhibitions')}}" class="card-link">ОБРАНІ ВИСТАВКИ</a>
    <a class="dropdown-item" href="{{route('user_favourites_authors')}}" class="card-link">ОБРАНІ АВТОРИ</a>
    <a class="dropdown-item" href="{{ route('account_edit') }}" class="card-link">РЕДАГУВАТИ ПРОФІЛЬ</a>
    @if(Auth::user()->role->name=='author')
    <a class="dropdown-item" href="{{ route('show_requests') }}" class="card-link">ЗАЯВКИ</a>
    <a class="dropdown-item" href="{{ route('new_exhibition') }}" class="card-link">НОВА ВИСТАВКА</a>
   <a class="dropdown-item" href="{{ route('user_exhibitions') }}" class="card-link">МОЇ ВИСТАВКИ</a>
   @elseif(Auth::user()->role->name=='reviewer')
   <a class="dropdown-item" href="{{ route('user_reviews') }}" class="card-link">МОЇ РЕЦЕНЗІЇ</a>
   @elseif(Auth::user()->role->name=='user')
   <a class="dropdown-item" href="{{ route('change_status') }}" class="card-link">ОТРИМАТИ НОВИЙ СТАТУС</a>
   @endif
    <a class="dropdown-item" href="{{ route('logout') }}" class="card-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">ВИХІД</a>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
    </form>
    
 