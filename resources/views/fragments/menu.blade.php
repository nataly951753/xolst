 <nav class="navbar navbar-expand-md navbar-light navbar-laravel mynavbar">
            <div class="container">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto left-part">
                        <a href="{{route('main_menu')}}" id="logo" ></a>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto right-part">
                            <li class="nav-item {{-- active --}}">
                                <a  href="{{ route('all_reviews')}}">РЕЦЕНЗІЇ</a>
                              </li>
                             <li class="nav-item {{-- active --}}">
                                <a  href="{{ route('all_exhibitions')}}">ВИСТАВКИ</a>
                              </li>
                              <li class="nav-item">
                                <a  href="{{ route('all_authors')}}">АВТОРИ</a>
                              </li>
                        <!-- Authentication Links -->
                        @guest
                            
                            <li class="nav-item">
                                <a  href="{{ route('login') }}">{{ __('УВІЙТИ') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a  href="{{ route('register') }}">{{ __('ПРИЄДНАТИСЬ') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <a  href="{{ route('account')}}">КАБІНЕТ</a>
                              </li>
                             {{--  <li class="nav-item dropdown ">
                                <a  id="navbarDropdown"  href="{{route('account')}}" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" v-pre>КАБІНЕТ <i class="fas fa-angle-down"></i></a>
                                <div class="dropdown-menu dropdown-menu-right text-center" aria-labelledby="navbarDropdown">
                                    @include('fragments.account-menu')

                                    
                                </div>
                              </li> --}}
                              
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>