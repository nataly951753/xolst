@extends('layouts.app')
@section('title', 'Xolst - Підтвердження реєстрації')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Перевірте свою адресу електронної пошти') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('На вашу адресу електронної пошти було відправлене ​​посилання для підтвердження.') }}
                        </div>
                    @endif

                    {{ __('Перш ніж продовжити, перевірте свою електронну пошту на наявність посилання для підтвердження.') }}
                    {{ __('Якщо ви не отримали листа') }}, <a href="{{ route('verification.resend') }}">{{ __('натисніть тут, щоб запросити інший') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $("main").css("margin-bottom", 0);
</script>
@endsection
