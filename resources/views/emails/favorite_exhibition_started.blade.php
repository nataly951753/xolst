<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Виставка відкрилась!</title>
	
{{-- <link rel="stylesheet" type="text/css" href="{{asset('css/email.css')}}" /> --}}
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Cormorant|Cormorant+Garamond|Pattaya|Nunito|Playball|Lobster|Lora|Playfair+Display|Open+Sans" rel="stylesheet">
{{-- <link rel="stylesheet" href="{{ asset('css/app.css') }}"> --}}

<style type="text/css">
	.inspire {
		font-family: "Playfair Display", serif;
		font-size: 24px;
		font-style: italic;
		padding-left: 10%;
		padding-top: 3%; 
		padding-bottom: 3%;
	}
	.site-name {
		font-family: "Playfair Display", serif;
		font-size: 30px;
		text-align: right; 
		padding-right: 10%;
		font-weight: bold;
		letter-spacing: 5px;
	}
	.first-block {
		text-align: center;
	}
	.first-block .title, .date{
		color:#3f9384;
		font-family: "Cormorant", serif;
	}
	.btn {
		width: 40%;
		background-color: #121528;
		font-family: "Playfair Display", serif;
		text-align: center;
		vertical-align: middle;
		padding: 2% 6%;
		margin: 0 auto;

	}
	.btn a{
		color: #d3dbf6;
		font-size: 20px;
		text-decoration: none;
		text-transform: uppercase;
	}
	.btn a:hover{
		color:#bfbdbe;
	}
	.btn:hover{
		background-color: black;
	}
	.author {
	font-family: "Playfair Display", serif;
  font-size: 1.2rem;
  text-transform: uppercase;
	}
	.author a{
color: #121528;
text-decoration: none;
text-transform: none;
	font-family: "Playfair Display", serif;
  text-shadow: 1px 1px 2px;
	}
	.author a:hover {
  text-decoration: none;
  color: #6f88c4;
}
.about {
	border: 2px solid #e4e4ec;
	margin: 2% 1%;
	font-family: "Playfair Display", serif;
	font-size: 18px;

}
.footer{
	text-align: center;
	font-family: "Playfair Display", serif;
		font-size: 14px;
		font-style: italic;
		background-color: #e4e4ec;
		padding: 1%;
}
</style>

</head>
 
<body bgcolor="#FFFFFF" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">

<!-- HEADER -->
<table class="head-wrap" bgcolor="#e4e4ec" style="width: 100%;">
	<tr style="width: 100%">
		<td class="header container" style="width: 100%" >
				<table bgcolor="#e4e4ec" style="width: 100%">
					<tr style="width: 100%">
						<td class="inspire" style="width: 60%;">
							Краса у кожній миті...							
						</td>
						<td  class="site-name" style="width: 40%">XOLST</td>
					</tr>
				</table>
		</td>
	</tr>
</table><!-- /HEADER -->

<!-- BODY -->
<table class="body-wrap">
	<tr>
		<td></td>
		<td class="container" bgcolor="#FFFFFF">
			
			<div>
				
				<div class="first-block">
				<table>
				<tr>
					<td>				
						
						<h1 class="title">Виставка {{$exhibition->name}} відкрилась - 
поспішайте відвідати!</h1>
						<table>
						<tr>
							<td>
								<h3 class="author">Автор - <a href="{{route('one_author',['id'=>$exhibition->author->id])}}" class="exh-author">{{$exhibition->author->name}}</a></h3>
								<p class="about"> {{$exhibition->about}}</p>
								<p class="date" style="font-size: 16px;">Виставка буде відкрита з  {{$exhibition->started_at->format('d-m-Y')}}       до  {{$exhibition->finished_at->format('d-m-Y')}}</p>
								<div class="btn">
								<a  href="{{ route('one_exhibition', ['id'=>$exhibition->id])}}">Відвідати виставку</a></div>
							</td>
							{{-- <td>
								<img src="{{$message->embed('https://www.google.com.ua/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&ved=2ahUKEwjcpa_N5oXkAhVvkIsKHZQoDlMQjRx6BAgBEAQ&url=https%3A%2F%2Fafisha.tut.by%2Fexhibition%2Fgustav_klimt_100_let_bez_geniya%2F&psig=AOvVaw0eSxyrAxQT5ME1gzyjWT7g&ust=1565990289485191')}}">
								@if($exhibition->photo_url)
								<img src="{{ asset('storage/exhibitions/'.$exhibition->photo_url)}}" />
								@else
								<img src="{{ asset('images/back_example.jpg') }}" />
								@endif
							</td> --}}
						</tr>
						</table>

						<div>{!! $exhibition->description !!}</div>

					</td>
				</tr>
				</table>
				</div>
			</div>
		</td>
		<td></td>
	</tr>
</table><!-- /BODY -->

<!-- FOOTER -->
<table class="footer-wrap" style="width: 100%">
	<tr style="width: 100%">
		<td class="footer" style="width: 100%">
			<em>
				З повагою команда Xolst...
			</em>
		</td>
	</tr>
</table><!-- /FOOTER -->

</body>
</html>