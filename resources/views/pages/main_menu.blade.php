<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Xolst - Меню</title>
    <link rel="shortcut icon" href="{{asset('images/xolst.ico')}}" type="image/x-icon">

    <script src="{{ mix('js/app.js') }}"></script>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{asset('css/room.css')}}">
    
    <link href="https://fonts.googleapis.com/css?family=Cormorant|Playball" rel="stylesheet">
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/three.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/basicSettings.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/main-menu.js')}}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/renderers/Projector.js')}}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/controls/FirstPersonControls.js')}}"></script>
    <script src="{{asset('js/preloader.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tween.js/16.3.5/Tween.min.js"></script>

</head>

<body data-page="{{ Route::currentRouteName() }}">
    @include('fragments.loader')

    <div id="page_title" class="justify-content-center">
            <h1 class="ml3">Xolst</h1>
        
    </div>

    <div id="blocker">
        <div id="instructions">
            <h1 class="ml12">Xolst</h1>
            <h1 class="ml1 mt-2">
            <span class="text-wrapper">
            <span class="line line1"></span>
            <span class="letters menu-text">ГОЛОВНЕ МЕНЮ</span>
            <span class="line line2"></span>
            </span>
            </h1>

        </div>
    </div>

    <div id="webgl-output"></div>
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
    </form>
    <script type="text/javascript">
        var textWrapper = document.querySelector('.ml12');
textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

anime.timeline({loop: true})
  .add({
    targets: '.ml12 .letter',
    translateX: [40,0],
    translateZ: 0,
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 1200,
    delay: function(el, i) {
      return 5000 + 30 * i;
    }
  }).add({
    targets: '.ml12 .letter',
    translateX: [0,-30],
    opacity: [1,0],
    easing: "easeInExpo",
    duration: 1100,
    delay: function(el, i) {
      return 100 + 30 * i;
    }
  });
        var textWrapper2 = document.querySelector('.ml1 .letters');
textWrapper2.innerHTML = textWrapper2.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

  anime.timeline({loop: true})
  .add({
    targets: '.ml1 .letter',
    scale: [0.3,1],
    opacity: [0,1],
    translateZ: 0,
    easing: "easeOutExpo",
    duration: 600,
    delay: function(el, i) {
      return 500 * (i+1)
    }
  }).add({
    targets: '.ml1 .line',
    scaleX: [0,1],
    opacity: [0.5,1],
    easing: "easeOutExpo",
    duration: 700,
    offset: '-=875',
    delay: function(el, i, l) {
      return 800 * (l - i);
    }
  }).add({
    targets: '.ml1',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 5000
  });
//page title anima
 
        var xost="{{config('app.url')}}";

        var user = {{$user}};

     (function () { 
        init();
        animate();
     })();
      </script>
</body>
</html>