@extends('layouts.app')
@section('title', "Xolst - $review->name")
@section('content')
<div class="container container-one-review mb-4">
    <div class="row justify-content-center content-bg pb-5">

        <div class="col-12 col-lg-10">
                <div class="row flex-column justify-content-between align-items-center my-4 mx-3 ">
                <h1 class="text-center all-title mt-2">{{$review->name}}</h1>
                </div>  
                   
                    
                  <p class="card-text">{!! $review->description !!}</p>  
                  <h6 class="card-title mt-2 text-right review-date"> {{$review->author->name}}</h6>
                <p class="review-date text-right"> Опубліковано {{$review->created_at}}</p>
                    <div class="d-flex justify-content-around"> 
                  <a href="{{ route('one_exhibition', ['id'=>$review->exhibition_id])}}" class="btn btn-outline-primary favourite">Дізнатися про виставку</a>   
                  <a href="{{ route('one_author', ['id'=>$review->author_id])}}" class="btn btn-outline-primary favourite">Дізнатися про автора</a>  
                  </div>      

              </div>
            </div>
        
    </div>
@endsection
