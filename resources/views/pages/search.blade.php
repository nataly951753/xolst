<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Xolst - Пошук</title>
    <link rel="shortcut icon" href="{{asset('images/xolst.ico')}}" type="image/x-icon">

    <script src="{{ mix('js/app.js') }}"></script>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{asset('css/room.css')}}">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Cormorant|Playball|Lobster|Lora|Playfair+Display|Open+Sans" rel="stylesheet">
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/three.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/basicSettings.js')}}"></script>

    <script type="text/javascript" src="{{asset('js/search.js')}}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/renderers/Projector.js')}}"></script>
    <script src="{{asset('js/preloader.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tween.js/16.3.5/Tween.min.js"></script>

</head>

<body data-page="{{ Route::currentRouteName() }}">
    @include('fragments.loader')
    <div id="blocker">
        <div id="instructions">
            <h1 class="ml12">Xolst</h1>
            <h1 class="ml1 mt-2">
            <span class="text-wrapper">
            <span class="line line1"></span>
            <span class="letters menu-text">ПОШУК ЗА ВИСТАВКАМИ</span>
            <span class="line line2"></span>
            </span>
            </h1>

        </div>
    </div>
    <div class="search-no-content">
            <h1>Немає результатів пошуку</h1>
        </div>
        <div class="search-exh-title">
            <h1 class="ml16">Title</h1>
        </div>
        <div class="search-exh-info">
            <div class="row justify-content-center">
               <div class="col-6" style="background:rgba(255,255,255,0.7);">
                    <div class="row">
                    <h4>
                      <span class="badge badge-success exh-badge exh-open search-badge">Відкрилася!</span>
                      <span class="badge badge-primary exh-badge exh-future search-badge">Запланована</span>
                      <span class="badge badge-danger exh-badge exh-last search-badge">Закрилася</span>
                    </h4>
                  </div>
                  <div class="row">
                      <div class="col-12 py-4 px-4 about">
                         Description
                       </div>
                  </div>
               </div>
             </div>
        </div>
    <div class="input-search">
  <span class="input input--haruki">
    <input class="input__field input__field--haruki" type="text" id="input-1" />
    <label class="input__label input__label--haruki" for="input-1">
        <span class="input__label-content input__label-content--haruki">Уведіть запит...</span>
    </label>
</span>   
<button type="submit"><i class="fas fa-search"></i></button>
    </div>
<div class="paginate">
        <span><i class="fas fa-chevron-left"></i></span>
        <span><i class="fas fa-chevron-right"></i></span>
    </div>
    <div class="menu-search" style="display: none; justify-content: space-between;">   
        <a href="{{route('main_menu')}}" id="logo" ></a>
        <a href="" id="search" ></a>
    </div>


<div id="webgl-output"></div>
     <script type="text/javascript">
        var textWrapper = document.querySelector('.ml12');
textWrapper.innerHTML = textWrapper.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

anime.timeline({loop: true})
  .add({
    targets: '.ml12 .letter',
    translateX: [40,0],
    translateZ: 0,
    opacity: [0,1],
    easing: "easeOutExpo",
    duration: 1200,
    delay: function(el, i) {
      return 5000 + 30 * i;
    }
  }).add({
    targets: '.ml12 .letter',
    translateX: [0,-30],
    opacity: [1,0],
    easing: "easeInExpo",
    duration: 1100,
    delay: function(el, i) {
      return 100 + 30 * i;
    }
  });
        var textWrapper2 = document.querySelector('.ml1 .letters');
textWrapper2.innerHTML = textWrapper2.textContent.replace(/([^\x00-\x80]|\w)/g, "<span class='letter'>$&</span>");

  anime.timeline({loop: true})
  .add({
    targets: '.ml1 .letter',
    scale: [0.3,1],
    opacity: [0,1],
    translateZ: 0,
    easing: "easeOutExpo",
    duration: 600,
    delay: function(el, i) {
      return 500 * (i+1)
    }
  }).add({
    targets: '.ml1 .line',
    scaleX: [0,1],
    opacity: [0.5,1],
    easing: "easeOutExpo",
    duration: 700,
    offset: '-=875',
    delay: function(el, i, l) {
      return 800 * (l - i);
    }
  }).add({
    targets: '.ml1',
    opacity: 0,
    duration: 1000,
    easing: "easeOutExpo",
    delay: 5000
  });
 
        var xost="{{config('app.url')}}";

        

     (function () { 
        init();
        animate();
     })();
     
      </script>
      <script type="text/javascript" src="{{asset('js/search-icon.js')}}"></script>
        <script type="text/javascript" src="{{asset('js/logo.js')}}"></script>
</body>
</html>