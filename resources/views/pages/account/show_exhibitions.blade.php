@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Мої виставки</h1>               
      <div class="row justify-content-around" >
      @foreach($my_exhibitions as $exh)
      
      <div class="card col-10 col-md-5 col-lg-5 mt-5 mb-3">
        <div  class="text-center pic-e" style="z-index: 100">
        @if($exh->photo_url)
    <img   src="{{asset('storage/exhibitions/'.$exh->photo_url)}}">
    @else
    <img   src="{{asset('images/back_example.jpg')}}">
    
    @endif
    </div>
     <a href="{{ route('one_exhibition', ['id'=>$exh->id])}}" class="mt-3 exh-title">{{$exh->name}}
                  @if($date->between($exh->started_at, $exh->finished_at))
                  <span class="badge badge-success exh-badge exh-open" style="z-index: 101" >Відкрилась!</span>
                  @elseif($date->lessThan($exh->started_at))
                  <span class="badge badge-primary exh-badge exh-future" style="z-index: 101" >Запланована</span>
                  @else
                  <span class="badge badge-danger exh-badge exh-last" style="z-index: 101" >Закрилась</span>
                  @endif

                 </a>
        <div class="card-body p-0">
             
             <div class="row exh-type bg-white pb-2">
              <div class="col-12">
               {{$exh->type->name}} - {{$exh->style->name}}
             </div>
             </div>
             <div class="row bg-white">
              <div class="col-12">
                 {{$exh->about}}
              </div>
             </div>
              <div class="row justify-content-end pt-2 bg-white icons">
                @include('fragments.exh-icons')
              </div>
              @if($date->between($exh->started_at, $exh->finished_at))
              <div class="row justify-content-center exh-date py-2 bg-white" style="color: #4caf50" >
              @else
              <div class="row justify-content-center exh-date py-2 bg-white">
              @endif
                з  {{$exh->started_at->format('d-m-Y')}}       до  {{$exh->finished_at->format('d-m-Y')}}
              </div>
        </div>
        <div class="card-footer">
        <a href="{{route('edit_exhibition', ['id'=>$exh->id])}}" class="btn btn-outline-primary btn-block ">Редагувати</a>
        <a href="{{route('pictures_for_exhibition', ['id' => $exh->id])}}" class="btn btn-outline-primary btn-block ">Додати картини</a>
        <a href="{{route('pictures_show', ['id'=>$exh->id])}}" class="btn btn-outline-primary btn-block ">Редагувати картини</a>
        <a href="{{route('room_settings', ['id'=>$exh->id])}}" class="btn btn-outline-primary btn-block ">Редагувати залу</a> 
        <a href="{{ route('room_show', ['id'=>$exh->id])}}" class="btn btn-outline-success btn-block ">Переглянути</a>   
        <a href="{{route('delete_exhibition', ['id'=>$exh->id])}}" class="btn btn-outline-danger btn-block ">Видалити</a>
        </div>
      </div>

      @endforeach
      </div>
      
      <div class="row justify-content-center mt-4" >
                {{ $my_exhibitions->links() }}                  
      </div>
            
@endsection
