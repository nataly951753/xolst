@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Редагування профілю</h1>                
    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form  method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="form-group">
                    <label for="name">Ім'я</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Ім'я" value="{{ Auth::user()->name }}">
                  </div>
                    <input type="file" id="photo" name="photo" accept="image/jpeg,image/png" class="inputfile">
                               
                  <div class="form-group">
                    <label for="about">Коротка інформація про вас</label>
                    <textarea class="form-control" id="about" name="about" rows="6" maxlength="{{config('variables.size_user_about')}}">{{ Auth::user()->about }}</textarea>
                  </div>

                  <div class="form-group">
                    <label for="page_description">Сторінка про вас</label>
                    <textarea class="form-control" id="page_description" name="page_description" rows="20" >{{ Auth::user()->information }}</textarea>
                  </div>                  
                  <div class="row justify-content-center my-4">
                  <button type="submit" class="btn btn-outline-primary btn-change-account">Зберегти зміни</button>
                  </div>
                </form>
              
<script>
  tinymce.init({
    selector: '#page_description',
    plugins: 'searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern emoticons',
    toolbar: ' bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | emoticons',
    image_advtab: true,
    image_caption: true
  });
</script>
@endsection
