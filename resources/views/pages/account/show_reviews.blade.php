@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Мої рецензії</h1>               
      <div class="row justify-content-around" >
      @foreach($reviews as $review)
      
      <div class="card col-10 col-md-5 col-lg-5 mt-5 mb-3">
        
     <a href="{{ route('one_review', ['id'=>$review->id])}}" class="mt-3 exh-title">{{$review->name}}</a>
        <div class="card-body p-0">
             
             <div class="row exh-type bg-white pb-2">
              <div class="col-12">
               {{$review->exhibition->name}}
             </div>
             </div>
             <div class="row bg-white">
              <div class="col-12">
                 {{$review->about}}
              </div>
             </div>
        </div>
        <div class="card-footer text-right">
        <a href="{{route('edit_review', ['id'=>$review->id])}}" class="btn btn-primary "><i class="fas fa-pen"></i></a>   
        <a href="{{route('delete_review', ['id'=>$review->id])}}" class="btn btn-danger "><i class="fas fa-trash-alt"></i></a>
        </div>
      </div>

      @endforeach
      </div>
      
      <div class="row justify-content-center mt-4" >
                {{ $reviews->links() }}                  
      </div>
            
@endsection
