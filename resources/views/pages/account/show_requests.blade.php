@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Заявки на відвідування виставок</h1>                

    
                
                <div class="row justify-content-around" >
               @foreach($requests as $req)
               @foreach($req->requests_exhibition as $r)

                @if($r->pivot->response===null)
                
                <div class="card col-5 " style="margin-top: 20px">
                   
                <div class="card-body">
                  {{-- {{dd()}} --}}
                  <h5 class="estimate"><a href="{{route('one_author',['id'=>$r->id])}}">{{$r->name}}</a></h5>                  
                  <p class="card-text"><a href="{{route('one_exhibition',['id'=>$req->id])}}">{{$req->name}}</a></p>
                  <a href="{{route('request_approve',['user_id'=>$r->id, 'exh_id'=>$req->id])}}" class="favourite btn btn-success ">Схвалити</a>
                  <a href="{{route('request_refuse',['user_id'=>$r->id, 'exh_id'=>$req->id])}}" class="favourite btn btn-danger ">Відмовити</a>
                  
                </div>
                </div>
                @endif
                @endforeach
                @endforeach
                </div>
                <div class="row justify-content-center" style="margin-top: 20px;">
                {{ $requests->links() }}
                  
                </div>

             
@endsection
