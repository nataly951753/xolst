@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Редагування інформації за картиною</h1>                
  
                <div class="row mt-2 p-4" >
                  <div class="col-12 picture-exh" ><img style="width: 100%" src="{{asset('storage/pictures/'. $picture->url)}}"></div>
                   </div>
                <div class="row mt-2 p-4" >

                <form  method="post" class="col-12"  action="{{ route('picture_save',['id'=>$id, 'p_id'=>$picture->id]) }}">
                    @csrf                    
                  <div class="form-group">
                    <label for="name">Назва</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Название картины" value="{{$picture->name}}">
                  </div>       
                  <div class="form-group">
                    <label for="description">Опис</label>
                    <textarea class="form-control" id="description" name="description" rows="4">{{$picture->description}}</textarea>
                  </div>
                  <button type="submit" class="btn btn-outline-primary btn-change-account">Зберігти</button>
                </form>
              </div>
              
@endsection
