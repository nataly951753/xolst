@extends('layouts.account')
@section('part-content')

<h1 class="text-center all-title">Обрані виставки</h1>


<div class="row justify-content-around fav-exh " >
@foreach($my_exhibitions as $exh)

@include('fragments.exh-card')

@endforeach
</div>
<div class="row justify-content-center mt-4">
{{ $my_exhibitions->links() }}
  
</div>

@endsection
