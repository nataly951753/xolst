@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Створення виставки</h1>                
    
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form  method="post" enctype="multipart/form-data">
                    @csrf
                  <div class="form-group">
                    <label for="name">Назва</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Назва виставки" value="{{ old('name') }}">
                  </div>
                  <input type="file" id="photo" name="photo" accept="image/jpeg,image/png" class="inputfile"  value="{{ old('photo') }}">
                  <div class="form-group">
                    <label for="type_exhibition">Вид</label>
                    <select class="form-control" id="type_exhibition" name="type_exhibition">
                        @foreach($types as $type)
                            @if($type['name']==old('type_exhibition'))
                            <option selected="true">{{ $type['name'] }}</option>
                            @else
                            <option>{{ $type['name'] }}</option>
                            @endif
                        @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="style_exhibition">Стиль</label>
                    <select class="form-control" id="style_exhibition" name="style_exhibition">
                        @foreach($styles as $style)
                        @if($style['name']==old('style_exhibition'))
                        <option selected="true">{{ $style['name'] }}</option>
                        @else
                        <option>{{ $style['name'] }}</option>
                        @endif
                        @endforeach

                    </select>
                  </div> 
                  <div class="form-group">
                  <div class="form-row">
                    <div class="col">
                    <label for="date_started">Дата відкриття</label>
                      <input type="date" class="form-control" id="date_started" name="date_started" placeholder="Дата відкриття" value="{{ old('date_started') }}">
                    </div>
                    <div class="col">
                    <label for="date_finished">Дата закриття</label>                        
                      <input type="date" class="form-control" id="date_finished" name="date_finished" placeholder="Дата закриття" value="{{ old('date_finished') }}">
                    </div>
                  </div>  
                  </div>               
                  
                  <div class="form-group">
                    <label for="about">Коротка інформація про виставку</label>
                    <textarea class="form-control" id="about" name="about" rows="6" maxlength="{{config('variables.size_user_about')}}">{{ old('about') }}</textarea>
                  </div>

                  <div class="form-group">
                    <label for="tags">Теги</label>
                    <input type="text" class="form-control" id="tags" name="tags" placeholder="Вкажіть теги через кому" value="{{ old('tags') }}">
                  </div>

                  <div class="form-group">
                    <label for="page_description">Сторінка про виставку</label>
                    <textarea class="form-control" id="page_description" name="page_description" rows="20">{{ old('page_description') }}</textarea>
                  </div>             
                  <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="private" name="private" checked="{{ old('private') }}">
                    <label class="form-check-label" for="private">Приватна</label>
                  </div>
                  <div class="row justify-content-center my-4">
                  <button type="submit" class="btn btn-outline-primary btn-change-account">Створити виставку</button>
                  </div>

                </form>
  <script>
  tinymce.init({
    selector: '#page_description',
    plugins: 'searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern emoticons',
    toolbar: ' bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | emoticons',
    image_advtab: true,
    image_caption: true
  });
</script>
@endsection
