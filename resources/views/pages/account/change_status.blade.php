@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Запит на зміну ролі</h1>                
    
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form  method="post">
                    @csrf
                    <div class="form-group">
                    <label for="role">Нова роль</label>
                    <select class="form-control" id="role" name="role">
                            @if("author"==old('role'))
                            <option selected="true" value="author" >Автор</option>
                            <option value="reviewer">Критик</option>
                            @else
                            <option value="author">Автор</option>
                            <option selected="true" value="reviewer" >Критик</option>
                            @endif
                    </select>
                  </div>
                
                  <div class="form-group">
                    <label for="message">Заявка</label>
                    <textarea class="form-control" id="message" name="message" rows="20">{{old('message') }}</textarea>
                  </div>             
                
                  <div class="row justify-content-center my-4">
                  <button type="submit" class="btn btn-outline-primary btn-change-account">
                  Відправити заявку
                  </button>
                  </div>

                </form>
  <script>
  tinymce.init({
    selector: '#message',
    plugins: 'searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern emoticons',
    toolbar: ' bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | emoticons',
    image_advtab: true,
    image_caption: true
  });
</script>
@endsection
