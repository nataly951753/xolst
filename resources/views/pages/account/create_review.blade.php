@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Створення рецензії</h1>                
    
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form  method="post">
                    @csrf
                  <div class="form-group">
                    <label for="name">Назва</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Назва рецензії" value="{{ (isset($review)) ? $review->name : old('name') }}">
                  </div>
            
                  
                  <div class="form-group">
                    <label for="about">Коротка інформація</label>
                    <textarea class="form-control" id="about" name="about" rows="6" maxlength="{{config('variables.size_reviews_about')}}">{{ (isset($review)) ? $review->about : old('about') }}</textarea>
                  </div>

                  <div class="form-group">
                    <label for="page_description">Сторінка рецензії</label>
                    <textarea class="form-control" id="page_description" name="page_description" rows="20">{{ (isset($review)) ? $review->description : old('page_description') }}</textarea>
                  </div>             
                
                  <div class="row justify-content-center my-4">
                  <button type="submit" class="btn btn-outline-primary btn-change-account">
                  @if(isset($review))
                  Зберігти зміни
                  @else
                  Створити рецензію
                  @endif
                  </button>
                  </div>

                </form>
  <script>
  tinymce.init({
    selector: '#page_description',
    plugins: 'searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern emoticons',
    toolbar: ' bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat | emoticons',
    image_advtab: true,
    image_caption: true
  });
</script>
@endsection
