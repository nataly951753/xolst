@extends('layouts.account')
@section('part-content')
    <h1 class="text-center all-title">Редагування інформації за картинами</h1>                
   
      <form  method="post"   action="{{ route('pictures_delete',['id'=>$id]) }}">
        <div class="d-flex justify-content-end">
        <button type="submit" class="btn btn-danger">Видалити вибрані картини</button>
      </div>


      @foreach($pictures as $picture)
      <div class="row p-4 mt-2 ml-2" style="border-bottom: 3px dotted rgba(0, 0, 0, 0.125)">
         <input type="checkbox" class="form-check-input" id="exampleCheck1" name="{{$picture->id}}">
        <div class="col-2 picture-exh" style="background-image: url({{asset('storage/pictures/'.$picture->url)}});"></div>
         
          @csrf                    
          <div class="col-7">
          <h6 >{{$picture->name}}</h6>
          <p class="text-secondary">{!! str_limit($picture->description, 100) !!}</p>
  
          </div>
          <div>
          <a href="{{ route('picture_edit',['id'=>$id, 'p_id'=>$picture->id]) }}" class="btn btn-outline-primary bg-light " role="button" aria-pressed="true">Редагувати</a>
          </div>

        </div>
      @endforeach
      </form>
@endsection
