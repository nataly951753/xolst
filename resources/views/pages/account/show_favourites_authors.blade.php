@extends('layouts.account')
@section('title')
Особистий кабінет - Обрані автори
@endsection
@section('part-content')

  <h1 class="text-center all-title">Обрані автори</h1>
  <div class="row justify-content-around card-columns">

 @foreach($authors as $a)
 {{-- <div class=" col-3" > --}}
  @include('fragments.author')
{{-- </div> --}}
 @endforeach
</div>

  <div class="row justify-content-center mt-4">
  {{ $authors->links() }}                  
  </div>
@endsection
