<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$name}}</title>
    <link rel="shortcut icon" href="{{asset('images/xolst.ico')}}" type="image/x-icon">

    <script src="{{ mix('js/app.js') }}"></script>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{asset('css/room.css')}}">
    
    <link href="https://fonts.googleapis.com/css?family=Cormorant|Playball" rel="stylesheet">
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/three.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/basicSettings.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/textureSettings.js')}}"></script>   
    <script type="text/javascript" src="{{asset('js/room_show.js')}}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/renderers/Projector.js')}}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/controls/FirstPersonControls.js')}}"></script>
        <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/controls/TrackballControls.js')}}"></script>
    <script src="{{asset('js/libs/three/loaders/TDSLoader.js')}}"></script>
    <script src="{{asset('js/libs/three/loaders/OBJLoader.js')}}"></script>


    <script src="{{asset('js/preloader.js')}}"></script>
</head>

<body data-page="{{ Route::currentRouteName() }}">
    @include('fragments.loader')
    <div id="ai-exhibitor-dialog">
        <div id="ai-exhibitor-response"></div>
        <input type="text" id="ai-exhibitor-answer" placeholder="Ваша відповідь...">
    </div>
    <div id="ai-exhibitor-icon"></div>

    <div id="blocker">
            <div id="instructions">
                <h1 style="font-size:40px">Пересувайтесь, використовуючи мишу</h1>                
             <div style="font-size:25px">    Клацніть на картину, щоб дізнатися її історію </div>
             <div style="font-size:25px">    Для виходу натисніть на двері </div>
            </div>
    </div>
    @foreach($pictures as $picture)
    <div id="picture" class="{{$picture->url}}">
        <h2> {{$picture->name }} </h2>
        <img src="{{asset('storage/pictures/'.$picture->url)}}">
        <h3> {{ $picture->description }}</h3>
        
    </div>

    @endforeach

    <div id="webgl-output"></div>





    {{-- <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs/dist/tf.min.js"></script> --}}
    {{-- <script src="https://unpkg.com/ml5@latest/dist/ml5.min.js"></script> --}}
   {{-- <script src="https://cdn.jsdelivr.net/npm/@tensorflow-models/universal-sentence-encoder"></script> --}}
   
    {{-- <script src="{{asset('ai_exhibitor/generate_pictures_order.js')}}"></script> --}}
{{-- <script type="module">
        import {FastText, addOnPostRun} from "{{asset('js/fasttext/fasttext.js')}}";

        addOnPostRun(() => {
            let ft = new FastText();
            console.log(ft);
            console.log({!!$picturesJson!!});
            const url = "{{asset('js/fasttext/fasttext_model_uk.ftz')}}";
    ft.loadModel(url).then(model => {
        
        console.log("Model loaded.")

      // console.log(model.getSentenceVector("Найвідоміша картина сучасності"));
    });
        });
  

    </script> --}}
    <script src="{{asset('js/ai_exhibitor.js')}}"></script>
    <script src="{{asset('js/ai_exhibitor_icon.js')}}"></script>

    <script type="text/javascript">
        var xost="{{config('app.url')}}";
        @if($scene)
        var code={!!$scene!!};
        @else
        var code=undefined;
        @endif

        activateExhibitor({{$ex_id}}, {{$user_id}});
     (function () { 
        init({!!$textures!!}, {{$ex_id}}, {{$user_id}});
     })();
      </script>      
</body>
</html>