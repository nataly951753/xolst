<!DOCTYPE html>

<html>

<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{$name}}</title>
    <link rel="shortcut icon" href="{{asset('images/xolst.ico')}}" type="image/x-icon">
    
    <script src="{{ mix('js/app.js') }}"></script>
    <link href="https://fonts.googleapis.com/css?family=Cormorant|Playball" rel="stylesheet">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="stylesheet" href="{{asset('css/room.css')}}">
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/three.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/libs/utils/dat.gui.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/basicSettings.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/textureSettings.js')}}"></script>
    
    <script type="text/javascript" src="{{asset('js/room.js')}}"></script>
    


<script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/renderers/Projector.js')}}"></script>
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/controls/FirstPersonControls.js')}}"></script>
    <script src="{{asset('js/libs/three/controls/TransformControls.js')}}"></script>
    <script src="{{asset('js/libs/three/loaders/MTLLoader.js')}}"></script>
    <script src="{{asset('js/libs/three/loaders/TDSLoader.js')}}"></script>
    <script src="{{asset('js/libs/three/loaders/OBJLoader.js')}}"></script>


    <script src="{{asset('js/preloader.js')}}"></script>
    <style type="text/css">
        input{
            line-height: normal;
        }
    </style>
</head>

<body data-page="{{ Route::currentRouteName() }}">
    @include('fragments.loader')

    <div id="blocker">

            <div id="instructions">
                <span style="font-size:40px">Пересувайтесь, використовуючи мишу</span>
                <div style="font-size:25px">    Скористайтеся елементами управління в правому верхньому куті екрану</div>
                <div style="font-size:25px">    Клацніть на картину або лампу лівою кнопкою миші, щоб перемістити її </div>
                <div style="font-size:25px">    Клацніть на картину правою кнопкою миші, щоб змінити розмір</div>
             <div style="font-size:25px">    Для збереження змін і виходу натисніть на двері </div>
                <br />
                
            </div>

    </div>
    <div id="webgl-output"></div>

    <script type="text/javascript">
        var xost="{{config('app.url')}}";
        
        var route='{{$route}}';
        @if($scene)
        var code={!!$scene!!};
        @else
        var code=undefined;
        @endif

     (function () { 
        init({!!$textures!!});
     })();
      </script>
</body>
</html>