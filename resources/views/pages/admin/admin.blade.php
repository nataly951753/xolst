@extends('layouts.app')
@section('title')
Панель адміністратора
@endsection
@section('content')
 <div class="row justify-content-around" >
 <div class="col-10 col-md-4 col-lg-4 mt-3 mb-3 justify-content-center  admin-action">
         <a href="{{ route('admins')}}" class="text-center p-4 m-4">
             <span class="admin-icon"><i class="fas fa-user-shield"></i><br></span>Адміністратори  
         </a>
    </div>
            <div class="col-10 col-md-4 col-lg-4 mt-3 mb-3 justify-content-center  admin-action">
         <a href="{{ route('role_requests')}}" class="text-center p-4 m-4">
             <span class="admin-icon"><i class="fas fa-user-secret"></i><br></span>Заявки  <span class="badge badge-light pb-3 px-3 ml-4" style="position: relative;top:-6px;">{{$reqs}}</span>
         </a>
    </div>
       <div class="col-10 col-md-4 col-lg-4 mt-3 mb-3 justify-content-center  admin-action">
         <a href="{{ route('report_new_exh')}}" class="text-center p-4 m-4">
         <span class="admin-icon"><i class="fas fa-palette"></i> <br></span>  Звіт за новими виставками
         </a>
    </div>
            <div class="col-10 col-md-4 col-lg-4 mt-3 mb-3 justify-content-center  admin-action">
         <a href="{{ route('report_new_users')}}" class="text-center p-4 m-4">
         <span class="admin-icon"><i class="fas fa-address-card"></i> <br></span>    Звіт за  користувачами
         </a>
    </div>
            <div class="col-10 col-md-4 col-lg-4 mt-3 mb-3 justify-content-center  admin-action">
         <a href="{{ route('report_new_rev')}}" class="text-center p-4 m-4">
            <span class="admin-icon"><i class="fas fa-pencil-alt"></i> <br></span>
            Звіт за новими рецензіями
         </a>
    </div>
     <div class="col-10 col-md-4 col-lg-4 mt-3 mb-3 justify-content-center  admin-action">
         <a href="{{ route('logout') }}" class="text-center p-4 m-4" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            <span class="admin-icon"><i class="fas fa-sign-out-alt"></i> <br></span>
            Вихід
         </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
    </form>
    </div>
 </div>
@endsection


