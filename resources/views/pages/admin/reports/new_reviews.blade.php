@extends('layouts.app')
@section('title')
Звіт за новими рецензіями
@endsection
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-9 bg-white">
         <div class="row justify-content-end pr-4 pt-4" >
                        <a class ="print-doc hidden-print p-2" href="javascript:(print());"> Роздрукувати</a>
                    </div>
        <h1 class="text-center all-title my-4">Звіт за рецензіями, створеними у {{$year}} році</h1>


 <div class="row justify-content-center" >
  <div class="col-12 col-md-7">
 <canvas id="myChart" width="400" height="200"></canvas>
  </div>
 </div>
  <div class="row justify-content-center" >
  <div class="col-12 col-md-10 my-4">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Назва рецензії</th>
      <th scope="col">Назва виставки</th>
      <th scope="col">Автор</th>
      <th scope="col">Дата створення</th>
    </tr>
  </thead>
  <tbody>
         @foreach($revs as $rev)
    <tr>
      <th scope="row">{{$rev->name}}</th>
      <td>{{$rev->exhibition->name}}</td>
      <td>{{$rev->author->name}}</td>
      <td>{{$rev->created_at}}</td>
    </tr>
        @endforeach
   
  </tbody>
</table>
  </div>
 </div>
    </div>
 
</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: {!! json_encode($labels) !!},
        datasets: [{
            label: 'Рецензії, що були створені',
            data: {!! json_encode($data) !!},
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderWidth: 3
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
@endsection