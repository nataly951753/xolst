@extends('layouts.app')
@section('title')
Звіт за новими користувачами
@endsection
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-9 bg-white">
         <div class="row justify-content-end pr-4 pt-4" >
                        <a class ="print-doc hidden-print p-2" href="javascript:(print());"> Роздрукувати</a>
                    </div>
        <h1 class="text-center all-title my-4">Звіт за користувачами, зареєстрованими у {{$year}} році</h1>


 <div class="row justify-content-center" >
  <div class="col-12 col-md-7">
 <canvas id="myChart" width="400" height="200"></canvas>
  </div>
 </div>
  <div class="row justify-content-center" >
  <div class="col-12 col-md-10 my-4">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">Номер</th>
      <th scope="col">Ім'я</th>
      <th scope="col">Роль</th>
      <th scope="col">Дата реєстрації</th>
    </tr>
  </thead>
  <tbody>
         @foreach($allUsers as $user)
    <tr>
      <th scope="row">{{$user->id}}</th>
      <td>{{$user->name}}</td>
      <td>{{$user->role->name}}</td>
      <td>{{$user->created_at}}</td>
    </tr>
        @endforeach
   
  </tbody>
</table>
  </div>
 </div>
    </div>
 
</div>

<script>
var ctx = document.getElementById('myChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data:  {
        datasets: [{
            label: 'Адміністратори',
            data: {!! json_encode($dataAdmin) !!},
            borderColor: 
                'rgba(54, 162, 235, 1)',
                backgroundColor: 
                'rgba(54, 162, 235, 0.2)',
        }, {
            label: 'Критики',
            data: {!! json_encode($dataRev) !!},
            borderColor: 
                'rgba(255, 99, 132, 1)',
                backgroundColor: 
                'rgba(255, 99, 132, 0.2)',

        }, {
            label: 'Автори',
            data: {!! json_encode($dataAuthors) !!},
            borderColor: 
                'rgba(153, 102, 255, 1)',

                backgroundColor: 
                'rgba(153, 102, 255, 0.2)',

        }, {
            label: 'Користувачі',
            data: {!! json_encode($dataUsers) !!},
            borderColor: 
                'rgba(75, 192, 192, 1)',
                backgroundColor: 
                'rgba(75, 192, 192, 0.2)',
        }],
        labels: {!! json_encode($labels) !!}
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
</script>
@endsection