@extends('layouts.app')
@section('title')
Заявки на зміну ролі
@endsection
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-9 bg-white">
        <h1 class="text-center all-title my-4">Заявки користувачів на зміну ролі</h1>

    <div class="row justify-content-around" >
      <div class="col-12 col-md-10">
      <table class="table">
  <thead>
    <tr>
      <th scope="col">Номер</th>
      <th scope="col">Користувач</th>
      <th scope="col">Бажана роль</th>
      <th scope="col">Дія</th>
    </tr>
  </thead>
  <tbody>
               @foreach($roles as $role)
               @foreach($role->users_requests as $req)
<tr>
      <th scope="row">{{$req->pivot->id}}</th>
      <td class="requests-change-status-user"><a  href="{{route('one_author',['id'=>$req->id])}}">{{$req->name}}</a></td>
      <td>{{$role->name}}</td>
      <td><a href="{{route('role_request',['user_id'=>$req->id])}}" class="favourite btn btn-info ">Розглянути</a></td>
    </tr>

                @endforeach
                @endforeach
                 </tbody>
</table>
                </div>

</div>

</div>
</div>
@endsection