@extends('layouts.app')
@section('title')
Адміністратори системи
@endsection
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-9 bg-white">
        <h1 class="text-center all-title my-4">Адміністратори</h1>
<form method="POST" action="{{ route('create_admin') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right lb-main">{{ __('Ім\'я') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right lb-main">{{ __('E-Mail ') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right lb-main">{{ __('Пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right lb-main">{{ __('Підтвердіть пароль') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Створити адміністратора') }}
                                </button>
                            </div>
                        </div>
                    </form>
    <div class="row justify-content-around mt-3" >
      <div class="col-12 col-md-10">
      <table class="table">
  <thead>
    <tr>
      <th scope="col">Номер</th>
      <th scope="col">Ім'я</th>
      <th scope="col">Пошта</th>
      <th scope="col">Дата реєстрації</th>
    </tr>
  </thead>
  <tbody>
               @foreach($admins as $ad)
<tr>
      <th scope="row">{{$ad->id}}</th>
      <td>{{$ad->name}}</a></td>
      <td>{{$ad->email}}</td>
      <td>{{$ad->created_at}}</td>
    </tr>

                @endforeach
                 </tbody>
</table>
                </div>

</div>
  <div class="row justify-content-center mt-4" >
                    {{ $admins->links() }}  
                </div>
</div>
</div>
@endsection