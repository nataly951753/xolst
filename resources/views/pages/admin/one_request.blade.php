@extends('layouts.app')
@section('title')
Заявкa на зміну ролі
@endsection
@section('content')
<div class="row justify-content-center">
    <div class="col-12 col-md-9 bg-white">
        <h1 class="text-center all-title my-4">Заявкa користувачa {{$user->name}} на отримання ролі "
        	@if("author"==$role->name)
             Автор
            @else
            Критик
            @endif
        "</h1>

    <div class="row justify-content-around" >
      <div class="col-12 col-md-10">
     {!!$role->pivot->message!!}
  </div>
</div>
 <div class="row justify-content-around my-4" >
      <a href="{{route('role_request_approve',['user_id'=>$user->id, 'role_id'=>$role->id])}}" class="favourite btn btn-success">Задовільнити</a>
                  <a href="{{route('role_request_refuse',['user_id'=>$user->id, 'role_id'=>$role->id])}}" class="favourite btn btn-danger">Відмовити</a>
  
</div>

</div>
</div>
@endsection