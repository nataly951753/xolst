@extends('layouts.app')
@section('title', "Xolst - $author->name")
@section('content')
<div class="container mb-4">
  <div role="alert" aria-live="assertive" aria-atomic="true" class="toastsuc  " data-autohide="false" >
            <div class="toast-header text-light bg-success">
              <strong class="mr-auto">Додано</strong>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close"  onclick="$('.toastsuc').hide(1000)">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="toast-body text-light " >
              Автор доданий до обраного
            </div>
          </div>
      <div role="alert" aria-live="assertive" aria-atomic="true" class="toastdel  " data-autohide="false" >
            <div class="toast-header text-light bg-success">
              <strong class="mr-auto">Видалено</strong>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close" onclick="$('.toastdel').hide(1000)">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="toast-body text-light" >
              Автор видален з обраного
            </div>
          </div>
    <div class="row justify-content-center content-bg pb-5">

        <div class="col-12 col-lg-10">
                <div class="row flex-column justify-content-between align-items-center my-4 mx-3 ">
                <h1 class="text-center all-title mt-2">{{$author->name}}</h1>

                  @if($sub)
                  <a href="" class="favourite btn " >Відписатись <i class="fa fas fa-crown"></i><span class="icon-text">{{$subscribers}}</span></a>
                  @else
                  <a href="" class="favourite btn " >Підписатись <i class="fa fas fa-crown"></i><span class="icon-text">{{$subscribers}}</span></a>
                  @endif
                </div>  
                   
                    
                  <p class="card-text">{!! $author->information !!}</p>               
                  @if($author->exhibitions->count())
                 <h2 class="author-exh text-center my-4 pb-2" >Виставки автора</h2>
 
                <div class="exh-slider exh-theme owl-carousel  row" >
                  
                @foreach($author->exhibitions as $exh)
                @include('fragments.exh-card')
                @endforeach
                </div>
                @endif
                @if($author->reviews->count())
                 <h2 class="author-exh text-center my-4 pb-2" >Рецензії автора</h2>
 
                <div class="reviews-slider exh-theme owl-carousel row slider-reviews" >
                  
                @foreach($author->reviews as $review)
                @include('fragments.review-card')
                @endforeach
                </div>
                @endif

              </div>
            </div>
        
    </div>
<script type="text/javascript">
    var sub={{$subscribers}};
      $(".favourite").on('click', function (e) {
                e.preventDefault();
      @guest
        document.location.href="{{route('login')}}";      
      @else
        $.ajax({
          headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
            method: 'post',
            data: {
                id: {{ $author->id }}
            },
              url: '/favourite_author',
              success(data) {
                console.log(data);
          if($(".favourite").text()=="Підписатись "+sub){
          $('.toastsuc').show(500);
          sub++;                    
          $(".favourite").html("Відписатись <i class='fa fas fa-crown'></i><span class='icon-text'>"+ sub +"</span>");
          }
            else if($(".favourite").text()=="Відписатись "+sub){
            $('.toastdel').show(500);
            sub--;
            $(".favourite").html("Підписатись <i class='fa fas fa-crown'></i><span class='icon-text'>"+sub+"</span>");
            }
              },
            });                    
      @endguest
      });
    </script>
@endsection
