@extends('layouts.app')
@section('title', "Xolst - $exh->name ")
@section('content')
<div class="container mb-4">
  <div role="alert" aria-live="assertive" aria-atomic="true" class="toastsuc  " data-autohide="false" >
            <div class="toast-header text-light bg-success">
              <strong class="mr-auto">Додано</strong>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close"  onclick="$('.toastsuc').hide(1000)">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="toast-body text-light " >
              Виставка додана до обраного
            </div>
          </div>
      <div role="alert" aria-live="assertive" aria-atomic="true" class="toastdel  " data-autohide="false" >
            <div class="toast-header text-light bg-success">
              <strong class="mr-auto">Видалено</strong>
              <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close" onclick="$('.toastdel').hide(1000)">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="toast-body text-light" >
              Виставка видалена з обраного
            </div>
          </div>
        <div class="row justify-content-center content-bg">

        <div class="col-12 col-lg-10">
               <h1 class="text-center all-title mt-2">
                  @if($exh->private)
                    <span class="badge badge-dark badge-private" >Приватна</span>
                  @endif
                    {{$exh->name}}</h1>
                    
                  @if($date->between($exh->started_at, $exh->finished_at))
                  <div class="row justify-content-center exh-date py-2" style="color: #4caf50">
                  @else
                  <div class="row justify-content-center exh-date py-2">
                  @endif
                    з  {{$exh->started_at->format('d-m-Y')}}       до  {{$exh->finished_at->format('d-m-Y')}}
                  </div>
                  
                    
                    <h6 class="text-center one-exh-author mb-3" ><a href="{{route('one_author',['id'=>$exh->author->id])}}">{{$exh->author->name}}</a></h6>
                  
                  <div class="exh-type mb-2 text-center mb-4">{{$exh->type->name}} - {{$exh->style->name}}</div>

                  <div>{!! $exh->description !!}</div>
                  @if($exh->tags->count())
                  <div class="estimate mt-2">Теги</div>
                  @foreach($exh->tags as $tag)
                  <span class="badge badge-primary exh-tag mr-2"> # {{$tag->name}}</span>
                  @endforeach
                  @endif


                  <div class="text-right my-3 exh-icons">
                       @include('fragments.exh-icons')                  
                </div>
                  <div class="row justify-content-around">
                  @if($exh->private)
                      @if($request===null)
                           <a href="{{route('delete_request_private_exhibition', ['id'=>$exh->id])}}" class="btn btn-outline-primary favourite">Скасувати запит на відвідування</a>  
                      @elseif($request==1)
                            <div class="btn btn-outline-success  ">Заявка схвалена</div>
                            @if($date->between($exh->started_at, $exh->finished_at))
                            <a href="{{ route('room_show', ['id'=>$exh->id])}}" class="btn btn-outline-primary favourite">Відвідати</a>   
                            @endif
                      @elseif($request==0)
                            <div class="btn btn-outline-danger  ">Заявка відхилена</div>
                      @else
                       <a href="{{route('add_request_private_exhibition', ['id'=>$exh->id])}}" class="btn btn-outline-primary favourite ">Відправити запит на відвідування</a>  
                       @endif
                  @else
                   @if($date->between($exh->started_at, $exh->finished_at))
                  <a href="{{ route('room_show', ['id'=>$exh->id])}}" class="btn btn-outline-primary favourite">Відвідати</a>   
                    @endif 
                    @endif 
                    @if($favourite)
                  <a href="" class="btn btn-outline-primary favourite exhfav">В обраному</a>  
                    @else
                  <a href="" class="btn btn-outline-primary favourite exhfav">До обраного</a>  
                    @endif
                    @if((Auth::check() && Auth::user()->role->name=='reviewer') || !Auth::check())
                  <a href="{{ route('create_review', ['id'=>$exh->id])}}" class="btn btn-outline-primary favourite exh_new_review">Додати рецензію</a>  
                    @endif
                    
                  </div>
                  <div class="row justify-content-between mt-4">
                    <div>

                  <div class="estimate">Оцініть</div>
                  <div id="reviewStars-input">
                    <input class="rating" id="star-4" type="radio" name="reviewStars"/>                  
                    @if($rating==5)
                    <label title="Чудово!" for="star-4" style="background-position: 0 -40px;"></label>
                    @else
                    <label title="Чудово!" for="star-4"></label>
                    @endif                                        
                    <input class="rating" id="star-3" type="radio" name="reviewStars"/>
                    @if($rating>=4)
                    <label title="Добре!" for="star-3" style="background-position: 0 -40px;"></label>
                    @else
                    <label title="Добре!" for="star-3"></label>
                    @endif  
                    <input class="rating" id="star-2" type="radio" name="reviewStars"/>
                    @if($rating>=3)
                    <label title="Нормально" for="star-2" style="background-position: 0 -40px;"></label>
                    @else
                    <label title="Нормально" for="star-2"></label>
                    @endif  
                    <input class="rating" id="star-1" type="radio" name="reviewStars"/>
                    @if($rating>=2)
                    <label title="Не дуже" for="star-1" style="background-position: 0 -40px;"></label>
                    @else
                    <label title="Не дуже" for="star-1"></label>
                    @endif  
                    <input class="rating" id="star-0" type="radio" name="reviewStars"/>
                    @if($rating>=1)
                    <label title="Не сподобалось" for="star-0" style="background-position: 0 -40px;"></label>
                    @else
                    <label title="Не сподобалось" for="star-0"></label>
                    @endif  
                  </div>
                  
                </div>

                </div>
                @if($exh->reviews->count())
                 <h2 class="author-exh text-center my-4 pb-2" >Рецензії до виставки</h2>
 
                <div class="reviews-slider exh-theme owl-carousel row slider-reviews">
                  
                @foreach($exh->reviews as $review)
                @include('fragments.review-card')
                @endforeach
                </div>
                @endif
              </div>
            </div>
        
    </div>
    <script type="text/javascript">

      $(".exhfav").on('click', function (e) {
                e.preventDefault();
      @guest
        document.location.href="{{route('login')}}";
      
      @else


                    $.ajax({
                      headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                        method: 'post',
                        data: {
                            id: {{ $exh->id }}
                        },
                          url: '/favourite_exhibition',
                          success(data) {
                            console.log(data);
                            if($(".exhfav").text()=="До обраного"){
                            $('.toastsuc').show(500);
                            $(".exhfav").text("В обраному");
                            $(".fa-crown").text(Number($(".fa-crown").text())+1);
                            }
                            else if($(".exhfav").text()=="В обраному"){
                            $('.toastdel').show(500);
                            $(".exhfav").text("До обраного");
                            $(".fa-crown").text(Number($(".fa-crown").text())-1);
                            }
                            
                          },
                        });
                    
      @endguest

      });
      //ratings
       $(".rating").on('click', function (e) {
      @guest
        document.location.href="{{route('login')}}";
      
      @else
      $("label").removeAttr("style");
                    $.ajax({
                      headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
                        method: 'post',
                        data: {
                            id: {{ $exh->id }},
                            rating: (e.target.id).substr(-1)
                        },
                          url: '/rating_exhibition',
                          success(data) {
                            console.log('success');
                            $(".fa-star").text(data);
                          },
                        });
               
      @endguest

      });

    </script>

@endsection
