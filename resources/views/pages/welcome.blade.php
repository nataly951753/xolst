@extends('layouts.app')
@section('title', 'Xolst')

@section('content')
@include('fragments.loader')
<script src="{{asset('js/preloader.js')}}"></script>


<section class="first">
<div class="container">

	<div class="row">
	<div class="col-sm-12 col-md-12 col-lg-4 col-xl-4" style="z-index: 1">
		<div class="inspire row flex-column align-items-center align-items-sm-center align-items-md-center align-items-lg-end align-items-xl-end justify-content-end">
		<span>краса</span>		
		<span>в кожній миті...</span>		
		</div>

	<div class="row justify-content-center justify-content-sm-center justify-content-md-center">
	<h1  class="site-name">Xolst</h1>
		
	</div>
	</div>
	<div class="d-none d-md-block col-md-12 col-lg-8 col-xl-8 align-self-end">
	<img class="picture3" src="{{asset('images/picture4.jpg')}}">
	<img class="picture2" src="{{asset('images/picture2.jpg')}}">
	<img class="picture4" src="{{asset('images/picture3.jpg')}}">
	<img class="picture1" src="{{asset('images/picture1.jpg')}}">		
	</div>
	</div>
	
</div>
</section>
<section class="second" style="background-image: url({{asset('images/exh6.png')}})" >
<div class="row" style="height: 100vh; overflow: hidden;">
	<div class="col-sm-12 col-md-12 col-lg-5 col-xl-5 second-text text-white">
<h2 class="second-text-title mt-4">Майданчик для проведення</h2>
<h2 class="second-text-title text-right mb-4">художніх виставок</h2>
<ul class="second-text-list mt-4">
	<li>Відвідуйте 3D-виставкові зали з індивідуальним авторським дизайном.</li>
	<li>Діліться своєю думкою о виставках, залишаючи розгорнуті рецензії, читайте рецензії інших відвідувачів і авторів.</li>
	<li>Знайомтеся з історіями авторів, що описують свої роботи, а також з особистими сторінками авторів.</li>
	<li>Відкривайте свої виставки. У виставкових залах Ви зможете налаштувати розміри приміщення, матеріали поверхонь, розміщення робіт і освітлення.</li>
	<li>Створюйте приватні виставки, контролюючи можливість відвідування.</li>
	<li>Відстежуйте успіх своєї виставки по рейтингу, кількості відвідувань і додаванню до Обраного.</li>
</ul>
	</div>
</div>
</section>
<section class="third ">
	<h2 class="third-title my-4 pt-4 pb-2">Гра з кольором, світлом і композицією </h2>
<div class="row justify-content-between align-items-center " style=" flex-wrap: wrap; padding-top: 4%;" >
	<div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 text-center" style="padding: 0;">
<img class="third-img" src="{{asset('images/exh8.png')}}" width="100%" >
	<div class="third-text mt-4">Широкий інструментарій</div>

	</div >
	<div class="col col-sm-12 col-md-12 col-lg-4 col-xl-4 text-center" style="min-height: 280px; overflow: hidden; padding: 0; ">
<img class="third-img" src="{{asset('images/exh7.png')}}" width="170%" >
	<div class="third-text mt-4">Огляд 360</div>

	</div>
	<div class="col col-sm-12 col-md-12 col-lg-4 col-xl-4 text-center" style="padding: 0;"   >
<img class="third-img" src="{{asset('images/exh9.png')}}" width="100%" >
	<div class="third-text mt-4">Атмосферність</div>

	</div>
</div>
</section>
<section class="fourth bg-white">
	<h2 class="fourth-title my-4 pt-4 pb-2 text-right ">Найкращі автори </h2>
<div class="row justify-content-between align-items-center " style=" flex-wrap: wrap; padding-top: 4%;" >

	@foreach($authors as $a)
@include("fragments.author_for_welcome")
	@endforeach

</div>
</section>

<section class="fifth" id="fifth">
	<a href="{{route('register')}}" >
		Xolst •
	Приєднатись •
		Xolst 

	</a>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>

<script type="text/javascript" src="{{asset('js/sign_in_welcome.js')}}"></script>
<script type="text/javascript">
	anime.timeline({loop: true})
  .add({
    targets: '.fifth a',
    opacity: 0,
    duration: 2000,
    easing: "easeOutExpo",
    delay: 1000
  }).add({
    targets: '.fifth a',
    opacity: 1,
    duration: 2000,
    easing: "easeOutExpo",
    delay: 1000
  });
</script>
@endsection