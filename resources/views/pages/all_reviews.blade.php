@extends('layouts.app')
@section('title', 'Xolst - Рецензии')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-12">
                <h1 class="text-center all-title">РЕЦЕНЗІЇ</h1>
    
                
                <div class="row justify-content-around" >
                @foreach($reviews as $review)
                <div class="card col-10 col-md-5 col-lg-5 all-reviews mt-4" >
                
                   
                <div class="card-body">
                <h5 class="card-title mt-2"><a class="all-reviews-name text-uppercase" href="{{route('one_review',['id'=>$review->id])}}">{{$review->name}}</a></h5>
                <h6 class="card-title mt-2 text-right all-reviews-author-name"> ВИСТАВКА - <a href="{{route('one_exhibition',['id'=>$review->exhibition_id])}}">{{$review->exhibition->name}}</a></h6>
                <p class="card-text">{{ $review->about }}</p>
                <h6 class="card-title mt-2 text-right all-reviews-author-name"> АВТОР РЕЦЕНЗІЇ - <a href="{{route('one_author',['id'=>$review->author_id])}}">{{$review->author->name}}</a></h6>
                <p class="review-date text-right"> Опубліковано {{$review->created_at}}</p>
                </div>
                
                </div>
                @endforeach
                </div>
                <div class="row justify-content-center mt-4" >
                {{ $reviews->links() }}
                  
                </div>
    </div>
    </div>

</div>
@endsection
