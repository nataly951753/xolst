@extends('layouts.app')
@section('title', 'Xolst - Авторы ')
@section('content')
<div class="container">
    <div class="row">

        <div class="col-12">
                <h1 class="text-center all-title">АВТОРИ</h1>
    
                
                <div class="row justify-content-around" >
                @foreach($authors as $a)
                <div class="card col-10 col-md-5 col-lg-5 all-author mt-4" >
                
                   
                <div class="card-body">
                <h5 class="card-title mt-2"><a class=" all-author-name" href="{{route('one_author',['id'=>$a->id])}}">{{$a->name}}</a></h5>

                <p class="card-text">{{ $a->about }}</p>
                <i class="fas fa-crown"><span class="icon-text">{{$a->subscribers()->count()}}</span></i>                  
                <i class="fas fa-clipboard-check"><span class="icon-text">{{$a->exhibitions()->count()}}</span></i>
                </div>
                
                </div>
                @endforeach
                </div>
                <div class="row justify-content-center mt-4" >
                {{ $authors->links() }}
                  
                </div>
    </div>
    </div>

</div>
@endsection
