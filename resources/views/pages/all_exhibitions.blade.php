@extends('layouts.app')
@section('title', 'Xolst - Выставки ')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
                <h1 class="text-center all-title">ВИСТАВКИ</h1>

                <div class="row justify-content-center" >
                    @foreach($types as $type)
                <a href="{{route('all_exhibitions',['type'=>$type->name])}}" class="filter-link-exh m-3 text-uppercase">{{$type->name}}</a>
                @endforeach

                </div>

                <div class="row justify-content-center  filter-exh-block " >
                    @foreach($styles as $style)
                    <a href="{{route('all_exhibitions',['type'=>$exh_type,'style'=>$style->name])}}" class="filter-link-exh mx-3 my-1">{{$style->name}}</a>
                    @endforeach
                </div>
                <div class="row justify-content-center m-3" >
                    <i class="fas fa-chevron-down more-styles"></i>
                    <i class="fas fa-chevron-up hide-styles" style="display:  none;"></i>
                </div>

                <nav class="navbar navbar-expand-lg navbar-light bg-light mr-3 row">
                  <span class="navbar-brand">Сортирувати за:</span>
                  <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        @if($orderBy=='views')
                          <li class="nav-item active">
                        @else
                          <li class="nav-item">
                        @endif
                        <a class="nav-link" href="{{route('all_exhibitions',['type'=>$exh_type,'style'=>$exh_style,'tag'=>$tag, 'orderBy'=>'views'])}}">Відвідуваностю</a>
                      </li>
                        @if($orderBy=='favorites')
                          <li class="nav-item active">
                        @else
                          <li class="nav-item">
                        @endif
                        <a class="nav-link" href="{{route('all_exhibitions',['type'=>$exh_type,'style'=>$exh_style,'tag'=>$tag, 'orderBy'=>'favorites'])}}">Популярністю</a>
                      </li>
                        @if($orderBy=='ratings')
                          <li class="nav-item active">
                        @else
                          <li class="nav-item">
                        @endif
                        <a class="nav-link" href="{{route('all_exhibitions',['type'=>$exh_type,'style'=>$exh_style,'tag'=>$tag, 'orderBy'=>'ratings'])}}">Рейтингом</a>
                      </li>
                        @if($orderBy=='started_date')
                          <li class="nav-item active">
                        @else
                          <li class="nav-item">
                        @endif
                        <a class="nav-link" href="{{route('all_exhibitions',['type'=>$exh_type,'style'=>$exh_style,'tag'=>$tag, 'orderBy'=>'started_date'])}}">Датою відкриття</a>
                      </li>
                        @if($orderBy=='public')
                          <li class="nav-item active">
                        @else
                          <li class="nav-item">
                        @endif
                        <a class="nav-link" href="{{route('all_exhibitions',['type'=>$exh_type,'style'=>$exh_style,'tag'=>$tag, 'orderBy'=>'public'])}}">Тільки публичні</a>
                      </li>
                    </ul>
                  </div>
                </nav>


                @if(!$exhibitions->count())
                @if($tag!='all')
                <h3 class="text-center all-reviews-name mt-4">Виставок під тегом #{{$tag}} немає.</h3>
                @elseif($exh_style!='all')
                <h3 class="text-center all-reviews-name mt-4">Виставок {{$exh_style}} немає.</h3>
                @elseif($exh_type!='all')
                <h3 class="text-center all-reviews-name mt-4">Виставок {{$exh_type}} немає.</h3>
                @endif
                @endif
                
                <div class="row justify-content-around" >

                @foreach($exhibitions as $exh)
                <div class="all-exhibitions col-10 col-md-6 col-lg-6 mt-4">
                  @include('fragments.exhibition')
              </div>
                @endforeach
                </div>
                <div class="row justify-content-center mt-4" >
                    {{ $exhibitions->links() }}  
                </div>
    </div>
    </div>
</div>
<script type="text/javascript">
       $(".more-styles").on('click', function (e) {
        $(".filter-exh-block").animate({
            height: "175"
          }, 1000, function() {
            $(".hide-styles").css('display', 'inline-block');
            $(".more-styles").css('display', 'none');
            });        
        });

       $(".hide-styles").on('click', function (e) {
        $(".filter-exh-block").animate({
            height: "27"
          }, 1000, function() {
        $(".more-styles").css('display', 'inline-block');
        $(".hide-styles").css('display', 'none');
        });
      });

    </script>
@endsection
