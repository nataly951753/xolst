<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{asset('images/xolst.ico')}}" type="image/x-icon">
    <title>@yield('title')</title>
    <script type="text/javascript" charset="UTF-8" src="{{asset('js/libs/three/three.min.js')}}"></script>
    <script src="{{ mix('js/app.js') }}"></script>        

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Cormorant|Cormorant+Garamond|Pattaya|Nunito|Playball|Lobster|Lora|Playfair+Display|Open+Sans" rel="stylesheet">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">


</head>
<body data-page="{{ Route::currentRouteName() }}">

    <div id="app">

@empty($exhibitions)
@empty($admin)
        @include('fragments.menu')   
        @endempty
        @endempty    
        <main class="pt-4" >

        @isset($exhibitions)
        <div style="display: flex; justify-content: space-around;">   
        <a href="{{route('main_menu')}}" id="logo" ></a>
        <a href="{{route('search')}}" id="search" ></a>
        <script src="{{asset('js/search-icon.js')}}"></script>
        </div>
        @endisset
        @empty($exhibitions)
        @isset($admin)
        <div class="logo hidden-print">   
        <a href="{{route('admin')}}" id="logo" class="hidden-print"></a>
        </div>
        @endisset
        @endempty


            @yield('content')
            
        </main>
        @include("fragments.footer")
    </div>
    
    <script src="{{asset('js/logo.js')}}"></script>
</body>

</html>
