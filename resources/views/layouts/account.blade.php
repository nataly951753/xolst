@extends('layouts.app')
@section('title')
Особистий кабінет - {{ Auth::user()->name }}
@endsection
@section('content')
<div class="container pb-4 ">
    <div class="row">

        <div class="col-12">
            <div class="row justify-content-between">
        <div class=" d-none d-lg-block  col-lg-3 col-xl-3 account-menu pb-3">
            <div class="row justify-content-center">
            @if(Auth::user()->photo_url)
            <img class="mt-1"style="max-height: 150px; max-width: 100%" src="{{asset('storage/users/'.Auth::user()->photo_url)}}">
            @else
            <img class="mt-1"style="width: 100%;" src="{{asset('images/back_example.jpg')}}">
            @endif
            </div>
            @include('fragments.account-menu')
        </div>
        <div class="col-12  col-lg-8 col-xl-8 content-bg" style="overflow: hidden;">
                @yield('part-content')   
        </div>
        </div>
        </div>
    </div>
</div>
@endsection

