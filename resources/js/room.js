var pictures = null;


// document.addEventListener('myEvent', function () {
// 	console.log(scene);
// })

function init(picturesTextures, code) {
// console.log(code);
//   var picturesTextures=[
// "assets/textures/picture1.png",
// "assets/textures/picture2.jpg",
// "assets/textures/picture3.jpg",
// "assets/textures/picture4.jpg",
// "assets/textures/picture13.jpg",
// "assets/textures/picture6.jpg",
// "assets/textures/picture7.jpg",
// "assets/textures/picture14.jpg",
// "assets/textures/picture9.jpg",
// "assets/textures/picture10.jpg",
// "assets/textures/picture11.jpg",
// "assets/textures/picture12.jpg"
//   ];
  // var picturesTextures=[];
  // for (var i = 0; i < pics.length; i++) {
  //   picturesTextures.push({{asset('uploads/'.pics[i].url)}});
  // }
  // console.log(picturesTextures);

  var maxPictures=picturesTextures.length;
  var wallsForPicturesCount=3;
  var picturesOnTheWallCount=Math.ceil(maxPictures/wallsForPicturesCount);

  var pictureDefaultWidth=300;
  var pictureDefaultHeight=400;
  // var distanceOfPictures=(roomWidth/picturesOnTheWallCount-pictureDefaultWidth)/picturesOnTheWallCount;
  var distanceOfPictures=30;


  var roomWidth= (pictureDefaultWidth + distanceOfPictures*2)* picturesOnTheWallCount;
  // var roomWidth=540 * picturesOnTheWallCount;
  var roomHeight=600;
  var roomDepth=(pictureDefaultWidth + distanceOfPictures*2) * picturesOnTheWallCount;
  
  // var pictureDefaultWidth=roomWidth/(picturesOnTheWallCount+1);

  // console.log(pictureDefaultWidth);


  var object;






  
  // var stats = initStats();
  var renderer = initRenderer();
  var scene = new THREE.Scene();


var lamps=new THREE.Group();
scene.add(lamps);



  var camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 30000); 
    // camera.position.set(800,2500,1800);
    camera.position.set(500,roomHeight/2,200);
    camera.lookAt(0,roomHeight/2,0);




  var moveControls = initPersonControls(camera);

 


  var blocker = document.getElementById( 'blocker' );
  var instructions = document.getElementById( 'instructions' );
        instructions.addEventListener( 'click', function () {
          // console.log(111)
          document.addEventListener( 'mousemove', onDocumentMouseMove, false );
          document.addEventListener( 'mousedown', onDocumentMouseDown, false );
          document.addEventListener( 'mouseup', onDocumentMouseUp, false );
          instructions.style.display = 'none';
          blocker.style.display = 'none';
          moveControls.enabled=true;
        }, false );

        
        var onKeyDown = function ( event ) {
          switch ( event.keyCode ) {
            case 27:/*Escape*/{ 
        moveControls.enabled=false;
        blocker.style.display = 'block';
          instructions.style.display = '';
          document.removeEventListener( 'mousemove', onDocumentMouseMove, false );
          document.removeEventListener( 'mousedown', onDocumentMouseDown, false );
          document.removeEventListener( 'mouseup', onDocumentMouseUp, false );
          break;}
          }
        };
        document.addEventListener( 'keydown', onKeyDown, false );


  //получение времени для точного рендеринга
  var clock = new THREE.Clock();
  //для загрузки текстур
  var textureLoader = new THREE.TextureLoader();


  // пол
 var floorGeometry = new THREE.BoxGeometry(roomWidth,5,roomDepth);
 var floorMaterial = makeMaterials(texturesForRoom.wood1.url,1,1);
 var floor=new THREE.Mesh(floorGeometry,floorMaterial);
 floor.name="floor";
//потолок
var ceilGeometry = new THREE.BoxGeometry(roomWidth,5,roomDepth);
 var ceilMaterial = makeMaterials(texturesForRoom.wallpaper1.url,1,1);
 var ceil=new THREE.Mesh(ceilGeometry,ceilMaterial);
 ceil.position.y=roomHeight;
 ceil.name="ceil";

//материал для стен
var WallMaterial =  makeMaterials(texturesForRoom.wallpaper1.url,texturesForRoom.wallpaper1.x,texturesForRoom.wallpaper1.y);
var WallMaterial2=  makeMaterials(texturesForRoom.wallpaper1.url,texturesForRoom.wallpaper1.x,texturesForRoom.wallpaper1.y);
// стена
var rightWallGeometry = new THREE.BoxGeometry(5,roomHeight,roomDepth);
 var rightWall=new THREE.Mesh(rightWallGeometry,WallMaterial2);
 rightWall.position.x=roomWidth/2;
 rightWall.position.y=roomHeight/2;
 rightWall.name="rightWall";

// стена
var leftWallGeometry = new THREE.BoxGeometry(5,roomHeight,roomDepth);
 var leftWall=new THREE.Mesh(leftWallGeometry,WallMaterial2);
 leftWall.position.x=-roomWidth/2;
 leftWall.position.y=roomHeight/2;
 leftWall.name="leftWall";

 // стена
 var topWallGeometry = new THREE.BoxGeometry(roomWidth,roomHeight,5);
 var topWall=new THREE.Mesh(topWallGeometry,WallMaterial);
 topWall.position.z=roomDepth/2;
 topWall.position.y=roomHeight/2;
 topWall.name="topWall";

 // стена
var bottomWallGeometry = new THREE.BoxGeometry(roomWidth,roomHeight,5);
 var bottomWall=new THREE.Mesh(bottomWallGeometry,WallMaterial);
 bottomWall.position.z=-roomDepth/2;
 bottomWall.position.y=roomHeight/2;
 bottomWall.name="bottomWall";
//все картины объединены в группу
pictures=new THREE.Group();
//добавление картин
var picture=0;
picture=picturesOnWall(-roomWidth/2+distanceOfPictures*8,roomHeight/2,roomDepth/2 -5,picture,0,true);
picture=picturesOnWall(-roomWidth/2+distanceOfPictures*8,roomHeight/2,-roomDepth/2 +5,picture,0,true);
picture=picturesOnWall(-roomWidth/2+5,roomHeight/2,-roomDepth/3 +distanceOfPictures*2,picture,Math.PI/2,false);
scene.add(pictures);


//объединение потолка пола и стен в группу комнаты
var room=new THREE.Group();
room.add(ceil);
room.add(floor);
room.add(topWall);
room.add(bottomWall);
room.add(rightWall);
room.add(leftWall);
scene.add(room);

room.receiveShadow=true;
//комната как ограничение для контроллера управления перемещением
moveControls.box=room;


// var dragControls = new THREE.DragControls( pictures.children, camera, renderer.domElement );
//         dragControls.addEventListener( 'dragstart', function () {
//           // controls.enabled = false;
//         } );
//         dragControls.addEventListener( 'dragend', function () {
//           // controls.enabled = true;
//         } );

var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var isWorkingWithPicture=false;
var isWorkingWithScale=false;
var isWorkingWithLamp=false;

var pObj;
var lockPictures=10;
var lampSize;
var lampX, lampZ;
// var raycaster = new THREE.Raycaster();

//       var mouse = new THREE.Vector2();
      // var selectedObjects = [];

      // var composer, effectFXAA, outlinePass;
      // // postprocessing

      //   composer = new THREE.EffectComposer( renderer );

      //   var renderPass = new THREE.RenderPass( scene, camera );
      //   composer.addPass( renderPass );

      //   outlinePass = new THREE.OutlinePass( new THREE.Vector2( window.innerWidth, window.innerHeight ), scene, camera );
      //   composer.addPass( outlinePass );

      //   // var onLoad = function ( texture ) {

      //   //   outlinePass.patternTexture = texture;
      //   //   texture.wrapS = THREE.RepeatWrapping;
      //   //   texture.wrapT = THREE.RepeatWrapping;

      //   // };

      //   // var loader = new THREE.TextureLoader();

      //   // loader.load( 'textures/tri_pattern.jpg', onLoad );

      //   effectFXAA = new THREE.ShaderPass( THREE.FXAAShader );
      //   effectFXAA.uniforms[ 'resolution' ].value.set( 1 / window.innerWidth, 1 / window.innerHeight );
      //   effectFXAA.renderToScreen = true;
      //   composer.addPass( effectFXAA );



function onDocumentMouseMove( event ) {
        // var f=scene.getObjectByName("floor");
        mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
        scene.remove(scene.getObjectByName("cOnFloor"));
        scene.remove(scene.getObjectByName("outline"));


        // event.preventDefault();
        raycaster.setFromCamera( mouse, camera );
        //пересекает пол
        var intersects = raycaster.intersectObjects( room.children.concat(pictures.children) );
        // var p=new THREE.Vector3();
        // console.log(camera.getWorldPosition(p));
        if ( intersects.length > 0 ) {
          var intersect = intersects[ 0 ];
          if(!isWorkingWithLamp){
          if(!isWorkingWithScale){
          if(!isWorkingWithPicture){
          if(intersect.object.name=="floor")
          {
            var gCircle =  new THREE.CylinderGeometry(25, 25, 1,25,25);
            var mCircle= new THREE.MeshBasicMaterial({
                color: 16777215,
                opacity: 0.5,
                transparent: !0,
                depthTest: !1
            });
            var Circle=new THREE.Mesh(gCircle,mCircle);
            Circle.name="cOnFloor";
            Circle.position.copy( intersect.point ).add( intersect.face.normal);
            scene.add(Circle);
          }
          else if(intersect.object.name=="topPicture"||intersect.object.name=="bottomPicture"||intersect.object.name=="leftPicture"||intersect.object.name=="rightPicture"){
            // console.log(intersect.object)
            var outlineMaterial1 = new THREE.MeshBasicMaterial( { color: 0xffffff, side: THREE.BackSide } );
            var f=-30;
            if(room.scale.x<1.5&&room.scale.z<1.5)f=2;
            var outlineMesh1 = new THREE.Mesh( new THREE.BoxBufferGeometry(intersect.object.geometry.parameters.width*intersect.object.scale.x,intersect.object.geometry.parameters.height*intersect.object.scale.y,intersect.object.geometry.parameters.depth-f)
              , outlineMaterial1 );
            outlineMesh1.position.copy(intersect.object.position);
            // console.log(outlineMesh1);
            outlineMesh1.rotation.copy(intersect.object.rotation);

            outlineMesh1.scale.multiplyScalar(1.05);
            outlineMesh1.name="outline";
            scene.add( outlineMesh1 );
        //     // console.log(outlineMesh1);
        // var selectedObject = intersect.object;
        //     addSelectedObject( selectedObject );
        //     outlinePass.selectedObjects = selectedObjects;

          }
        }
        else
        {

          if(pObj.name=="topPicture" ||pObj.name=="bottomPicture"){
              console.log(pictures);

            var lock=((roomWidth/2)*room.scale.x)-lockPictures;            
            if(intersect.point.x<=lock&&intersect.point.x>=-lock)
            pObj.position.x= intersect.point.x;
            else
            {
              pObj.rotation.y=Math.PI/2;
              if(intersect.point.x<-lock){
                pObj.name="leftPicture";
                pObj.position.z= intersect.point.z+lockPictures;
              }
              else
              {
                pObj.name="rightPicture";
                pObj.position.z= intersect.point.z-lockPictures;
              }
              console.log(pictures);
              movePictures();
              console.log(pictures);
              
            }
            pObj.position.y= intersect.point.y;
              console.log(pictures);
              console.log(pObj);


          }
          if(pObj.name=="rightPicture" || pObj.name=="leftPicture")
          {
            var lock=((roomDepth/2)*room.scale.z)-lockPictures;
            if(intersect.point.z<=lock&&intersect.point.z>=-lock)
            pObj.position.z= intersect.point.z;
            else
            {
              pObj.rotation.y=Math.PI;
              if(intersect.point.z<-lock){
                pObj.name="bottomPicture";
                pObj.position.x= intersect.point.x+lockPictures;                
              }
              else
              {
                pObj.name="topPicture";
                pObj.position.x= intersect.point.x-lockPictures;
              }
              movePictures();
            }
            pObj.position.y= intersect.point.y;
          }
        }
        }
      }
      else
      {
        if(intersect.object.name=="ceil")
          {
            var lock=((roomWidth/2)*controls.width);   
            var lock2=((roomDepth/2)*controls.depth);  
              lamps.children.forEach(function(item,i,arr) {
                  if(item.name==pObj.name ){
                    if(item.type=="PointLight")
                    {
                    if(intersect.point.x<=lock-50&&intersect.point.x>=-lock+50)
                      item.position.x= intersect.point.x;
                    if(intersect.point.z<=lock2-50&&intersect.point.z>=-lock2+50)
                      item.position.z= intersect.point.z;
                    }
                    else{
                    if(intersect.point.x<=lock-50&&intersect.point.x>=-lock+50)
                      item.position.x= intersect.point.x+lampSize/lampX;
                    if(intersect.point.z<=lock2-50&&intersect.point.z>=-lock2+50)
                      item.position.z= intersect.point.z+lampSize/lampZ;
                    }
                    
                  }
              });
          }
      }
      }
      }
      // function addSelectedObject( object ) {

      //     selectedObjects = [];
      //     selectedObjects.push( object );

      //   }
      function onDocumentMouseDown( event ) {
        // event.preventDefault();

        mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
        raycaster.setFromCamera( mouse, camera );
        var intersects = raycaster.intersectObjects( room.children.concat(pictures.children) );
        
        if ( intersects.length > 0 ) {
          var intersect = intersects[ 0 ];
          if(intersect.object.name=="floor")
          {            
            var constant=200;
            if(intersect.point.x<roomWidth/2*room.scale.x-100)
            (intersect.point.x<0) ?
            camera.position.x=intersect.point.x+constant:
            camera.position.x=intersect.point.x-constant;
            if(intersect.point.z<roomDepth/2*room.scale.z-100)
            (intersect.point.z<0) ?
            camera.position.z=intersect.point.z+constant:
            camera.position.z=intersect.point.z-constant;
          }

          
        }
 
      }
      function onDocumentMouseUp( event ) {
        mouse.set( ( event.clientX / window.innerWidth ) * 2 - 1, - ( event.clientY / window.innerHeight ) * 2 + 1 );
        raycaster.setFromCamera( mouse, camera );
        var m=[];
        for (var i = 0; i < lamps.children.length; i++) {
          if(i%2!=0)
          m=m.concat(lamps.children[i]);
          else
          m=m.concat(lamps.children[i].children);
        }
        var intersects = raycaster.intersectObjects( pictures.children.concat(scene.getObjectByName("door").children, m ) );        
        // console.log(pictures.children.concat(scene.getObjectByName("door").children, m  ))
        if ( intersects.length > 0 ) {
          var intersect = intersects[ 0 ];
          if(!isWorkingWithLamp&&( intersect.object.name=="topPicture"||intersect.object.name=="bottomPicture"||intersect.object.name=="leftPicture"||intersect.object.name=="rightPicture"))
          {            
            //правая кнопка мыши
            if(event.which==1 && !isWorkingWithScale){
            isWorkingWithPicture=!isWorkingWithPicture;
            pObj=intersect.object;
            moveControls.enabled=!isWorkingWithPicture;

            }
            //левая кнопка мыши
            else if(event.which==3)
            {
              isWorkingWithScale=!isWorkingWithScale;
              if(isWorkingWithScale)
              control.attach( intersect.object );
              else
              control.detach();
              moveControls.enabled=!isWorkingWithScale;

            }

          }
          else if(intersect.object.name=="door"){
            console.log(JSON.stringify(scene.children));

            // $.ajax({
            //   url: '',
            //   data: {

            //   },

            // })
            // document.location.href='http://localhost/';
          }
          else
          {
            isWorkingWithLamp=!isWorkingWithLamp;
            pObj=intersect.object;
            moveControls.enabled=!isWorkingWithLamp;
          }
        } 
      }




      // function loadModelDoor() {
      //     object.traverse( function ( child ) {
      //       // if ( child.isMesh ) child.material= new THREE.MeshStandardMaterial({color:0x1b0303})
      //       if ( child.isMesh ) child.material.map = textureDoor;
      //     } );
      //     object.position.set(roomWidth/2-150,50,0);
      //     object.scale.x=65;
      //     object.scale.y=65;
      //     object.rotation.y=Math.PI/2;

      //     // object.position.y = - 95;
      //     scene.add( object );
      //   }
      //   var manager = new THREE.LoadingManager( loadModelDoor );
      //   manager.onProgress = function ( item, loaded, total ) {
      //     console.log( item, loaded, total );
      //   };
      //   // texture
      //   var textureLoader = new THREE.TextureLoader( manager );
      //   var textureDoor = textureLoader.load( 'assets/textures/Front door.jpg' );
      //   // // model
      //   function onProgress( xhr ) {
      //     if ( xhr.lengthComputable ) {
      //       var percentComplete = xhr.loaded / xhr.total * 100;
      //       console.log( 'model ' + Math.round( percentComplete, 2 ) + '% downloaded' );
      //     }
      //   }
      //   function onError() {}
      //   var loader = new THREE.OBJLoader( manager );
      //   loader.load( 'assets/models/CustomFrontdoor.obj', function ( obj ) {
      //     object = obj;
      //     console.log(object);
      //   }, onProgress, onError );
        // var onProgress = function ( xhr ) {
        //   if ( xhr.lengthComputable ) {
        //     var percentComplete = xhr.loaded / xhr.total * 100;
        //     console.log( Math.round( percentComplete, 2 ) + '% downloaded' );
        //   }
        // };
        // var onError = function () { };
        // new THREE.MTLLoader()
        //   .setPath( 'assets/models/' )
        //   .load( 'lamp.mtl', function ( materials ) {
        //     materials.preload();
        //     new THREE.OBJLoader()
        //       .setMaterials( materials )
        //       .setPath( 'assets/models/' )
        //       .load( 'lamp.obj', function ( object ) {
        //         // object.position.y = - 95;
        //         object.traverse( function ( child ) {
        //     if ( child.isMesh ) child.material= new THREE.MeshStandardMaterial({color:0x1b0303})
        //     // if ( child.isMesh ) child.material.map = new THREE.TextureLoader().load( 'assets/textures/wood.jpg' );;
        //   } );
        //         object.position.set(0,roomHeight-210,0);
        //         object.scale.x=1/8;
        //         object.scale.y=1/8;
        //         object.scale.z=1/8;
        //         object.name="lamp"+lamps.children.length;
        //         lamps.add( object );
        //       }, onProgress, onError );
        //   } );


        // var loader = new THREE.TextureLoader();
        // var normal = loader.load( 'assets/textures/wood.jpg' );
        // var loader = new THREE.TDSLoader( );
        // loader.load( 'assets/models/door3ds.3ds', function ( object ) {
        //   object.traverse( function ( child ) {
        //     if ( child instanceof THREE.Mesh ) {
        //       child.material=new THREE.MeshStandardMaterial ({metalness:0.5,roughness:0.8});
        //       child.material.map = normal;
        //     }
        //   } );
        //   object.position.set(roomWidth/2+20,0,0);
        //   object.scale.x=1/3.5;
        //   object.scale.z=1/3.5;
        //   object.rotation.x=-Math.PI/2;
        //   object.rotation.z=-Math.PI/2;
        //   scene.add( object );
        // } );








var loader = new THREE.TextureLoader();
        var doorMap = loader.load( '../../../assets/textures/Front Door.jpg' );
        var normal = loader.load( '../../../assets/textures/Front Door normal.png' );

        var loader = new THREE.TDSLoader( );
        loader.load( '../../../assets/models/Custom Front door.3ds', function ( object ) {
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({metalness:0.5,roughness:0.8});
              child.material.map = doorMap;
              child.material.normalMap = normal;
              child.name="door";

            }
          } );
          object.position.set(roomWidth/2-20,0,0);
          object.scale.x=85;
          object.scale.z=85;
          object.rotation.x=-Math.PI/2;
          object.rotation.z=Math.PI/2;
          object.name="door";
          scene.add( object );
        } );




  //объект элемента управления 
  var controls = new function () {

    this.width = room.scale.x;
    this.height = room.scale.y;
    this.depth = room.scale.z;
    this.materialFloor="Дерево1";
    this.materialWalls="Обои1";
    this.materialCeil="Обои1";
    this.lampType="Тип 1";
    this.colorFloor=0xffffff;
    this.colorWalls=0xffffff;
    this.colorCeil=0xffffff ;
    this.colorLight=0xffffff;
    this.colorLamp=0xffffff;
    this.lightIntensity=1;

    // this.pictureX=picture[0].scale.x;
    // this.pictureY=picture[0].scale.y;

    this.deleteLamp=function(e){
      lamps.remove(lamps.children[lamps.children.length-1]);
      lamps.remove(lamps.children[lamps.children.length-1]);
    };

    this.addLamp=function(e) {
        // var loader = new THREE.TextureLoader();
        // var la = loader.load( 'assets/textures/wood.jpg' );

        if(controls.lampType=="Тип 1"){
        var loader = new THREE.TDSLoader();
        loader.load( '../../../assets/models/Shade+Lamp-1.3ds', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({metalness:0.5,roughness:0.8});
              // child.material.map = la;
              child.name="lamp"+lamps.children.length;
            }
          } );
          lampSize=210; lampZ=2; lampX=1;
          object.position.set(0,roomHeight*controls.height-lampSize,0);
          object.scale.x=3.5;
          object.scale.z=3.5;
          object.scale.y=3.5;
          object.rotation.x=-Math.PI/2;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
          pointLight(lamps,object.position,controls.colorLight, controls.lightIntensity);
        } );
        }
        else if(controls.lampType=="Тип 2"){
          var loader = new THREE.TDSLoader();
          loader.load( '../../../assets/models/lamp.3ds', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({color:controls.colorLamp,metalness:0.5,roughness:0.8});
              // child.material.map = la;
              child.name="lamp"+lamps.children.length;
            }
          } );
          lampSize=210;lampZ=21;lampX=40;
          object.position.set(0,roomHeight*controls.height-lampSize,0);
          object.scale.x=1/7;
          object.scale.z=1/7;
          object.scale.y=1/7;
          object.rotation.x=-Math.PI/2;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
          pointLight(lamps,object.position,controls.colorLight, controls.lightIntensity);
        } );
        }
        else if(controls.lampType=="Тип 3"){
        var loader = new THREE.OBJLoader();
          loader.load( '../../../assets/models/1.obj', function ( object ) {          
          object.traverse( function ( child ) {
            if ( child instanceof THREE.Mesh ) {
              child.material=new THREE.MeshStandardMaterial ({color:controls.colorLamp,metalness:0.5,roughness:0.8});
              // child.material.map = la;
              child.name="lamp"+lamps.children.length;
            }
          } );
          lampSize=150;lampZ=21;lampX=40;
          object.position.set(0,roomHeight*controls.height-lampSize,0);
          object.scale.x=4;
          object.scale.z=4;
          object.scale.y=4;
          // object.rotation.x=-Math.PI/2;
          object.name="lamp"+lamps.children.length;
          lamps.add( object );
          pointLight(lamps,object.position,controls.colorLight, controls.lightIntensity);
        } );
        }
      };

    this.update= function (e) {

      //удаляем существующее выделение картины
      scene.remove(scene.getObjectByName("outline"));

      camera.position.set(0,400,0);
      //изменение размеров комнаты
      room.scale.x=controls.width;
      room.scale.y=controls.height;
      room.scale.z=controls.depth;
      movePictures();
      moveDoor();
      moveLamp();

      //сохранение пропорций материала стен,потолка и пола при изменении размера
      for (var i = 0; i < room.children.length; i++) {
      if(room.children[i].material.map){
      (i>=0 && i<=1) ?
      room.children[i].material.map.repeat.set(controls.width,controls.depth)
      :
      (i<=3) ?
      room.children[i].material.map.repeat.set(roomWidth*controls.width/500, controls.height*roomHeight/200)
      :
      room.children[i].material.map.repeat.set(roomDepth*controls.depth/300, controls.height*roomHeight/200);
      room.children[i].material.map.needsUpdate = true;
      }}
      };
      this.updateFloorColor=function(e){
        scene.getObjectByName("floor").material=new THREE.MeshStandardMaterial({color:controls.colorLamp,color:new THREE.Color(e)});
      };
      this.updateFloorTexture=function(e){
        var l=JSON.parse(e);
        scene.getObjectByName("floor").material=makeMaterials(l.url,l.x,l.y);
      };
      this.updateWallsColor=function(e){
        for (var i = 2; i < room.children.length; i++) 
          room.children[i].material=new THREE.MeshStandardMaterial({
            color:new THREE.Color(e),
            metalness: 0.3,
            roughness: 0.9});    
      };
      this.updateWallsTexture=function(e){
        var l=JSON.parse(e);
         for (var i = 2; i < room.children.length; i++) 
          room.children[i].material=makeMaterials(l.url,l.x,l.y);
      };
      this.updateCeilColor=function(e){
        scene.getObjectByName("ceil").material=new THREE.MeshStandardMaterial({color:new THREE.Color(e)});
      };
      this.updateCeilTexture=function(e){
        var l=JSON.parse(e);
        scene.getObjectByName("ceil").material=makeMaterials(l.url,l.x,l.y);
      };
      this.updateLightIntensity = function (e){
        lamps.children[lamps.children.length-1].intensity=controls.lightIntensity;
      }
};
controls.addLamp();

      //элементы управления
      var gui = new dat.GUI({width:400});
      var f1 = gui.addFolder('Комната');
      f1.add(controls, 'width',  1,2).onChange(controls.update);
      f1.add(controls, 'height', 1,2).onChange(controls.update);
      f1.add(controls, 'depth', 1,2).onChange(controls.update);
      var f2 = f1.addFolder('Пол');
      f2.addColor(controls,'colorFloor').onChange(controls.updateFloorColor);
      f2.add(controls, 'materialFloor', { 
        "Дерево1": JSON.stringify(texturesForRoom.wood1),
        "Дерево2": JSON.stringify(texturesForRoom.wood2),
        "Дерево3": JSON.stringify(texturesForRoom.wood3),
        "Ламинат": JSON.stringify(texturesForRoom.laminate),
        "Ковролин1": JSON.stringify(texturesForRoom.carpet1),
        "Ковролин2": JSON.stringify(texturesForRoom.carpet2), 
        "Краска": JSON.stringify(texturesForRoom.paint1),
        "Лед": JSON.stringify(texturesForRoom.ice2),
        "Мрамор1": JSON.stringify(texturesForRoom.marble1),
        "Мрамор2": JSON.stringify(texturesForRoom.marble2),
        "Мрамор3": JSON.stringify(texturesForRoom.marble3),
        "Волны": JSON.stringify(texturesForRoom.sea),
        "Ракушки": JSON.stringify(texturesForRoom.seamless),
        "Плитка1": JSON.stringify(texturesForRoom.tile1),
        "Плитка2": JSON.stringify(texturesForRoom.tile3),
      } ).onChange(controls.updateFloorTexture);
      var f3 = f1.addFolder('Стены');
      f3.addColor(controls,'colorWalls').onChange(controls.updateWallsColor);
      f3.add(controls, 'materialWalls', { 
        "Обои1": JSON.stringify(texturesForRoom.wallpaper1),
        "Обои2": JSON.stringify(texturesForRoom.wallpaper2),
        "Обои3": JSON.stringify(texturesForRoom.wallpaper4),
        "Обои4": JSON.stringify(texturesForRoom.wallpaper5),         
      } ).onChange(controls.updateWallsTexture);
      var f4 = f1.addFolder('Потолок');
      f4.addColor(controls,'colorCeil').onChange(controls.updateCeilColor);
      f4.add(controls, 'materialCeil', { 
        "Обои1": JSON.stringify(texturesForRoom.wallpaper1),
        "Обои2": JSON.stringify(texturesForRoom.wallpaper2),
        "Обои3": JSON.stringify(texturesForRoom.wallpaper4),
        "Обои4": JSON.stringify(texturesForRoom.wallpaper5),         
      } ).onChange(controls.updateCeilTexture);
      var f5=gui.addFolder('Освещение');
      f5.addColor(controls,'colorLight');
      f5.addColor(controls,'colorLamp');
      f5.add(controls, 'lightIntensity', 0,2).onChange(controls.updateLightIntensity);;

       f5.add(controls, 'lampType', [
        "Тип 1",
        "Тип 2",
        "Тип 3",
      ] );
      f5.add(controls,'addLamp');
      f5.add(controls,'deleteLamp');
      $(".dg  .property-name").first().text('Длина');
      $(".dg  .property-name").eq(1).text('Высота');
      $(".dg  .property-name").eq(2).text('Ширина');
      $(".dg  .property-name").eq(3).text('Краска');
      $(".dg  .property-name").eq(4).text('Напольное покрытие');
      $(".dg  .property-name").eq(5).text('Краска');
      $(".dg  .property-name").eq(6).text("Текстура");
      $(".dg  .property-name").eq(7).text('Краска');
      $(".dg  .property-name").eq(8).text("Текстура");


      var control = new THREE.TransformControls( camera, renderer.domElement );
        // control.addEventListener( 'change', render );
        control.setMode( "scale" );
        // control.setSpace( 10 );

        scene.add( control );

      // for (var i = 0; i <   maxPictures ; i++) {
          
      // }


      // function  addFolderPicture(i,f1,controls){
      //     var name='Картина'+i;
      //     var f2 = f1.addFolder(name);
      //     f2.add(controls, 'pictureX', 0,2).onChange(controls.updatePicture);
      //     f2.add(controls, 'pictureY', 0,2).onChange(controls.updatePicture);

      // }



      // console.log(scene.getObjectByName("floor").material)


     

      //обработчик изменения размеров экрана
      window.addEventListener( 'resize', onWindowResize, false );

      //отрисовка сцены
      render();


      function render() {
        // stats.update();
        //обновление данных о размере комнаты для контроллера управления перемещением
        moveControls.box=room;
        moveControls.update(clock.getDelta());
        requestAnimationFrame(render);
        renderer.render(scene, camera);
        // composer.render();
      }
      function makeMaterials(url,repeatX,repeatY){
        return new THREE.MeshStandardMaterial({
      map: textureLoader.load(url,function(texture){
        texture.wrapS=texture.wrapT=THREE.RepeatWrapping;
        texture.magFilter=THREE.LinearFilter;
        texture.repeat.set(repeatX,repeatY);
      }),
      metalness: 0.5,
      roughness: 0.9
  });

      }

      

        function  moveDoor(){
          scene.getObjectByName("door").position.x=(roomWidth*controls.width)/2-20;
        }
        function  moveLamp(){
          for (var i = 0; i < lamps.children.length; i++) {
            lamps.children[i].position.y=(roomHeight*controls.height)-210;
          }
           
        }
        //перемещение картин вслед за стеной
      function movePictures(){
      for (var i = 0; i < pictures.children.length; i++) {
        (pictures.children[i].name=="topPicture") ?
        pictures.children[i].position.z=(roomDepth*controls.depth)/2 -8
        :
        (pictures.children[i].name=="bottomPicture") ?
        pictures.children[i].position.z=(-roomDepth*controls.depth)/2 +8
        :
        (pictures.children[i].name=="leftPicture") ?
        pictures.children[i].position.x=(-roomWidth*controls.width)/2+8
        :
        pictures.children[i].position.x=(roomWidth*controls.width)/2-8;
      }
      }
      //размещение картин на стене
      function picturesOnWall(x,y,z,picture,deg,flag){
        for (var j = 1; j <= picturesOnTheWallCount; j++) { 
        pictureOnWall(picturesTextures[picture],x,y,z,deg,picture);
        picture++;
        if(flag) x+=pictureDefaultWidth+distanceOfPictures;
        else z+=pictureDefaultWidth+distanceOfPictures;
        }
        return picture;
      }
      //создание одной картины
    function pictureOnWall(url,posX,posY,posZ,deg,pic){
      //создание материала
      var texture = new THREE.TextureLoader().load( url, function(texture) {
          texture.minFilter = THREE.LinearFilter;
          texture.magFilter = THREE.NearestFilter;
          texture.anisotropy = renderer.capabilities.getMaxAnisotropy();
         var material= new THREE.MeshStandardMaterial({
        map: texture,metalness: 0.5,
      roughness: 0.9});
      //сохранение пропорций изображения
         var w= texture.image.naturalWidth;
         var h= texture.image.naturalHeight;
         var p =(w>h) ? w/h:h/w;
  		if(w>=h && w>pictureDefaultWidth)     {
  		h-=(w-pictureDefaultWidth)/p;
  		w=pictureDefaultWidth;
  		}
  		else if(h>w && h>pictureDefaultHeight)     {
  		w-=(h-pictureDefaultHeight)/p;
  		h=pictureDefaultHeight;
  		}
      //создание меша с картиной
      var plane = new THREE.BoxBufferGeometry(w,h, 5,100,100 );
      var quad = new THREE.Mesh( plane, material);      
      quad.position.x = posX;
      quad.position.z = posZ;
      quad.position.y = posY;    
      quad.rotation.y=deg;
      quad.castShadow=true;
      (pic<=picturesOnTheWallCount-1) ? quad.name='topPicture':
      (pic<=picturesOnTheWallCount*2-1) ? quad.name='bottomPicture':
      quad.name='leftPicture';
      pictures.add(quad);

});
}
  function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
      }
  
}
