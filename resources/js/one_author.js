$(document).ready(function(){
  $(".exh-slider").owlCarousel({
  	items: 3,
  	autoplay: true,
    autoplayTimeout: 2000,
    smartSpeed: 1000,
    nav:true,
  	margin:80,
  	nav: true,
  	autoplayHoverPause:true,
    navText: ['<i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>',
              '<i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>'],
  	responsive: {
  		0: {
  			items: 1
  		},
  		600: {
  			items: 2
  		},
  		1000: {
  			items: 3
  		}
  	}
  });
});