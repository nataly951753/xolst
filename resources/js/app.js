    
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
document.addEventListener('DOMContentLoaded', function() {
	if(screen.width>1800){
	$('body').css('background-size', 'cover');
}
	const page = document.body.dataset.page;
	if (page === 'one_author') {
		require('owl.carousel')
		require('./one_author');
		require('./reviews_slider');
	} else if (page == 'one_exhibition') {
		require('owl.carousel')
		require('./reviews_slider');
	} else if (page == 'report_new_exh') {
		require('chart.js');

	}
});