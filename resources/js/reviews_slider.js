$(document).ready(function(){
  $(".reviews-slider").owlCarousel({
  	items: 2,
  	autoplay: true,
    loop:true,
    autoplayTimeout: 2000,
    smartSpeed: 1000,
    nav:true,
  	margin:80,
  	nav: true,
  	autoplayHoverPause:true,
    navText: ['<i class="fa fa-angle-left fa-2x" aria-hidden="true"></i>',
              '<i class="fa fa-angle-right fa-2x" aria-hidden="true"></i>'],
  	responsive: {
  		0: {
  			items: 1
  		},
  		1000: {
  			items: 2
  		}
  	}
  });
});