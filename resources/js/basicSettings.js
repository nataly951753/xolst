
// function initStats(type) {

//     var stats = new Stats();
//     stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
//     $("body").append(stats.dom);
//     return stats;
// }


function initRenderer(additionalProperties) {

    var props = (typeof additionalProperties !== 'undefined' && additionalProperties) ? additionalProperties : {
                        alpha: !0,
                        antialias: !0};
    var renderer = new THREE.WebGLRenderer(props);
    renderer.shadowMap.enabled = true;
    renderer.shadowMapSoft = true;
    renderer.shadowMap.type = THREE.PCFSoftShadowMap;

    renderer.setClearColor(new THREE.Color(0x000000));
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.shadowMap.enabled = true;
   $("#webgl-output").append(renderer.domElement);

    return renderer;
}


function pointLight(scene,position,color,intensity){
  var pointLight = new THREE.PointLight(color,intensity);
  pointLight.position.copy(position  );
  pointLight.castShadow = true;
  pointLight.name="lamp"+(scene.children.length-1);
  scene.add(pointLight);
}
function initLighting(scene, initialPosition,roomWidth,roomHeight,roomDepth) {
    var position = (initialPosition !== undefined) ? initialPosition : new THREE.Vector3(-10, 30, 40);
    
    // var spotLight = new THREE.SpotLight(0xffffff);
    // spotLight.position.set(200,roomHeight   ,600);
    // spotLight.shadow.mapSize.width = 2048;
    // spotLight.shadow.mapSize.height = 2048;
    // spotLight.shadow.camera.fov = 15;
    // spotLight.castShadow = true;
    // spotLight.decay = 2;
    // spotLight.penumbra = 0.05;
    // spotLight.name = "spotLight";
    // scene.add(spotLight);


  // var pointColor = "#ff5808";
  // var directionalLight = new THREE.DirectionalLight(0xffffff);
  // directionalLight.position.copy(position);
  // directionalLight.castShadow = true;
  // directionalLight.shadow.camera.near = roomHeight;
  // directionalLight.shadow.camera.far = 0;
  // directionalLight.shadow.camera.left = -roomWidth   ;
  // directionalLight.shadow.camera.right = roomWidth ;
  // directionalLight.shadow.camera.top = roomDepth / 2;
  // directionalLight.shadow.camera.bottom = -roomDepth    /2;

  // directionalLight.intensity = 0.8;
  // directionalLight.shadow.mapSize.width = 2048;
  // directionalLight.shadow.mapSize.height = 2048;
  // scene.add(directionalLight);
  // var shadowCamera = new THREE.CameraHelper(directionalLight.shadow.camera)
  // scene.add(shadowCamera);

  // var pointColor = "#ff5808";
  var pointLight = new THREE.PointLight(0xffffff,1);
  pointLight.position.copy(position  );
  // pointLight.decay = 0.1

  pointLight.castShadow = true;

  scene.add(pointLight);

  // var helper = new THREE.PointLightHelper(pointLight);
  // scene.add(helper);

  // var shadowHelper = new THREE.CameraHelper(pointLight.shadow.camera);
  // scene.add(shadowHelper);



    var ambientLight = new THREE.AmbientLight(0x343434,1.3);
    ambientLight.name = "ambientLight";
    scene.add(ambientLight);
    
}

function  initPersonControls(camera)  {
     var fpControls = new THREE.FirstPersonControls(camera);
  fpControls.lookSpeed = 0.03;
  fpControls.movementSpeed = 100;
  fpControls.lookVertical = true;
  fpControls.constrainVertical = true;
  fpControls.verticalMin = 1.0;
  fpControls.verticalMax = 2.0;
  fpControls.lon = -5;
  fpControls.lat = 5;
  fpControls.enabled=false;
  

  return fpControls;

}


  