<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'WelcomeController')->name('main');
Auth::routes(['verify' => true]);

Route::group(['prefix' => 'account', 'middleware'=>['auth','verified','user']], function () {
Route::get('/exhibition/{id}/room', 'RoomController@index')->name('room_settings')->middleware('exhibition')->middleware('count_pictures')->middleware('cors');
	Route::post('/exhibition/{id}/room/save_room', 'RoomController@save')->name('room_save')->middleware('exhibition');
});
Route::group(['prefix' => 'admin', 'middleware'=>['auth','verified','admin'], 'namespace' => 'Admin'], function () {
	Route::get('/', 'AdminController')->name('admin');
	Route::get('/manage_admins', 'CreateAdminController@index')->name('admins');
	Route::post('/create_admin', 'CreateAdminController@create_admin')->name('create_admin');
	Route::get('/role_requests', 'ChangeRoleRequestsController@index')->name('role_requests');
	Route::get('/role_requests/{user_id}', 'ChangeRoleRequestsController@show_one')->name('role_request');
	Route::get('/role_requests/{user_id}/approve/{role_id}', 'ChangeRoleRequestsController@request_approve')->name('role_request_approve');
Route::get('/role_requests/{user_id}/refuse/{role_id}', 'ChangeRoleRequestsController@request_refuse')->name('role_request_refuse');
	Route::get('/reports/new/exhibitions', 'ReportNewExhibitionsController')->name('report_new_exh');
	Route::get('/reports/new/reviews', 'ReportNewReviewsController')->name('report_new_rev');
	Route::get('/reports/new/users', 'ReportNewUsersController')->name('report_new_users');
});
Route::group(['prefix' => 'account', 'middleware'=>['auth','verified','user'], 'namespace' => 'Account'], function () {
	Route::get('/', 'AccountController')->name('account');
	Route::get('/new_change_status_request', 'NewChangeStatusRequestController@index')->name('change_status');
	Route::post('/new_change_status_request', 'NewChangeStatusRequestController@save');
	Route::get('/new_exhibition', 'NewExhibitionController@index')->name('new_exhibition')->middleware("author");
	Route::post('/new_exhibition', 'NewExhibitionController@save')->middleware("author");

	Route::group(['prefix' => 'pictures_for_exhibition/{id}', 'middleware'=>'exhibition'], function () {
		Route::get('/', 'PicturesForExhibitionController@index')->name('pictures_for_exhibition');
		Route::post('/', 'PicturesForExhibitionController@save')->name('pictures_save');
		Route::get('/edit', 'EditPicturesController@index')->name('pictures_show');
		Route::get('/edit/{p_id}', 'EditPicturesController@edit')->name('picture_edit')->middleware('picture');
		Route::post('/edit/{p_id}', 'EditPicturesController@save')->name('picture_save')->middleware('picture');
		Route::post('/delete', 'EditPicturesController@delete')->name('pictures_delete');
	});
	
	Route::get('/exhibitions', 'UsersExhibitionsController@index')->name('user_exhibitions')->middleware("author");
	Route::get('/exhibitions/delete/{id}', 'UsersExhibitionsController@delete')->name('delete_exhibition')->middleware('exhibition');
	Route::get('/exhibitions/edit/{id}', 'UsersExhibitionsController@edit')->name('edit_exhibition')->middleware('exhibition');
	Route::post('/exhibitions/edit/{id}', 'UsersExhibitionsController@save')->name('save_exhibition')->middleware('exhibition');
	Route::get('/reviews', 'UsersReviewsController@index')->name('user_reviews')->middleware("reviewer");
Route::get('/reviews/delete/{id}', 'UsersReviewsController@delete')->name('delete_review')->middleware('review');
Route::get('/reviews/exhibition/{id}/create', 'UsersReviewsController@create')->name('create_review')->middleware("reviewer");
	Route::post('/reviews/exhibition/{id}/create', 'UsersReviewsController@save')->middleware("reviewer");

	Route::get('/reviews/edit/{id}', 'UsersReviewsController@edit')->name('edit_review')->middleware('review')->middleware("reviewer");
	Route::post('/reviews/edit/{id}', 'UsersReviewsController@save_changes')->middleware("reviewer");
	Route::get('/edit', 'EditAccountController@index')->name('account_edit');
	Route::post('/edit', 'EditAccountController@save')->name('account_save');
	Route::get('/favourites_exhibitions', 'FavouriteController@show_exhibitions')->name('user_favourites_exhibitions');
	Route::get('/favourites_authors', 'FavouriteController@show_authors')->name('user_favourites_authors');
	Route::get('/private_exhibition/add_request/{id}', 'PrivateExhibitionController@request_add')->name('add_request_private_exhibition');
	Route::get('/private_exhibition/delete_request/{id}', 'PrivateExhibitionController@request_delete')->name('delete_request_private_exhibition');
	Route::get('/requests/all', 'PrivateExhibitionController@show_requests')->name('show_requests')->middleware("author");
	Route::get('/requests/{user_id}/approve/{exh_id}', 'PrivateExhibitionController@request_approve')->name('request_approve')->middleware("author");
Route::get('/requests/{user_id}/refuse/{exh_id}', 'PrivateExhibitionController@request_refuse')->name('request_refuse')->middleware("author");

   
});
Route::get('/exhibition/{id}', 'ExhibitionsController@show_one')->name('one_exhibition');
Route::get('/exhibition/{id}/room', 'RoomController@show')->name('room_show')->middleware('date_exhibition')->middleware('private_exhibition');
Route::group(['namespace' => 'Account'], function () {
Route::post('/favourite_exhibition', 'FavouriteController@favourite_exhibition');
Route::post('/favourite_author', 'FavouriteController@favourite_author');
});
Route::post('/rating_exhibition', 'ExhibitionsController@rating');


	
Route::get('/exhibitions/{type?}/{style?}/{tag?}/{orderBy?}', 'ExhibitionsController@index')->name('all_exhibitions');
Route::get('/authors/all', 'AuthorsController@index')->name('all_authors');
Route::get('/authors/{id}', 'AuthorsController@show_one')->name('one_author');
Route::get('/reviews/all', 'ReviewsController@index')->name('all_reviews');
Route::get('/reviews/{id}', 'ReviewsController@show_one')->name('one_review');

Route::get('/menu', 'MainMenuController')->name('main_menu');

Route::get('/search', 'SearchController@index')->name('search');
Route::post('/search', 'SearchController@search_exhibition');


